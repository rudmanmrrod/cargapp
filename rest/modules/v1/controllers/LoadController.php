<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTloadstatus;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class LoadController
 * @package rest\versions\v1\controllers
 */
class LoadController extends Controller
{
    public $modelClass = "app\models\McTloadstatus";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    /*
    *   Funcion que permite reportar al transportador su localización actual
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @return $response Se regresa el mensaje
    */
    public function actionReportlocation()
    {
        //Se recolectan los datos del json
        $data = \Yii::$app->request->post();
        
        //Se maneja la peticion options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTloadstatus::find()->where(['fkcontractemp'=>$data['contract_id']])->one();
        $contract = $model->getFkcontractemp0()->one();
        if($model && ($contract->fkuser==\Yii::$app->user->id))
        {
            $model->latitude = (string)$data['latitude'];
            $model->longitude = (string)$data ['longitude'];
            if($model->save())
            {
                $response = ["mensaje"=>'Se actualizó correctamente la localización actual','validacion'=>'ok'];
                $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
                if($contract->fkcontract!=null)
                {
                    $id_contract = $contract->fkcontract0->pk_contract;
                    //Se obtiene el número de contrato
                    $zeros = '000000';
                    $contract_type = ''.$contract->pk_contract;
                    $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                    //Se crea la notificación para la empresa
                    $model_notificacion = new McTnotificaciones();
                    $model_notificacion->fk_login=$company->id;
                    $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> actualizó su ubicación en el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/usertcontract?id_contract=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                    $model_notificacion->activa=1;
                    $model_notificacion->save();
                }
                else if($contract->fkcotizaremp!=null)
                {
                    $id_contract = $contract->fkcotizaremp0->pk_contract;
                    //Se obtiene el número de contrato
                    $zeros = '000000';
                    $contract_type = ''.$contract->pk_contract;
                    $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                    //Se crea la notificación para la empresa
                    $model_notificacion = new McTnotificaciones();
                    $model_notificacion->fk_login=$company->id;
                    $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> actualizó su ubicación en el contrato ET".$contract_number." <a href='".Yii::getAlias('@web')."/seecotization?id_cot=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                    $model_notificacion->activa=1;
                    $model_notificacion->save();
                }
            }
            else
            {
                $response = ["mensaje"=>'Ocurrió un error','validacion'=>'error', 'error'=>$model->getErrors()];
            }
        }
        else
        {
            $response = ["mensaje"=>'Ocurrió un error','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite reportar al transportador el estado de la carga
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @return $response Se regresa el mensaje
    */
    public function actionReportloadstatus()
    {
        //Se recolectan los datos del json
        $data = \Yii::$app->request->post();
        
        //Se maneja la peticion options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTloadstatus::find()->where(['fkcontractemp'=>$data['contract_id']])->one();
        $contract = $model->getFkcontractemp0()->one();
        if($model && ($contract->fkuser==\Yii::$app->user->id))
        {
            $model->fkstatus = $data['status'];
            $model->save();
            $response = ["mensaje"=>'Se actualizó correctamente el estado de la carga','validacion'=>'ok'];
            $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
            if($contract->fkcontract!=null)
            {
                $id_contract = $contract->fkcontract0->pk_contract;
                //Se obtiene el número de contrato
                $zeros = '000000';
                $contract_type = ''.$contract->pk_contract;
                $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                //Se crea la notificación para la empresa
                $model_notificacion = new McTnotificaciones();
                $model_notificacion->fk_login=$company->id;
                $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> actualizó el estado de la carga en el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/usertcontract?id_contract=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                $model_notificacion->activa=1;
                $model_notificacion->save();
            }
            else if($contract->fkcotizaremp!=null)
            {
                $id_contract = $contract->fkcotizaremp0->pk_contract;
                //Se obtiene el número de contrato
                $zeros = '000000';
                $contract_type = ''.$contract->pk_contract;
                $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                //Se crea la notificación para la empresa
                $model_notificacion = new McTnotificaciones();
                $model_notificacion->fk_login=$company->id;
                $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> actualizó el estado de la carga en el contrato ET".$contract_number." <a href='".Yii::getAlias('@web')."/seecotization?id_cot=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                $model_notificacion->activa=1;
                $model_notificacion->save();
            }
        }
        else
        {
            $response = ["mensaje"=>'Ocurrió un error','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite enviar al transportador el codigo y foto de sellos
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @return $response Se regresa el mensaje
    */
    public function actionReportloadend()
    {
        //Se recolectan los datos del json
        $data = \Yii::$app->request->post();
        
        //Se maneja la peticion options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTloadstatus::find()->where(['fkcontractemp'=>$data['contract_id']])->one();
        $contract = $model->getFkcontractemp0()->one();
        if($model && ($contract->fkuser==\Yii::$app->user->id))
        {
            $model->codigo = $data['codigo'];
            $sello1 = \yii\web\UploadedFile::getInstanceByName('sello1');
            $rnd = $this->randKey("abcdef0123456789", 50);
            $fileName = $rnd . str_replace(' ','_',$sello1->name);
            $sello1->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/load_img/' . $fileName);
            $model->sello1 = '/img/system_imgs/' . $fileName;
            $sello2 = \yii\web\UploadedFile::getInstanceByName('sello2');
            $fileName = $rnd . str_replace(' ','_',$sello2->name);
            $sello2->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/load_img/' . $fileName);
            $model->sello2 = '/img/system_imgs/' . $fileName;
            $model->fkstatus = 13;
            if($model->save())
            {
                $response = ["mensaje"=>'Se actualizó correctamente el estado de la carga','validacion'=>'ok'];
                $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
                if($contract->fkcontract!=null)
                {
                    $id_contract = $contract->fkcontract0->pk_contract;
                    //Se obtiene el número de contrato
                    $zeros = '000000';
                    $contract_type = ''.$contract->pk_contract;
                    $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                    //Se crea la notificación para la empresa
                    $model_notificacion = new McTnotificaciones();
                    $model_notificacion->fk_login=$company->id;
                    $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> finalizó la carga en el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/usertcontract?id_contract=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                    $model_notificacion->activa=1;
                    $model_notificacion->save();
                }
                else if($contract->fkcotizaremp!=null)
                {
                    $id_contract = $contract->fkcotizaremp0->pk_contract;
                    //Se obtiene el número de contrato
                    $zeros = '000000';
                    $contract_type = ''.$contract->pk_contract;
                    $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                    //Se crea la notificación para la empresa
                    $model_notificacion = new McTnotificaciones();
                    $model_notificacion->fk_login=$company->id;
                    $model_notificacion->msj_notificacion="El Transportador <b>". $transportador->user_name ."</b> finalizó la carga en el contrato ET".$contract_number." <a href='".Yii::getAlias('@web')."/seecotization?id_cot=".$id_contract."' onclick='modalClick(event)'> Ver Carga</a>";
                    $model_notificacion->activa=1;
                    $model_notificacion->save();
                }
            }
            else
            {
                $response = ["mensaje"=>'Ocurrió un error','validacion'=>'error', 'error'=>$model->getErrors()];
            }
        }
        else
        {
            $response = ["mensaje"=>'Ocurrió un error','validacion'=>'error'];
        }
        return $response;
    }
}
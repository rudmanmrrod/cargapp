<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTuser;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\auth\QueryParamAuth;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class ProfileController
 * @package rest\versions\v1\controllers
 */
class ProfileController extends Controller
{
    public $modelClass = "app\models\McTuser";

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
   

    /*
    *   Funcion que permite actualizar el perfil del transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @return $response Se regresa el mensaje
    */
    public function actionUpdate()
    {
        $data = \Yii::$app->request->post();
        
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); } 

        $profile = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
        $model = $profile->getFklogin0()->one();
        $profile->user_name = $data['user_name'];
        $profile->user_phone = $data['user_phone'];
        $model->email = $data['email'];
        $ti = \yii\web\UploadedFile::getInstanceByName('user_avatar');
        if($ti){
            $rnd = $this->randKey("abcdef0123456789", 50);
            $fileName = $rnd . str_replace(' ','_',$ti->name);
            $ti->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/' . $fileName);
            $profile->user_avatar = '/img/system_imgs/' . $fileName;
        }
        $profile->save();
        $model->save();
        if($model->getErrors() or $profile->getErrors())
        {
             $response = ['mensaje'=>'Oucrrió un error al actualizar el perfil','validacion'=>'error',
             'error'=>[$model->getErrors(),$profile->getErrors()]];
        }
        else
        {
            $response = ['mensaje'=>'Se actualizó el perfil con éxito','validacion'=>'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite ver el perfil del transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @return $response Se regresa el mensaje
    */
    public function actionSeeprofile()
    {
        $profile = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
        $model = $profile->getFklogin0()->select(['email'])->one();
        $data['user_name'] = $profile->user_name;
        $data['user_phone'] = $profile->user_phone;
        $data['user_avatar'] = $profile->user_avatar;
        $data['email'] = $model->email;
        $response = ['mensaje'=>'Se obtuvo el perfil con éxito','validacion'=>'ok',
        'perfil'=>$data];
        return $response;
    }
}
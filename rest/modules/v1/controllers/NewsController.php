<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTnew;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class FriendshipController
 * @package rest\versions\v1\controllers
 */
class NewsController extends Controller
{
    public $modelClass = "app\models\McTnew";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite suministrar un servicio rest para la consulta de
    *   Noticias del portal cargapp
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 30/06/2016
    *   @return $response de la consulta
    */
   public function actionList()
   {
        //Se recolectan los datos del json
        $model = McTnew::find()->where(['new_type'=>2])->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $count_query = count($model);
        if ($model and $count_query>0) {
            //Se construye en json lo que va a tomar la aplicación web
            $response = ['mensaje'=> 'Estas son las noticias', 'validacion' => 'ok', 'noticias' => $model];
        } 
        elseif($count_query==0){
            $response = ['mensaje'=> 'No hay Noticias', 'validacion' => 'ok', 'noticias' => $model];
        }
        else {
            return ['mensaje'=>'ocurrio un error','validacion'=>'error'];
        }
        return $response;
    }
}
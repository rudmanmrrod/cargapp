<?php
namespace app\rest\modules\v1\controllers; 

use app\models\FormRegister;
use app\models\FormRecoverPass;
use app\models\Users;
use app\models\McTuser;
use app\models\McTvehicle;
use app\models\McTreferences;
use app\models\McTattachments;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\web\Session;
use yii\widgets\ActiveForm;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class AccountController
 * @package rest\versions\v1\controllers
 */
class AccountController extends Controller
{
    public $modelClass = "app\models\FormRegister";

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
   

    public function actionCreate()
    {
        $model = new FormRegister;
       
        $msg = null;
        $t = "";
        $ti = "";
        $validacion = "ok";

        $data = \Yii::$app->request->post();
        
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }

        $model->name = $data['name'];   
        $model->email = $data['email'];
        $model->phone = $data['phone'];
        $model->password = $data['password'];
        $model->password_repeat = $data['password_repeat'];
        $model->type = 4;

        if($model->validate())
        {
            //Se crea el usuario
            $table = new Users;
            $table->email = $model->email;
            $table->email2 = $model->email;
            $table->password = crypt($model->password, \Yii::$app->params["salt"]);
            $table->authKey = $this->randKey("abcdef0123456789", 200);
            $table->accessToken = $this->randKey("abcdef0123456789", 200);
            $table->role = $model->type;
            if ($table->insert())
            {
                 $user = $table->find()->where(["email2" => $model->email])->one();
                 $id = urlencode($user->id);
                 $authKey = urlencode($user->authKey);
                 //Se crea el perfil
                 $ti = \yii\web\UploadedFile::getInstanceByName('avatar');
                 $mc_user = new McTuser;
                 if($ti){
                     $rnd = $this->randKey("abcdef0123456789", 50);
                     $fileName = $rnd . str_replace(' ','_',$ti->name);
                     $ti->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/' . $fileName);
                     $mc_user->user_avatar = '/img/system_imgs/' . $fileName;
                 }else
                    $mc_user->user_avatar = "/img/system_imgs/pp.png";
                 $mc_user->user_name = $model->name;
                 $mc_user->user_phone = $model->phone;
                 $mc_user->fklogin = $id;
                 $mc_user->insert();

                //Se crea el vehiculo
                $vehicle = new McTvehicle;
                $vehicle->fkvehicletype = $data['vehicletype'];
                $vehicle->fkuser = $mc_user->pkuser;
                $vehicle->vehicle_bodywork = $data['vehicle_bodywork'];
                $vehicle->vehicle_licenceplate = $data['vehicle_licenceplate'];
                $vehicle->vehicle_model = $data['vehicle_model'];
                $vehicle->vehicle_brand = $data['vehicle_brand'];
                $vehicle->vehicle_color = $data['vehicle_color'];
                $vehicle->save();

                //Se crean las refencias
                $reference_comertial = new McTreferences;
                $reference_comertial->fkuser = $mc_user->pkuser;
                $reference_comertial->reference_name = $data['reference_comertial_name'];
                $reference_comertial->reference_address = $data['reference_comertial_address'];
                $reference_comertial->reference_phone = $data['reference_comertial_phone'];
                $reference_comertial->fk_referencetype = 1;
                $reference_comertial->save();

                $reference_personal1 = new McTreferences;
                $reference_personal1->fkuser = $mc_user->pkuser;
                $reference_personal1->reference_name = $data['reference_personal1_name'];
                $reference_personal1->reference_address = $data['reference_personal1_address'];
                $reference_personal1->reference_phone = $data['reference_personal1_phone'];
                $reference_personal1->fk_referencetype = 2;
                $reference_personal1->save();

                $reference_personal2 = new McTreferences;
                $reference_personal2->fkuser = $mc_user->pkuser;
                $reference_personal2->reference_name = $data['reference_personal2_name'];
                $reference_personal2->reference_address = $data['reference_personal2_address'];
                $reference_personal2->reference_phone = $data['reference_personal2_phone'];
                $reference_personal2->fk_referencetype = 2;
                $reference_personal2->save();

                //Se crean los adjuntos
                //Cedulas
                $cedula1 = new McTattachments;
                $cedula1->fkuser = $mc_user->pkuser;
                $cedula1_a = \yii\web\UploadedFile::getInstanceByName('cedula1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$cedula1_a->name);
                $cedula1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $cedula1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $cedula1->attachment_type = 1;
                $cedula1->save();

                $cedula2 = new McTattachments;
                $cedula2->fkuser = $mc_user->pkuser;
                $cedula2_a = \yii\web\UploadedFile::getInstanceByName('cedula2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$cedula2_a->name);
                $cedula2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $cedula2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $cedula2->attachment_type = 1;
                $cedula2->save();

                //Tarjetas de propiedad
                $tarj_prop1 = new McTattachments;
                $tarj_prop1->fkuser = $mc_user->pkuser;
                $tarj_prop1_a = \yii\web\UploadedFile::getInstanceByName('tarj_prop1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$tarj_prop1_a->name);
                $tarj_prop1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $tarj_prop1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $tarj_prop1->attachment_type = 2;
                $tarj_prop1->save();

                $tarj_prop2 = new McTattachments;
                $tarj_prop2->fkuser = $mc_user->pkuser;
                $tarj_prop2_a = \yii\web\UploadedFile::getInstanceByName('tarj_prop2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$tarj_prop2_a->name);
                $tarj_prop2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $tarj_prop2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $tarj_prop2->attachment_type = 2;
                $tarj_prop2->save();

                //Licencias
                $licencia1 = new McTattachments;
                $licencia1->fkuser = $mc_user->pkuser;
                $licencia1_a = \yii\web\UploadedFile::getInstanceByName('licencia1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$licencia1_a->name);
                $licencia1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $licencia1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $licencia1->attachment_type = 3;
                $licencia1->save();

                $licencia2 = new McTattachments;
                $licencia2->fkuser = $mc_user->pkuser;
                $licencia2_a = \yii\web\UploadedFile::getInstanceByName('licencia2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$licencia2_a->name);
                $licencia2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $licencia2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $licencia2->attachment_type = 3;
                $licencia2->save();

                //Revisiones
                $revision1 = new McTattachments;
                $revision1->fkuser = $mc_user->pkuser;
                $revision1_a = \yii\web\UploadedFile::getInstanceByName('revision1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$revision1_a->name);
                $revision1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $revision1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $revision1->attachment_type = 4;
                $revision1->save();

                $revision2 = new McTattachments;
                $revision2->fkuser = $mc_user->pkuser;
                $revision2_a = \yii\web\UploadedFile::getInstanceByName('revision2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$revision2_a->name);
                $revision2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $revision2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $revision2->attachment_type = 4;
                $revision2->save();

                //SOAT
                $soat1 = new McTattachments;
                $soat1->fkuser = $mc_user->pkuser;
                $soat1_a = \yii\web\UploadedFile::getInstanceByName('soat1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$soat1_a->name);
                $soat1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $soat1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $soat1->attachment_type = 5;
                $soat1->save();

                $soat2 = new McTattachments;
                $soat2->fkuser = $mc_user->pkuser;
                $soat2_a = \yii\web\UploadedFile::getInstanceByName('soat2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$soat2_a->name);
                $soat2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $soat2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $soat2->attachment_type = 5;
                $soat2->save();

                //Tarjetas de la empresa
                $tarjetas1 = new McTattachments;
                $tarjetas1->fkuser = $mc_user->pkuser;
                $tarjetas1_a = \yii\web\UploadedFile::getInstanceByName('tarjetas1');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$tarjetas1_a->name);
                $tarjetas1_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $tarjetas1->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $tarjetas1->attachment_type = 6;
                $tarjetas1->save();

                $tarjetas2 = new McTattachments;
                $tarjetas2->fkuser = $mc_user->pkuser;
                $tarjetas2_a = \yii\web\UploadedFile::getInstanceByName('tarjetas2');
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$tarjetas2_a->name);
                $tarjetas2_a->saveAs(\Yii::getAlias('@webroot') . '/../web/img/system_imgs/transporter_attachments/' . $fileName);
                $tarjetas2->attachment_location = '/img/system_imgs/transporter_attachments/' . $fileName;
                $tarjetas2->attachment_type = 6;
                $tarjetas2->save();
                  
                 /*Yii::$app->mailer->compose('verify', ['name'=>$model->name, 'link'=>'http://micargapp.com'. Yii::getAlias('@web').'/site/confirm?id='.$id."&authKey=".$authKey])
                 ->setTo($user->email2)
                 ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                 ->setSubject("Confirmar registro")
                 ->send();*/
                 
                 /*$model->name = null;   
                 $model->email = null;
                 $model->phone = null;
                 $model->password = null;
                 $model->password_repeat = null;
                 $model->avatar = null;
                 $model->type = null;*/

                 $msg = "Se ha registrado correctamente, un correo de verificación ha sido enviado " . $model->email;
            }
            else
            {
                $msg = "Ha ocurrido un error al llevar a cabo tu registro";
            }
        }
        else
        {
            $msg = $model->getErrors();
            $validacion = "error";
        }
        $response = ['mensaje'=>$msg,'validacion'=>$validacion];
        return $response;
    }

    public function actionRecover()
    {
        //Instancia para validar el formulario
        $model = new FormRecoverPass;
     
        //Mensaje que será mostrado al usuario
        $msg = null;
        $validacion = "ok";
        $error = '';

        $data = \Yii::$app->request->post();
        
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }

        $model->email = $data['email'];
     
        if ($model->validate())
        {
            //Buscar al usuario a través del email
            $table = Users::find()->where("email=:email", [":email" => $model->email])->one();

            //Si el usuario existe
            if ($table)
            {
                //El id del usuario es requerido para generar la consulta a la tabla users y 
                //restablecer el password del usuario
                $table = Users::find()->where("email=:email", [":email" => $model->email])->one();

                //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
                //para que lo introduzca en un campo del formulario de reseteado
                //Es guardada en el registro correspondiente de la tabla users
                $verification_code = $this->randKey("abcdef0123456789", 8);
                //Columna verification_code
                $table->verification_code = $verification_code;
                //Guardamos los cambios en la tabla users
                $table->save();

                //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                /*$subject = "Recuperar password";
                $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
                $body .= "<strong>".$verification_code."</strong></p>";
                $body .= "<p><a href='http://localhost/micargapp_v1/web/site/resetpass'>Recuperar password</a></p>";

                //Enviamos el correo
                Yii::$app->mailer->compose()
                ->setTo($model->email)
                ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                ->setSubject($subject)
                ->setHtmlBody($body)
                ->send();*/


                //Mostrar el mensaje al usuario
                $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
            }
            else //El usuario no existe
            {
                $msg = "No Existe el correo solicitado";
                $validacion = "error";
            }
        }
        else
        {
			$msg = "No Existe el correo solicitado";
            $error = $model->getErrors();
            $validacion = "error";
        }
        $response = ['mensaje'=>$msg,'error'=>$error,'validacion'=>$validacion];
        return $response;
    }
}
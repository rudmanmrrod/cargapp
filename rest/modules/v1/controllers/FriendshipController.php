<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTnotificaciones;
use app\models\McTfriendship;
use app\models\McTcompany;
use app\models\McTlogin;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class FriendshipController
 * @package rest\versions\v1\controllers
 */
class FriendshipController extends Controller
{
    public $modelClass = "app\models\McTfriendship";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite suministrar un servicio rest para la consulta de
    *   Solicitudes pendites para el usuario transportador
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 19/06/2016
    *   @return $response de la consulta
    */
   public function actionFriend($status)
   {
        //Se recolectan los datos del json
        $model = McTfriendship::find()->select(['fkuser_send'])->where(['fkuser_recive' => \Yii::$app->user->id, 'status'=> $status ])->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $count_query = count($model);
        $company = array();
        $detallescom = array();
        $data = array();
        foreach ($model as $value) {
            $var = $value->getFkuserSend()->one();
            array_push($company, $var);
        }
        foreach ($company as $values) {
            $detalles = $values->getMcTcompanies()->one();
            array_push($data, $detalles);
        }
        if ($data and $count_query>0) {
            //Se construye en json lo que va a tomar la aplicación web
            $response = ['mensaje'=> 'Estas son tus solicitudes pendites', 'validacion' => 'ok', 'solicitudes' => $data];
        } 
        elseif($count_query==0){
            $response = ['mensaje'=> 'No posees solicitudes de amistad', 'validacion' => 'ok', 'solicitudes' => $data];
        }
        else {
            return ['mensaje'=>'ocurrio un error','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite enviar una solicitud de amistad a una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/07/2016
    *   @param $id Recibe el id de la empresa
    *   @return $response Regresa el estado de la solicitud
    */
    public function actionSendsolicitude($id)
    {
        $company = McTcompany::find()->where(['pkcompany'=>$id])->one();
        $user_id = $company->fklogin0->id;
        $response = '';
        $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
        if(!McTfriendship::find()->where(['fkuser_send'=>$user_id,'fkuser_recive'=>\Yii::$app->user->id])->one())
        {
            $model = new McTfriendship();
            $model->fkuser_send = $user_id;
            $model->fkuser_recive = \Yii::$app->user->id;
            $model->send_date = date('Y-m-d H:i:s');
            $model->status = 2;
            $model->save();
            //Se crea la notificación para la empresa
            $notificacion = new McTnotificaciones();
            $notificacion->fk_login=$company->fklogin;
            $notificacion->msj_notificacion="El transportador <b>". $transportador->user_name ."</b> envio una solicitud de amistad <a href='".\Yii::getAlias('@web')."/accountc' onclick='modalClick(event)'> Ver amistades</a>";
            $notificacion->activa=1;
            $notificacion->save();
            $response = ['mensaje'=> 'Se envió la solicitud con éxito', 'validacion' => 'ok'];
        }
        else
        {
            $response = ['mensaje'=> 'Ya envió una solicitud a esta empresa', 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite aceptar una solicitud de amistad de una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 20/07/2016
    *   @param $id Recibe el id de la empresa
    *   @return $response Regresa el estado de la solicitud
    */
    public function actionAceptsolicitude($id)
    {
        $company = McTcompany::find()->where(['pkcompany'=>$id])->one();
        $user_id = $company->fklogin0->id;
        $friendship = McTfriendship::find()->where(['fkuser_send'=>$user_id,'fkuser_recive'=>\Yii::$app->user->id])->one();
        if(count($friendship)>0)
        {
            if($friendship->status==0)
            {
                $friendship->status = 1;
                $friendship->save();
                $response = ['mensaje'=> 'Se aceptó la solicitud con éxito', 'validacion' => 'ok'];
                //Se crea la notificación
                $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
                $notificacion = new McTnotificaciones();
                $notificacion->fk_login=$company->fklogin;
                $notificacion->msj_notificacion="El transportador <b>". $transportador->user_name ."</b> aceptó tu solicitud de amistad <a href='".\Yii::getAlias('@web')."/accountc' onclick='modalClick(event)'> Ver amistades</a>";
                $notificacion->activa=1;
                $notificacion->save();
            }
            else
            {
                $response = ['mensaje'=> 'Ya se aceptó la solicitud', 'validacion' => 'ok'];    
            }
        }
        else
        {
            $response = ['mensaje'=> 'No se encontró la solicitud', 'validacion' => 'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite borrar una solicitud de amistad de una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 16/07/2016
    *   @param $id Recibe el id de la empresa
    *   @return $response Regresa el estado de la solicitud
    */
    public function actionDeletesolicitude($id)
    {
        $company = McTcompany::find()->where(['pkcompany'=>$id])->one();
        $user_id = $company->fklogin0->id;
        $friendship = McTfriendship::find()->where(['fkuser_send'=>$user_id,'fkuser_recive'=>\Yii::$app->user->id])->one();
        if(count($friendship)>0)
        {
            $friendship->delete();
            $response = ['mensaje'=> 'Se eliminó la solicitud con éxito', 'validacion' => 'ok'];
        }
        else
        {
            $response = ['mensaje'=> 'No se encontró la solicitud', 'validacion' => 'error'];
        }
        return $response;
    }
}
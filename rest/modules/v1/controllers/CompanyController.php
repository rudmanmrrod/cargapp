<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTlogin;
use app\models\McTcompany;
use app\models\McTfriendship;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class CompanyController
 * @package rest\versions\v1\controllers
 */
class CompanyController extends Controller
{
    public $modelClass = "app\models\McTlogin";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite suministrar un servicio rest para la consulta del
    *   listado de las empresas que existen y se encuentran activas
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 19/06/2016
    *   @return $response de la consulta
    */
   public function actionList()
   {
        //Se recolectan los datos del json

        $model = McTlogin::find()->where(['role' => 3, 'activate'=> 1 ])->all();
        $company = array();
        foreach ($model as $value) {
            $var = $value->getMcTcompanies()->select(['pkcompany','company_rz','company_avg'])->all();
            array_push($company, $var);
        }
        $count_query = count($company);
        if ($company and $count_query>0) {
            //Se construye en json lo que va a tomar la aplicación web
            $response = ['mensaje'=> 'Se muestra el listado de las empresas que estan activas', 'validacion' => 'ok', 'empresas' => $company];
        } 
        elseif($count_query==0){
            $response = ['mensaje'=> 'No existe un listado de empresas', 'validacion' => 'ok', 'empresas' => $model];
        }
        else {
            return ['mensaje'=>'ocurrio un error','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite mostrar las empresas que son amigas de un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/07/2016
    *   @return $response de la consulta
    */
   public function actionFriend()
   {
        $model = McTfriendship::find()->where(['fkuser_recive' => \Yii::$app->user->id, 'status'=> 1 ])->all();
        $company = array();
        foreach ($model as $value) {
            $var = $value->getFkuserSend()->one()->getMcTcompanies()->select(['pkcompany','company_rz','company_avg'])->all();
            array_push($company, $var);
        }
        $count_query = count($company);
        if ($company and $count_query>0) {
            $response = ['mensaje'=> 'Se encontraron los siguientes amigos', 'validacion' => 'ok', 'empresas' => $company];
        } 
        elseif($count_query==0){
            $response = ['mensaje'=> 'No tienes amigos', 'validacion' => 'ok', 'empresas' => $company];
        }
        else {
            return ['mensaje'=>'ocurrio un error','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite mostrar de forma más profunda los datos de una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/07/2016
    *   @param $id Es el id de la empresa
    *   @return $response de la consulta
    */
   public function actionSee($id)
   {
        $model = McTcompany::findOne($id);
        if($model)
        {   
            $company = array();
            $company['company_rz'] = $model->company_rz;
            $company['company_review'] = $model->company_review;
            $company['company_avatar'] = $model->company_avatar;
            $company['company_avg'] = $model->company_avg;
            $response = ['mensaje'=> 'El perfil de la empresa es el siguiente', 'validacion' => 'ok', 'perfil' => $company];
        }
        else
        {
            $response = ['mensaje'=> 'La Empresa solicitada no existe', 'validacion' => 'error'];
        }
        return $response;
    }
}
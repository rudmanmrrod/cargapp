<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTnotificaciones;
use app\models\McTcompany;
use app\models\McTlogin;
use app\models\McTuser;
use app\models\McTcotizaremp;
use app\models\McTfriendship;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class NotificacionesController
 * @package rest\versions\v1\controllers
 */
class NotificacionesController extends Controller
{
    public $modelClass = "app\models\McTnotificaciones";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite suministrar un servicio rest para la consulta de
    *   Notificaciones pendites para el usuario transportador
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 27/07/2016
    *   @return $response de la consulta
    */
   public function actionNotificacion()
   {
        //Se recolectan los datos del json
        $model = McTnotificaciones::find()->where(['fk_login' => \Yii::$app->user->id, 'activa'=> 1 ])->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $count_query = count($model);
        $response = '';
        if ($model and $count_query>0) {
            //Se construye en json lo que va a tomar la aplicación web
            $response = ['mensaje'=> 'Estas son las notificaciones pendientes por leer', 'validacion' => 'ok', 'notificaciones' => $model];
        } 
        elseif($count_query==0){
            $response = ['mensaje'=> 'No posees notificaciones', 'validacion' => 'ok', 'notificaciones' => $model];
        }
        else {
            return ['mensaje'=>'Ocurrio un error, verifica la conexion','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite enviar cambiar el estatus de de la notificacion
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 27/07/2016
    *   @param $id Recibe el id de la notificacion
    *   @return $response Regresa  un mensaje de estatus de la notificacion
    */
    public function actionChangestatus($id)
    {
        $model = McTnotificaciones::findOne($id);
        $response = '';
        if(!$model->activa == 0)
        {
            $model->activa = 0;
            $model->save();
            $response = ['mensaje'=> 'El estatus se cambio a visto', 'validacion' => 'ok'];
        }
        else
        {
            $response = ['mensaje'=> 'La notificacion ya se verifico', 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite enviar las notificaciones por el tipo
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/08/2016
    *   @param $type Recibe el tipo de la notificacion
    *   @return $response Regresa la cantidad de notificaciones
    */
    public function actionGetnots($type)
    {
        $model = McTnotificaciones::find()->where(['fk_login'=>\Yii::$app->user->id,'activa'=>1,
            'type'=>$type])->all();
        $response = '';
        $cantidad = count($model);
        if($cantidad > 0)
        {
            $response = ['mensaje'=> 'Se obtuvieron las siguientes notificaciones', 'validacion' => 'ok',
            'cantidad'=> $cantidad];
        }
        else
        {
            $response = ['mensaje'=> 'No hay notificaciones activas', 'validacion' => 'ok', 'cantidad'=> 0];
        }
        return $response;
    }

    /*
    *   Funcion que permite marcar como leidas las notificaciones por el tipo
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/08/2016
    *   @param $type Recibe el tipo de la notificacion
    *   @return $response Regresa la validación
    */
    public function actionReadnots($type)
    {
        $models = McTnotificaciones::find()->where(['fk_login'=>\Yii::$app->user->id,'activa'=>1,
            'type'=>$type])->all();
        $response = '';
        $cantidad = count($models);
        if($cantidad > 0)
        {
            foreach ($models as $model) {
                $model->activa = 0;
                $model->save();
            }
            $response = ['mensaje'=> 'Se marcaron como leidas las notificaciones', 'validacion' => 'ok'];
        }
        else
        {
            $response = ['mensaje'=> 'No hay notificaciones para desactivar', 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite ver las cantidad de cargas disponibles que pueda aceptar un transportador 
    *   de acuerdo al  tipo de su vehículo
    *   Creado por: Rodrigo Boet
    *   Fecha: 16/07/2016
    *   @return $response Se regresan la cantidad cotizaciones que existan
    */
   public function actionCotization()
   {
        //Se recolectan los datos del json
        $profile = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
        $vehicle = $profile->getMcTvehicles()->one()->fkvehicletype;
        $model = McTcotizaremp::find()->where(['fk_vehicletype' => $vehicle, 'send_all'=> 1, 'fkstatus' => 1])->all();
        $cotizacion = 0;
        if(count($model)>0)
        {
            foreach ($model as $value) {
                $friendcompany = $value->getFkcompany0()->one()->getFklogin0()->one();
                $friendship = McTfriendship::find()->where(['fkuser_send'=>$friendcompany->id,'fkuser_recive'=>\Yii::$app->user->id])->one();
                $tload = $value->getMcTloadproposals()->one();
                if(count($friendship)>0 and !$tload)
                {
                    $cotizacion++;
                }
            }
            $response = ['mensaje'=> 'Se encontraron las siguientes cotizaciones', 'validacion' => 'ok', 'cantidad' => $cotizacion];
        }
        else
        {
            $response = ['mensaje'=> 'No hay cotizaciones disponibles', 'validacion' => 'ok'];
        }
        return $response;
    }
}
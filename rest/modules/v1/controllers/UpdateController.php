<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTuser;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class UpdateController
 * @package rest\versions\v1\controllers
 */
class UpdateController extends Controller
{
    public $modelClass = "app\models\McTuser";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite suministrar un servicio rest para la cargar la data
    *   del usuario transportador en el formulario de update de la app movil 
    *   Creado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 24/06/2016
    *   @return $response de la consulta
    */
   public function actionUpdate(){
        $model = McTuser::find()->where(['fklogin' => \Yii::$app->user->id])->one();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($model){
            $response = ['mensaje' => 'Se estan cargando los datos del usuario', 'validacion' => 'ok', 'datos' => $model];
        }
        else{
            $response = ['mensaje' => 'Existe un error, verifica tu acceso...', 'validacion' => 'error', 'datos' => $model];
        }
        return $response;
    }
}
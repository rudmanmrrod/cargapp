<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTnotificaciones;
use app\models\McTcotizaremp;
use app\models\McTuser;
use app\models\McTcompany;
use app\models\McTfriendship;
use app\models\McTloadproposal;
use app\models\McUloadproposal;
use app\models\McTloadstatus;
use app\models\McTcontractemp;
use app\models\McTtransporterpayment;
use app\models\McTuserchattrans;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class CotizarController
 * @package rest\versions\v1\controllers
 */
class CotizarController extends Controller
{
    public $modelClass = "app\models\McTcotizaremp";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Funcion que permite ver las cargas que pueda aceptar un transportador de acuerdo al 
    *   tipo de su vehículo
    *   Creado por: Rodrigo Boet
    *   Fecha: 08/07/2016
    *   @return $response Se regresan las contizaciones si existen
    */
   public function actionSeecotization()
   {
        //Se recolectan los datos del json
        $profile = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
        $vehicle = $profile->getMcTvehicles()->one()->fkvehicletype;
        $model = McTcotizaremp::find()->where(['fk_vehicletype' => $vehicle, 'send_all'=> 1, 'fkstatus' => 1])->all();
        $cotizacion = array();
        if(count($model)>0)
        {
            foreach ($model as $value) {
                $friendcompany = $value->getFkcompany0()->one()->getFklogin0()->one();
                $friendship = McTfriendship::find()->where(['fkuser_send'=>$friendcompany->id,'fkuser_recive'=>\Yii::$app->user->id])->one();
                $tload = $value->getMcTloadproposals()->one();
                if(count($friendship)>0 and !$tload)
                {
                    $empresa = $value->getFkcompany0()->one();
                    $data = array();
                    $data['pkcotizaremp'] = $value->pkcotizaremp;
                    $data['origen'] = $value->origen;
                    $data['destino'] = $value->destino;
                    $data['peso'] = $value->weight;
                    $data['payment'] = $value->payment;
                    $data['fecha'] = date('d-m-Y');
                    $data['comment'] = $value->comment;
                    $data['company_name'] = $empresa->company_rz;
                    $data['company_avatar'] = $empresa->company_avatar;
                    array_push($cotizacion,$data);
                }
            }
            if(count($cotizacion)>0)
            {
                $response = ['mensaje'=> 'Se encontraron las siguientes cotizaciones', 'validacion' => 'ok', 'cotizacion' => $cotizacion];
            }
            else
            {
                $response = ['mensaje'=> 'No hay cotizaciones disponibles', 'validacion' => 'ok'];
            }
        }
        else
        {
            $response = ['mensaje'=> 'No hay cotizaciones disponibles', 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite ver las cargas asignadas a un transportador por parte 
    *   de la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 08/07/2016
    *   @return $response Se regresan las solicitudes no aceptadas
    */
   public function actionSeeloadasigned()
   {
        //Se recolectan los datos del json
        $tload = McTloadproposal::find()->where(['fktransportador'=>\Yii::$app->user->id,'status'=>0,'hide'=>0])->all();
        $uload = McUloadproposal::find()->where(['fktransportador'=>\Yii::$app->user->id,'status'=>0,'hide'=>0])->all();
        $solicitudes = array();
        if(count($tload)>0 or count($uload)>0)
        {
            if(count($tload)>0)
            {
                foreach ($tload as $value) {
                    $cotizacion = $value->getFkcotizacionemp0()->where(['fkstatus' => 1])->one();
                    if(count($cotizacion)>0)
                    {
                        $empresa = $cotizacion->getFkcompany0()->one();
                        $data = array();
                        $data['type'] = 'tload';
                        $data['pkcotizaremp'] = $cotizacion->pkcotizaremp;
                        $data['origen'] = $cotizacion->origen;
                        $data['destino'] = $cotizacion->destino;
                        $data['peso'] = $cotizacion->weight;
                        $data['payment'] = $cotizacion->payment;
                        $data['fecha'] = date('d-m-Y');
                        $data['comment'] = $cotizacion->comment;
                        $data['company_name'] = $empresa->company_rz;
                        $data['company_avatar'] = $empresa->company_avatar;
                        array_push($solicitudes,$data);
                    }
                    
                }
            }
            if(count($uload)>0)
            {
                foreach ($uload as $value) {
                    $contrato = $value->getFkcontract0()->where(['fk_status' => 1])->one();
                    $carga = $contrato->getFkCarga()->one();
                    if(count($carga)>0)
                    {
                        $empresa = $contrato->getFkuserCompany()->one()->getMcTcompanies()->one();
                        $data = array();
                        $data['type'] = 'uload';
                        $data['pk_contract'] = $contrato->pk_contract;
                        $data['origen'] = $carga->load_citydestination;
                        $data['destino'] = $carga->load_cityorigin;
                        $data['peso'] = $carga->load_weight;
                        $data['payment'] = $value->payment;
                        $data['fecha'] = date('d-m-Y');
                        $data['comment'] = $carga->load_comment;
                        $data['company_name'] = $empresa->company_rz;
                        $data['company_avatar'] = $empresa->company_avatar;
                        array_push($solicitudes,$data);
                    }
                }
            }
            if(count($solicitudes)>0)
            {
                $response = ['mensaje'=> 'Se encontraron las siguientes solicitudes', 'validacion' => 'ok', 'cotizacion' => $solicitudes];
            }
            else
            {
                $response = ['mensaje'=> 'No hay cargas asignadas', 'validacion' => 'ok'];
            }
        }
        else
        {
            $response = ['mensaje'=> 'No hay solicitudes', 'validacion' => 'ok'];
        }
        return $response;
   }

    /*
    *   Funcion que permite enviar una propuesta a una cotizacion libre enviada por la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 08/07/2016
    *   @return $response Se regresan el estado de la solicitud
    */
   public function actionSendcotization($id_cot)
   {
        //Se recolectan los datos del json
        $friend = McTfriendship::find()->where(['fkuser_recive'=>\Yii::$app->user->id,'status'=>1])->one();
        if($friend)
        {  
            if(McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'fktransportador'=>\Yii::$app->user->id])->one())
            {
                $msg = "Ya se envío esta propuesta a la empresa";
                $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
            }
            else
            {
                $cotization = McTcotizaremp::find($id_cot)->where(['fkstatus'=>1])->one();
                $company = McTcompany::find()->where(['pkcompany'=>$cotization->fkcompany])->one();
                $transportador = McTuser::find()->where(['fklogin'=>\Yii::$app->user->id])->one();
                if(count($cotization)>0)
                {
                    $loadproposal = new McTloadproposal();
                    $loadproposal->fkcotizacionemp = $id_cot;
                    $loadproposal->fktransportador = \Yii::$app->user->id;
                    $loadproposal->status = 2;
                    $loadproposal->payment = $cotization->payment;
                    $loadproposal->save();
                    $msg = "Se envío la carga con éxito";
                    //Notificación
                    $notificacion = new McTnotificaciones();
                    $notificacion->fk_login=$company->fklogin;
                    $notificacion->msj_notificacion="El transportador ". $transportador->user_name ." envio una carga con exito <a href='/carga'> Ver carga</a>";
                    $notificacion->activa=1;
                    $notificacion->save();
                    $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
                }
                else
                {
                    $msg = "No existe esta cotización";
                    $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
                }
            }
        }
        else
        {
            $msg = 'No se puede envíar la solicitud a esta empresa';
            $response = ['mensaje'=> $msg, 'validacion' => 'error'];
        }
        return $response;
    }

    /*
    *   Funcion que permite aceptar un transportador en una cotización envíada
    *   de la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @param $id_cot Recibe el id de la cotizacion
    *   @return $response Se regresan las solicitudes no aceptadas
    */
    public function actionAcepttproposal($id_cot)
    {
        if(McTcontractemp::find()->where(['fkcotizaremp'=>$id_cot,'fkuser'=>\Yii::$app->user->id])->one())
        {
            $msg = 'Ya este usuario tiene un contrato';
            $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
        }
        else
        {
            $transportador = McTloadproposal::find()
            ->where(['fkcotizacionemp'=>$id_cot,'fktransportador'=>\Yii::$app->user->id])->one();
            $transportador->status = 1;
            $transportador->save();
            $msg = 'Se acepto la solicitud éxitosamente';
            //Se crea el contrato
            $contract = new McTcontractemp;
            $contract->fk_status = 1;
            $contract->fkcotizaremp = $id_cot;
            $contract->fkuser = \Yii::$app->user->id;
            $contract->save();
            //Se crea el estado de la carga
            $loadstatus = new McTloadstatus;
            $loadstatus->fkcontractemp = $contract->pk_contract;
            $loadstatus->fkstatus = 11;
            $loadstatus->save();
            //Se crea la instancia del chat
            $chat = new McTuserchattrans;
            $chat->fkcontractemp = $contract->pk_contract;
            $chat->save();
            $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
        }
        return $response; 
    }


    /*
    *   Funcion que permite aceptar un transportador en una cotización envíada
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @param $id_contract Recibe el id del contrato
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionAceptutproposal($id_contract)
    {
        if(McTcontractemp::find()->where(['fkcontract'=>$id_contract,'fkuser'=>\Yii::$app->user->id])->one())
        {
            $msg = 'Ya este usuario tiene un contrato';
            $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
        }
        else
        {
            $proposal = McUloadproposal::find()
            ->where(['fkcontract'=>$id_contract,'fktransportador'=>\Yii::$app->user->id])->one();
            $proposal->status = 1;
            $proposal->save();
            $msg = 'Se acepto la solicitud éxitosamente';
            //Se crea el contrato
            $contract = new McTcontractemp;
            $contract->fk_status = 1;
            $contract->fkcontract = $id_contract;
            $contract->fkuser = \Yii::$app->user->id;
            $contract->save();
            //Se crea el estado de la carga
            $loadstatus = new McTloadstatus;
            $loadstatus->fkcontractemp = $contract->pk_contract;
            $loadstatus->fkstatus = 11;
            $loadstatus->save();
            //Se crea la instancia del chat
            $chat = new McTuserchattrans;
            $chat->fkcontractemp = $contract->pk_contract;
            $chat->save();
            $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite aceptar un transportador en una cotización envíada
    *   Creado por: Rodrigo Boet
    *   Fecha: 09/07/2016
    *   @param $status Recibe el estado del contrato
    *   @return $response Regresa un mensaje con la validación
    */
    public function actionLoadcontracts($status)
    {
        $contracts = McTcontractemp::find()->where(['fkuser'=>\Yii::$app->user->id, 'fk_status' => $status])->all();
        $info = array();
        if(count($contracts)>0)
        {
            foreach ($contracts as $contract) {
                $inner_data = array();
                array_push($inner_data, $contract);
                if($contract->fkcontract)
                {
                    $load = $contract->getFkcontract0()->one()->getFkCarga()->one();
                    $emp = $contract->getFkcontract0()->one()->getFkProposal()->one()->getFkcompany0()->one();
                    $pay = McUloadproposal::find()->where(['fkcontract'=>$contract->pk_contract])->one();
                    $data = array();
                    $data['id_contract'] = $contract->pk_contract;
                    $data['origen'] = $load->load_citydestination;
                    $data['destino'] = $load->load_cityorigin;
                    $data['peso'] = $load->load_weight;
                    $data['payment'] = $pay->payment;
                    $data['fecha'] = date('d-m-Y');
                    $data['comment'] = $load->load_comment;
                    $data['latitude_origin'] = $load->latitude_origin;
                    $data['longitude_origin'] = $load->longitude_origin;
                    $data['latitude_destination'] = $load->latitude_destination;
                    $data['longitude_destination'] = $load->longitude_destination;
                    $data['company_name'] = $emp->company_rz;
                    $data['company_avatar'] = $emp->company_avatar;
                    $data['company_avatar'] = $emp->company_avatar;
                    $data['company_mail'] = $emp->fklogin0->email2;
                    $data['loadstatus'] = $contract->getMcTloadstatuses()->one()->fkstatus;
                    array_push($info, $data);
                }
                else
                {
                    $load = $contract->getFkcotizaremp0()->one();
                    $emp = $load->getFkcompany0()->one();
                    $data = array();
                    $data['id_contract'] = $contract->pk_contract;
                    $data['origen'] = $load->origen;
                    $data['destino'] = $load->destino;
                    $data['peso'] = $load->weight;
                    $data['payment'] = $load->payment;
                    $data['fecha'] = date('d-m-Y');
                    $data['latitude_origin'] = $load->origin_latitude;
                    $data['longitude_origin'] = $load->origin_longitude;
                    $data['latitude_destination'] = $load->destination_latitude;
                    $data['longitude_destination'] = $load->destination_longitude;
                    $data['company_name'] = $emp->company_rz;
                    $data['company_avatar'] = $emp->company_avatar;
                    $data['company_mail'] = $emp->fklogin0->email2;
                    $data['loadstatus'] = $contract->getMcTloadstatuses()->one()->fkstatus;
                    array_push($info, $data);
                }
            }
            $response = ['mensaje'=> 'Se encontraron los siguientes contratos', 'validacion' => 'ok',
            'contratos'=>$info];
        }
        else
        {
            $response = ['mensaje'=> 'No se encontraron contratos', 'validacion' => 'ok'];
        }
        return $response;
    }

    /*
    *   Funcion que permite enviar los pagos que debe hacer un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 10/07/2016
    *   @param $id_contract Recibe el id del contrato
    *   @return Regresa la respuesta con los pagos
    */
    public function actionTransporterpayment($id_contract)
    {
        $contrato = McTcontractemp::find()->where(['pk_contract'=>$id_contract,'fkuser'=>\Yii::$app->user->id])->one();
        if($contrato)
        {   
            $tpayment = McTtransporterpayment::find()
            ->where(['fkcontractemp'=>$contrato->pk_contract,'fkstatus'=>9])->one();
            if(count($tpayment)>0)
            {
                $pay = [];
                $pay['reference'] = $tpayment->reference;
                $pay['value'] = $tpayment->value;
                $msg = 'Se encontró el siguiente pago';
                $response = ['mensaje'=> $msg, 'validacion' => 'ok','pago'=>$pay];
            }
            else
            {
                $msg = 'No se encontraron pagos';
                $response = ['mensaje'=> $msg, 'validacion' => 'ok'];
            }
        }
        else
        {
            $response = ['mensaje'=> 'No existe el transportador en el contrato solicitado', 'validacion' => 'error'];
        }
        return $response;
    }
}
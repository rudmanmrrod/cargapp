<?php
namespace app\rest\modules\v1\controllers; 

use app\models\McTlogin;
use app\models\McTcompany;
use app\models\McTuserchat;
use app\models\McTuserchattrans;
use app\models\McTuserchatmsj;
use app\models\McTuserchatmsjtrans;
use app\models\McTnotificaciones;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Allow-Headers: Authorization, Lang, content-type");
header("Access-Control-Allow-Credentials: true");
/**
 * Class CompanyController
 * @package rest\versions\v1\controllers
 */
class ChatController extends Controller
{
    public $modelClass = "app\models\McTuserchattrans";
    /**
     * This method implemented to demonstrate the receipt of the token.
     * Do not use it on production systems.
     * @return string AuthKey or model with errors
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className(),],
        ];
    }
    

   /*
    *   Función que permite cargar un chat relacionado a un contrato entre empresa-transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 06/07/2016
    *   @return $response devuelve el chat y el total de mensajes
    */
   public function actionGetchattrans()
   {
        //Se recolectan los datos del json
        $post = \Yii::$app->request->post();
        //Se lidia con el metodo options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTuserchattrans::find()->where(['fkcontractemp'=>$post['contract']])->one();
        if($model)
        {
            if(!array_key_exists('offset',$post))
            {
                $post['offset'] = 0;
            }
            $chat = array();
            $total_msj =  count($model->getMcTuserchatmsjtrans()->all());
            $msjs = $model->getMcTuserchatmsjtrans()->limit((int)($post['limit']))->offset((int)($post['offset']))->orderBy(['date'=>SORT_DESC])->all();
            if(count($msjs)>0)
            {
                foreach ($msjs as $msj) {
                    $new_array = [];
                    $perfil='';
                    $yo = false;
                    $user = McTlogin::findOne(\Yii::$app->user->id);
                    if($msj->fkuser0->role==3)
                    {
                        $perfil = $msj->fkuser0->getMcTcompanies()->one()->company_rz;
                    }
                    else
                    {
                        $perfil = $msj->fkuser0->getMcTusers()->one()->user_name;
                    }
                    if(($user->role==3 and ($msj->fkuser0->id==$user->id)) or ($user->role==4 and ($msj->fkuser0->id==$user->id)))
                    {
                        $yo = true;
                    }
                    array_push($new_array, $perfil);
                    array_push($new_array, $yo);
                    array_push($new_array, $msj->message);
                    array_push($new_array, $msj->date);
                    $chat[] = $new_array;
                } 
                $response = ['mensaje'=> 'Se cargó el chat correctamente', 'validacion' => 'ok', 'chat' => $chat, 'total_msj' => $total_msj];
            }
            else
            {
                $response = ['mensaje'=> 'No hay mensajes para este chat', 'validacion' => 'ok', 'chat' => $chat];
            }
        }
        else
        {
            $response = ['mensaje'=>'Este chat no éxiste','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Función que permite enviar un mensaje a un chat
    *   Creado por: Rodrigo Boet
    *   Fecha: 06/07/2016
    *   @return $response devuelve el estado del mensaje
    */
   public function actionPostmsjtrans()
   {
        //Se recolectan los datos del json
        $post = \Yii::$app->request->post();
        //Se lidia con el metodo options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTuserchattrans::find()->where(['fkcontractemp'=>$post['contract']])->one();
        if($model)
        {
            $msj = new McTuserchatmsjtrans();
            $msj->fkuser = \Yii::$app->user->id;
            $msj->fkuserchattrans = $model->pkuserchat;
            $msj->message = $post['message'];
            if($msj->save())
            {
                $response = ['mensaje'=> 'Se envió el mensaje con éxito', 'validacion' => 'ok'];
                $user = McTlogin::findOne($msj->fkuser);
                $contract = $model->getFkcontractemp0()->one();
                //Se crea una notificación para el chat
                $chatnot = new McTnotificaciones();
                $chatnot->fkchattrans = $model->pkuserchat;
                if ($user->role==3)
                {
                    if(!McTnotificaciones::find()->where(['fk_login'=>$contract->fkuser,'fkchattrans'=>$model->pkuserchat,'activa'=>1])->one())
                    {
                        $chatnot->msj_notificacion="Tienes un nuevo mensaje en el chat";
                        $chatnot->fk_login = $contract->fkuser;
                        $chatnot->fktype = 6;
                    }
                }
                else
                {
                    $company_id = $contract->fkcotizaremp0->fkcompany0->fklogin0->id;
                    if(!McTnotificaciones::find()->where(['fk_login'=>$company_id,'fkchattrans'=>$model->pkuserchat,'activa'=>1])->one())
                    {
                        if($contract->fkcontract0)
                        {
                            $chatnot->msj_notificacion="Tienes un nuevo mensaje en el chat <a href='".\Yii::getAlias('@web')."/../accountc/transporterchat?id_contract=".$contract->fkcontract0."&id_transcontract=".$contract->pk_contract."&action=usertcontract' onclick='modalClick(event)'> Ver Chat</a>";
                        }
                        else
                        {
                            $chatnot->msj_notificacion="Tienes un nuevo mensaje en el chat <a href='".\Yii::getAlias('@web')."/../accountc/transporterchat?id_contract=".$contract->fkcotizaremp0."&id_transcontract=".$contract->pk_contract."&action=seecotization' onclick='modalClick(event)'> Ver Chat</a>";
                        }
                        $chatnot->fk_login = $company_id;
                    }
                }
                $chatnot->save();
            }
            else
            {
                $response = ['mensaje'=> 'Ocurrió un error', 'validacion' => 'error', 'error'=>$msj->error()];
            }
            
        }
        else
        {
            $response = ['mensaje'=>'Este chat no éxiste','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Función que permite cargar un chat relacionado a un contrato entre empresa-transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 31/07/2016
    *   @return $response devuelve el chat y el total de mensajes
    */
   public function actionGetchat()
   {
        //Se recolectan los datos del json
        $post = \Yii::$app->request->post();
        //Se lidia con el metodo options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTuserchat::find()->where(['fkcontract'=>$post['contract']])->one();
        if($model)
        {
            if(!array_key_exists('offset',$post))
            {
                $post['offset'] = 0;
            }
            $chat = array();
            $total_msj =  count($model->getMcTuserchatmsjs()->all());
            $msjs = $model->getMcTuserchatmsjs()->limit((int)($post['limit']))->offset((int)($post['offset']))->orderBy(['date'=>SORT_DESC])->all();
            if(count($msjs)>0)
            {
                foreach ($msjs as $msj) {
                    $new_array = [];
                    $perfil='';
                    $yo = false;
                    $user = McTlogin::findOne(\Yii::$app->user->id);
                    if($msj->fkuser0->role==3)
                    {
                        $perfil = $msj->fkuser0->getMcTcompanies()->one()->company_rz;
                    }
                    else
                    {
                        $perfil = $msj->fkuser0->getMcTusers()->one()->user_name;
                    }
                    if(($user->role==3 and ($msj->fkuser0->id==$user->id)) or ($user->role==2 and ($msj->fkuser0->id==$user->id)))
                    {
                        $yo = true;
                    }
                    array_push($new_array, $perfil);
                    array_push($new_array, $yo);
                    array_push($new_array, $msj->message);
                    array_push($new_array, $msj->date);
                    $chat[] = $new_array;
                } 
                $response = ['mensaje'=> 'Se cargó el chat correctamente', 'validacion' => 'ok', 'chat' => $chat, 'total_msj' => $total_msj];
            }
            else
            {
                $response = ['mensaje'=> 'No hay mensajes para este chat', 'validacion' => 'ok', 'chat' => $chat];
            }
        }
        else
        {
            $response = ['mensaje'=>'Este chat no éxiste','validacion'=>'error'];
        }
        return $response;
    }

    /*
    *   Función que permite enviar un mensaje a un chat
    *   Creado por: Rodrigo Boet
    *   Fecha: 31/07/2016
    *   @return $response devuelve el estado del mensaje
    */
   public function actionPostmsj()
   {
        //Se recolectan los datos del json
        $post = \Yii::$app->request->post();
        //Se lidia con el metodo options
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { header("HTTP/1.1 200 OK"); die(); }
        $model = McTuserchat::find()->where(['fkcontract'=>$post['contract']])->one();
        if($model)
        {
            $msj = new McTuserchatmsj();
            $msj->fkuser = \Yii::$app->user->id;
            $msj->fkuserchat = $model->pkuserchat;
            $msj->message = $post['message'];
            if($msj->save())
            {
                $response = ['mensaje'=> 'Se envió el mensaje con éxito', 'validacion' => 'ok'];
                $user = McTlogin::findOne($msj->fkuser);
                $contract = $model->getFkcontract0()->one();
                //Se crea una notificación para el chat
                $chatnot = new McTnotificaciones();
                $chatnot->fkchat = $model->pkuserchat;
                if ($user->role==2)
                {
                    if(!McTnotificaciones::find()->where(['fk_login'=>$contract->fkuser_company,'fkchat'=>$model->pkuserchat,'activa'=>1])->one())
                    {
                        $chatnot->msj_notificacion="Tienes un nuevo mensaje en el chat <a href='".\Yii::getAlias('@web')."/../web/accountc/userucontract?id_contract=".$contract->pk_contract."' onclick='modalClick(event)'> Ver Chat</a>";
                        $chatnot->fk_login = $contract->fkuser_company;
                    }
                }
                else
                {
                    $user_id = $contract->fkCarga->fkuser0->fklogin0->id;
                    if(!McTnotificaciones::find()->where(['fk_login'=>$user_id,'fkchat'=>$model->pkuserchat,'activa'=>1])->one())
                    {
                        $chatnot->msj_notificacion="Tienes un nuevo mensaje en el chat <a href='".\Yii::getAlias('@web')."/../web/account/enterproposal/".$contract->fk_proposal."' onclick='modalClick(event)'> Ver Chat</a>";
                        $chatnot->fk_login = $user_id;
                    }
                }
                $chatnot->save();
            }
            else
            {
                $response = ['mensaje'=> 'Ocurrió un error', 'validacion' => 'error', 'error'=>$msj->error()];
            }
            
        }
        else
        {
            $response = ['mensaje'=>'Este chat no éxiste','validacion'=>'error'];
        }
        return $response;
    }
}
 
<?php

namespace rest\modules\v1\controllers;

use common\models\User; 
use frontend\models\PasswordResetRequestForm;
use yii\rest\Controller;
use yii\web\Response; 
use yii\filters\ContentNegotiator;

header("Access-Control-Allow-Origin: *");

/**
 * Class UserController
 * @package rest\versions\v1\controllers
 */
class PasswordresetController extends Controller
{
    public $modelClass = 'frontend\models\PasswordResetRequestForm';

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /*
    *   Funcion para resetear contraseñas en la aplicación
    *   Creado por: Rodrigo Da Costa
    *   Fecha: 18/01/2016
    */
    public function actionReset()
    {
    	$model = new PasswordResetRequestForm();
    	$data = \Yii::$app->request->post();
    	$model->email = $data['email'];
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if($model->sendEmail())
    	{
    		$response = ['mensaje'=>'Se envio el correo para resetear la contraseña','validacion'=>'ok'];
    		return $response;
    	}
    	else
    	{
    		$response = ['mensaje'=>'Ocurrió un error','validacion'=>'error'];
    		return $response;
    	}
    }
}
<?php

use yii\web\JsonParse;

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/../../config/params.php')
);



$db     = require(__DIR__ . '/../../config/db.php');

return [
    'id' => 'basic',
    'basePath' => dirname(__DIR__).'/..',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'app\rest\modules\v1\Module',
            'controllerNamespace' => 'app\rest\modules\v1\controllers' 
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],      
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => ['user','v1/account',
                    'v1/friendship','v1/update','v1/company', 'v1/news', 'v1/chat',
                    'v1/cotizar','v1/load','v1/profile', 'v1/notificaciones']
                ],
                'OPTIONS v1/user/login' => 'v1/user/login',
                'POST v1/user/login' => 'v1/user/login',
                'OPTIONS v1/account/create' => 'v1/account/create',
                'POST v1/account/create' => 'v1/account/create',
                'OPTIONS v1/account/recover' => 'v1/account/recover',
                'POST v1/account/recover' => 'v1/account/recover',
                'GET v1/friendship/friend' => 'v1/friendship/friend',
                'GET v1/friendship/sendsolicitude' => 'v1/friendship/sendsolicitude',
                'GET v1/friendship/deletesolicitude' => 'v1/friendship/deletesolicitude',
                'GET v1/company/list' => 'v1/company/list',
                'GET v1/company/friend' => 'v1/company/friend',
                'GET v1/company/see' => 'v1/company/see',
                'GET v1/news/list' => 'v1/news/list',
                'POST v1/chat/getchattrans' => 'v1/chat/getchattrans',
                'OPTIONS v1/chat/getchattrans' => 'v1/chat/getchattrans',
                'POST v1/chat/postmsjtrans' => 'v1/chat/postmsjtrans',
                'OPTIONS v1/chat/postmsjtrans' => 'v1/chat/postmsjtrans',
                'POST v1/chat/getchat' => 'v1/chat/getchat',
                'OPTIONS v1/chat/getchat' => 'v1/chat/getchat',
                'POST v1/chat/postmsj' => 'v1/chat/postmsj',
                'OPTIONS v1/chat/postmsj' => 'v1/chat/postmsj',
                'GET v1/cotizar/seecotization' => 'v1/cotizar/seecotization',
                'GET v1/cotizar/sendcotization' => 'v1/cotizar/sendcotization',
                'GET v1/cotizar/seeloadasigned' => 'v1/cotizar/seeloadasigned',
                'GET v1/cotizar/acepttproposal' => 'v1/cotizar/acepttproposal',
                'GET v1/cotizar/aceptutproposal' => 'v1/cotizar/aceptutproposal',
                'GET v1/cotizar/loadcontracts' => 'v1/cotizar/loadcontracts',
                'GET v1/cotizar/transporterpayment' => 'v1/cotizar/transporterpayment',
                'POST v1/load/reportlocation' => 'v1/load/reportlocation',
                'OPTIONS v1/load/reportlocation' => 'v1/load/reportlocation',
                'POST v1/load/reportloadstatus' => 'v1/load/reportloadstatus',
                'OPTIONS v1/load/reportloadstatus' => 'v1/load/reportloadstatus',
                'POST v1/load/reportloadend' => 'v1/load/reportloadend',
                'OPTIONS v1/load/reportloadend' => 'v1/load/reportloadend',
                'GET v1/profile/seeprofile' => 'v1/profile/seeprofile',
                'POST v1/profile/update' => 'v1/profile/update',
                'OPTIONS v1/profile/update' => 'v1/profile/update',
                'GET v1/notificaciones/notificacion' => 'v1/notificaciones/notificacion',
                'GET v1/notificaciones/changestatus' => 'v1/notificaciones/changestatus',
                'GET v1/notificaciones/getnots' => 'v1/notificaciones/getnots',
                'GET v1/notificaciones/readnots' => 'v1/notificaciones/readnots',
                'GET v1/notificaciones/cotization' => 'v1/notificaciones/cotization',
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

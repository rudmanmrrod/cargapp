$(document).ready(function(){
	$('.notification-icon a').click(function(event){
		event.preventDefault();
		if($('#before-not').css('display')!='none')
		{
			$('#before-not').css('display','none');
			$('#inside-not').html('');
		}
		else
		{
			$('#before-not').css('display','');
			if(mynots.length!=0)
			{
				$.each(mynots,function(index,value){
					$('#inside-not').append('<div id="not"><div style="padding:2px 2px 5px 2px;" id="'+value.pk_notificaciones+'">'+value.msj_notificacion+'</div><hr style="border-width:1px;border-color: gray;padding:none;margin-top:0px;margin-bottom:0px;"></div>')
				});
			}
			else
			{
				$('#inside-not').append('<div id="not"><div style="padding:2px 2px 5px 2px;"><p>No tiene notificaciones pendientes</p></div><hr style="border-width:1px;border-color: gray;padding:none;margin-top:0px;margin-bottom:0px;"></div>');
			}
		}
	});
});

function modalClick(event)
{
	event.preventDefault();
	var notid = $(event.path[0]).parent().attr('id');
	var newUrl = '/'+url.split('/')[1]+'/rest/v1/notificaciones/changestatus';
	var params = '?id='+notid+'&access-token='+ak;
	$.get(newUrl+params)
      .success(function(data){
        window.location.href = event.target;   
      })
      .error(function(data,estado){
      	alert("Ocurrió un error en el servidor");
      });
}

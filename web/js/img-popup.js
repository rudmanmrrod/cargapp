$(document).ready(function(){
	$('.img-class').after('<a class="my-link">');
	$('.img-class').detach().appendTo('.my-link');
	$('.my-link').attr('href',$('.img-class').attr('src'));
	$('.my-link').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
});
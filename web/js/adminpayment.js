$(document).ready(function(){
  var pago_total=0;
  var pago_comision=0;
  var pago_pendiente=0;
  $('.block').each(function(){
    var estado = $(this).find('.estado');
    if($(estado).text()=='Abonado')
    {
      var pagominimo = $(this).find('.pagominimo');
      var comision = $(this).find('.comision');
      pago_comision+= parseFloat($(comision).text().replace(',','').replace('COP',''));
      pago_total+= parseFloat($(pagominimo).text().replace(',','').replace('COP',''));
    }
    var pagar = $(this).find('.pagar');
    if(pagar.length!=0)
    {
      pago_pendiente+= parseFloat($(pagar).text().replace(',','').replace('COP',''));
    }
  });
  $('.abono_total').text(pago_total+' COP');
  $('.comision_total').text(pago_comision+' COP');
  $('.pagos_total').text(pago_pendiente+' COP');
});
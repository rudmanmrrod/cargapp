$('.srch').click(function(event){
  $('.block').each(function(index,value){
    $(this).show();
  });
  $('.srch').each(function(index,value){
    $(value).removeClass('btn-active');
    $(value).addClass('btn-desactive');
  });
  $(this).removeClass('btn-desactive');
  $(this).addClass('btn-active');
  $('.block').parent().find('.not-found').remove();
  var text = $(this).attr('id');
  if(text)
  {
    $('.block').each(function(index,value){
      var bool = false;
      ($(value).find('.status').text()===text) ? bool = true: bool = false;
      if(!bool)
      {
        $(this).hide();
      }
    });
  }
  else
  {
      $('.block').each(function(index,value){
        $(this).show();
    });
  }
  var bool=true;
  $('.block').each(function(index,value){
    $(this).attr('style')!="display: none;" ? bool=false:'';
  });
  if(bool)
  {
    var text ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
    text+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
    text+='</h1></div></div>';
    $('.block').parent().append(text);
  }
});
var app = angular.module("MyApp.Controllers");

app.controller("ChatTransporter",function($scope,$http){
	$scope.at = ak;
	$scope.limit = 10;
	$scope.contract = contractemp;
	$scope.textMsj = "";
    $scope.yo = "";

    /*	Servicio entre empresa transportador	*/
   	/*Válida si el mensaje esta vacío para luego enviarlo o no*/
    $scope.send = function (){
      if (this.textMsj == "") return false;
      this.sendService(this.textMsj);
    };

    /*Obtiene el servicio con los msj*/
    $scope.getMsjsService = function(){
		var parametros = {
	    	"contract": $scope.contract,
	    	"limit": $scope.limit
	    }
	  	$http.post('http://localhost/cargapp/rest/v1/chat/getchattrans?access-token='+$scope.at,parametros)
	    .success(function(data) {
	      $scope.mensajes = data.chat;
	      for (var i = 0; i <= data.chat.length; i++) {
	        if (data.chat[i][1] == true){
	          $scope.yo = data.chat[i][0];
	          break;
	        }
	      }
	    })
	    .error(function(error,status,config,headers){
			console.log(error);
		});;
    };
    $scope.getMsjsService();

    /*Envia el mensaje al servidor*/
    $scope.sendService = function(mensaje){
      		var parametros = {
	    	"contract": $scope.contract,
	    	"message": mensaje
	    }
	  	$http.post('http://localhost/cargapp/rest/v1/chat/postmsjtrans?access-token='+$scope.at,parametros)
        .success(function(data) {
          console.log(data);
          $scope.getMsjsService();
          $scope.textMsj="";
      });
      // setTimeout($scope.getMsjsService(), 3000);
    };
});
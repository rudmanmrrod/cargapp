<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_thideload".
 *
 * @property string $pkhideload
 * @property string $fkcompany
 * @property string $fkload
 *
 * @property McTcompany $fkcompany0
 * @property McTcarga $fkload0
 */
class McThideload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_thideload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcompany', 'fkload'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkhideload' => 'Pkhideload',
            'fkcompany' => 'Fkcompany',
            'fkload' => 'Fkload',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcompany0()
    {
        return $this->hasOne(McTcompany::className(), ['pkcompany' => 'fkcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkload0()
    {
        return $this->hasOne(McTcarga::className(), ['pkload' => 'fkload']);
    }
}

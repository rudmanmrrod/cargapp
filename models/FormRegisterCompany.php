<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;

class FormRegisterCompany extends Model{
 
    public $rz;   
    public $nit;
    public $name;
    public $address;
    public $email;
    public $phone;
    public $password;
    public $password_repeat;
    public $avatar;
    public $type;
    public $user;
    public $review;
    public $bank;
    public $typeaccount;
    public $numberaccount;
    public $nameaccount;
    public $mcTsubcategories;
    public $mcTpercent;
    
    public function rules()
    {
        return [
            ['rz', 'required', 'message'=> 'Ingresa la razón social'],
            ['nit', 'required', 'message'=> 'Ingresa el NIT'],
            ['name', 'required', 'message'=> 'Ingresa nombre del encargado'],
            ['address', 'required', 'message'=> 'Ingresa la dirección'],
            ['review', 'required', 'message'=> 'Ingresa una corta reseña sobre la empresa'],
            ['review', 'match', 'pattern' => "/^.{20,80}$/", 'message' => 'Mínimo 20 y máximo 80 caracteres'],
            ['email', 'required', 'message'=> 'Ingresa el correo'],
            ['bank', 'required', 'message'=> 'Seleccione el banco'],
            ['typeaccount', 'required', 'message'=> 'Seleccione el tipo de cuenta'],
            ['numberaccount', 'required', 'message'=> 'Ingresa el número de cuenta'],
            ['mcTsubcategories', 'required', 'message'=> 'Ingresa al menos una categoria'],
            ['mcTpercent', 'required', 'message'=> 'Ingresa el porcentaje de las categorias'],
            ['bank', 'required', 'message'=> 'Seleccione el banco'],
            ['user', 'required', 'message'=> 'Asigna un nombre de usuario'],
            ['nameaccount', 'required', 'message'=> 'Asigna un titular a la cuenta'],
            ['password', 'required', 'message'=> 'Ingresa una contraseña'],
            ['password_repeat', 'required', 'message'=> 'Ingresa nuevamente la contraseña'],
            ['phone', 'required', 'message'=> 'Ingresa tu número de contácto'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'El correo ingresado no tiene un formato valido'],
            ['email', 'email_existe'],
            ['user', 'user_existe'],
            ['avatar','image'],
            ['password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
            ['phone', 'number'],
            ['type', 'number']
        ];
    }
    
    public function email_existe($attribute, $params)
    {
      $table = Users::find()->where("email2=:email", [":email" => $this->email]);
      if ($table->count() == 1)
        $this->addError($attribute, "Esta cuenta de email ya se encuentra registrada");
    }

    public function user_existe($attribute, $params)
    {
      $table = Users::find()->where("email=:email", [":email" => $this->user]);
      if ($table->count() == 1)
        $this->addError($attribute, "Este usuario ya esta en uso");
    }
 
}
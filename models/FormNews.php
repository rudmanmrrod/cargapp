<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormNews extends Model{
 
    public $title;   
    public $message;
    public $image;
    public $type;
    
    public function rules()
    {
        return [
            ['title', 'required', 'message'=> 'Ingresa el título'],
            ['message', 'required', 'message'=> 'Ingresa el mensaje'],
            ['type','required','message'=>'Ingresa el tipo de noticia'],
            ['image','image']
        ];
    }
 
}
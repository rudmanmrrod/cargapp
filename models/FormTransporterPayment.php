<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormTransporterPayment extends Model{
 
    public $payment;   
    public $plate;

    public function rules()
    {
        return [
            ['payment', 'required', 'message'=> 'Debes especificar el monto a pagar'],
            ['plate', 'required', 'message'=> 'Debes ingresar una placa'],
        ];
    }
 
}
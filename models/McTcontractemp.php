<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcontractemp".
 *
 * @property integer $pk_contract
 * @property string $fkcontract
 * @property string $signature_time
 * @property integer $fk_status
 * @property integer $fkcotizaremp
 * @property integer $fkuser
 *
 * @property McTcontract $fkcontract0
 * @property McTcotizaremp $fkcotizaremp0
 * @property McTlogin $fkuser0
 * @property McStatus $fkStatus
 * @property McTloadstatus[] $mcTloadstatuses
 * @property McTtransporterpayment[] $mcTtransporterpayments
 */
class McTcontractemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcontractemp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontract', 'fk_status', 'fkcotizaremp', 'fkuser'], 'integer'],
            [['signature_time'], 'safe'],
            [['fk_status', 'fkuser'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_contract' => 'Pk Contract',
            'fkcontract' => 'Fkcontract',
            'signature_time' => 'Signature Time',
            'fk_status' => 'Fk Status',
            'fkcotizaremp' => 'Fkcotizaremp',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontract0()
    {
        return $this->hasOne(McTcontract::className(), ['pk_contract' => 'fkcontract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcotizaremp0()
    {
        return $this->hasOne(McTcotizaremp::className(), ['pkcotizaremp' => 'fkcotizaremp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStatus()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fk_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTloadstatuses()
    {
        return $this->hasMany(McTloadstatus::className(), ['fkcontractemp' => 'pk_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTtransporterpayments()
    {
        return $this->hasMany(McTtransporterpayment::className(), ['fkcontractemp' => 'pk_contract']);
    }
}

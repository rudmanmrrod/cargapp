<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tadminlog".
 *
 * @property integer $id
 * @property integer $fklogin
 * @property string $message
 * @property string $date
 *
 * @property McTlogin $fklogin0
 */
class McTadminlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tadminlog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fklogin', 'message'], 'required'],
            [['fklogin'], 'integer'],
            [['message'], 'string'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fklogin' => 'Fklogin',
            'message' => 'Message',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFklogin0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fklogin']);
    }
}

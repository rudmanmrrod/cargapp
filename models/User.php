<?php

namespace app\models;

use Yii;
use yii\helpers\Time;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    
    public $id;
    public $email;
    public $email2;
    public $password;
    public $authKey;
    public $accessToken;
    public $activate;
    public $verification_code;
    public $role;
    public $last_connection;

    public static function isAdmin($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'role' => 1]))
            return true;
        else
            return false;
    }

    public static function isUser($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'role' => 2]))
            return true;
        else
            return false;
    }

    public static function isCompany($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'role' => 3]))
            return true;
        else
            return false;
    }

    public static function validateRole(){
        $user = Users::findOne(Yii::$app->user->id);
        Yii::$app->session->set('ak',$user->accessToken);
        if(User::isAdmin(Yii::$app->user->identity->id))
            return "/admin";
        else 
            if(User::isUser(Yii::$app->user->identity->id))
                return "/account";
            else
                return "/accountc";
    }

    /**
     * @inheritdoc
     */
    
    /* busca la identidad del usuario a través de su $id */

    public static function findIdentity($id)
    {
        
        $user = Users::find()
                ->where("activate=:activate", [":activate" => 1])
                ->andWhere("id=:id", ["id" => $id])
                ->one();
        
        return isset($user) ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    
    /* Busca la identidad del usuario a través de su token de acceso */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        
        $users = Users::find()
                ->where("activate=:activate", [":activate" => 1])
                ->andWhere("accessToken=:accessToken", [":accessToken" => $token])
                ->all();
        
        foreach ($users as $user) {
            if ($user->accessToken === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    
    /* Busca la identidad del usuario a través del username */
    public static function findByUsername($username)
    {
        $users = Users::find()
                ->where("activate=:activate", ["activate" => 1])
                ->andWhere("email=:username", [":username" => $username])
                ->all();
        
        foreach ($users as $user) {
            if (strcasecmp($user->email, $username) === 0) {
                $now = new \DateTime('now', new \DateTimeZone('UTC'));
                $user->last_connection = $now->format('Y-m-d H:i:s');
                $user->save();
                if($user->role == 3){
                    $tmpuser = $user->getMcTcompanies()->One();
                    Yii::$app->session->set('userid', $tmpuser->pkcompany);
                    Yii::$app->session->set('name', $tmpuser->company_rz);
                }else{
                    $tmpuser = $user->getMcTusers()->One();
                    Yii::$app->session->set('userid', $tmpuser->pkuser);
                    Yii::$app->session->set('name', $tmpuser->user_name);
                }
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    
    /* Regresa el id del usuario */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    
    /* Regresa la clave de autenticación */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /* Regresa la clave de acceso */
    public function getAccessKey()
    {
        return $this->accessToken;
    }

    /**
     * @inheritdoc
     */
    
    /* Valida la clave de autenticación */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        /* Valida el password */
        if (crypt($password, $this->password) == $this->password)
        {
        return $password === $password;
        }
    }
}
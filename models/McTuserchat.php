<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tuserchat".
 *
 * @property integer $pkuserchat
 * @property string $fkcontract
 *
 * @property McTcontract $fkcontract0
 * @property McTuserchatmsj[] $mcTuserchatmsjs
 */
class McTuserchat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tuserchat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontract'], 'required'],
            [['fkcontract'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkuserchat' => 'Pkuserchat',
            'fkcontract' => 'Fkcontract',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontract0()
    {
        return $this->hasOne(McTcontract::className(), ['pk_contract' => 'fkcontract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTuserchatmsjs()
    {
        return $this->hasMany(McTuserchatmsj::className(), ['fkuserchat' => 'pkuserchat']);
    }
}

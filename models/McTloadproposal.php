<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tloadproposal".
 *
 * @property integer $pkloadproposal
 * @property integer $fkcotizacionemp
 * @property integer $fktransportador
 * @property integer $status
 * @property integer $hide
 * @property string $payment
 *
 * @property McTcotizaremp $fkcotizacionemp0
 * @property McTlogin $fktransportador0
 */
class McTloadproposal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tloadproposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcotizacionemp', 'fktransportador', 'payment'], 'required'],
            [['fkcotizacionemp', 'fktransportador', 'status', 'hide', 'payment'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkloadproposal' => 'Pkloadproposal',
            'fkcotizacionemp' => 'Fkcotizacionemp',
            'fktransportador' => 'Fktransportador',
            'status' => 'Status',
            'hide' => 'Hide',
            'payment' => 'Payment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcotizacionemp0()
    {
        return $this->hasOne(McTcotizaremp::className(), ['pkcotizaremp' => 'fkcotizacionemp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFktransportador0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fktransportador']);
    }
}

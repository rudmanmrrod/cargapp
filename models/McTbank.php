<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tbank".
 *
 * @property integer $pkbank
 * @property string $bank_name
 *
 * @property McTcompany[] $mcTcompanies
 */
class McTbank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tbank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkbank' => 'Pkbank',
            'bank_name' => 'Bank Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcompanies()
    {
        return $this->hasMany(McTcompany::className(), ['fkbank' => 'pkbank']);
    }
}

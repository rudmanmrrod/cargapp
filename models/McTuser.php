<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tuser".
 *
 * @property integer $pkuser
 * @property string $user_name
 * @property string $user_phone
 * @property string $user_avatar
 * @property string $user_createat
 * @property integer $fklogin
 *
 * @property McTattachments[] $mcTattachments
 * @property McTcarga[] $mcTcargas
 * @property McThideload[] $mcThideloads
 * @property McTproposal[] $mcTproposals
 * @property McTreferences[] $mcTreferences
 * @property McTlogin $fklogin0
 * @property McTvehicle[] $mcTvehicles
 */
class McTuser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tuser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_phone', 'user_avatar', 'fklogin'], 'required'],
            [['user_avatar'], 'string'],
            [['user_createat'], 'safe'],
            [['fklogin'], 'integer'],
            [['user_name'], 'string', 'max' => 50],
            [['user_phone'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkuser' => 'Pkuser',
            'user_name' => 'User Name',
            'user_phone' => 'User Phone',
            'user_avatar' => 'User Avatar',
            'user_createat' => 'User Createat',
            'fklogin' => 'Fklogin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTattachments()
    {
        return $this->hasMany(McTattachments::className(), ['fkuser' => 'pkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcargas()
    {
        return $this->hasMany(McTcarga::className(), ['fkuser' => 'pkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcThideloads()
    {
        return $this->hasMany(McThideload::className(), ['fkuser' => 'pkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTproposals()
    {
        return $this->hasMany(McTproposal::className(), ['fkuser' => 'pkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTreferences()
    {
        return $this->hasMany(McTreferences::className(), ['fkuser' => 'pkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFklogin0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fklogin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTvehicles()
    {
        return $this->hasMany(McTvehicle::className(), ['fkuser' => 'pkuser']);
    }
}

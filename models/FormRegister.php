<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;

class FormRegister extends Model{
 
    public $name;   
    public $email;
    public $phone;
    public $password;
    public $password_repeat;
    public $avatar;
    public $type;
    public $user;
    
    public function rules()
    {
        return [
            ['name', 'required', 'message'=> 'Ingresa tu nombre'],
            ['email', 'required', 'message'=> 'Ingresa tu correo'],
            ['password', 'required', 'message'=> 'Ingresa una contraseña'],
            ['type', 'required', 'message'=> 'Selecciona un perfil'],
            ['password_repeat', 'required', 'message'=> 'Ingresa nuevamente la contraseña'],
            ['phone', 'required', 'message'=> 'Ingresa tu número de contácto'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'El correo ingresado no tiene un formato valido'],
            ['email', 'email_existe'],
            ['user', 'user_existe'],
            ['avatar','image'],
            ['password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
            ['phone', 'number'],
            ['type', 'number'],
            [['name','email','password','password_repeat','phone','avatar','type'],'safe']
        ];
    }
    
    public function email_existe($attribute, $params)
    {
      $table = Users::find()->where("email2=:email", [":email" => $this->email]);
      if ($table->count() == 1)
        $this->addError($attribute, "Esta cuenta de email ya se encuentra registrada");
    }

    public function user_existe($attribute, $params)
    {
      $table = Users::find()->where("email=:email", [":email" => $this->user]);
      if ($table->count() == 1)
        $this->addError($attribute, "Este usuario ya esta en uso");
    }
 
}
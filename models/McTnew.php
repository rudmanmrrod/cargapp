<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tnew".
 *
 * @property integer $pknew
 * @property string $new_title
 * @property string $new_message
 * @property string $new_image
 * @property integer $new_type
 * @property integer $fkuser
 * @property string $new_date
 *
 * @property McTuser $fkuser0
 */
class McTnew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tnew';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_message', 'new_image'], 'string'],
            [['new_type', 'fkuser'], 'integer'],
            [['fkuser'], 'required'],
            [['new_date'], 'safe'],
            [['new_title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pknew' => 'Pknew',
            'new_title' => 'New Title',
            'new_message' => 'New Message',
            'new_image' => 'New Image',
            'new_type' => 'New Type',
            'fkuser' => 'Fkuser',
            'new_date' => 'New Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }
}

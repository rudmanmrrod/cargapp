<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;

class FormUpdateU extends Model{
 
    public $id;
    public $name;   
    public $email;
    public $phone;
    public $password;
    public $password_repeat;
    public $avatar;
    
    public function rules()
    {
        return [
            ['id', 'required', 'message'=> 'No se puede procesar la solicitud'],
            ['name', 'required', 'message'=> 'Ingresa tu nombre'],
            ['email', 'required', 'message'=> 'Ingresa tu correo'],
            ['phone', 'required', 'message'=> 'Ingresa tu número de contácto'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'El correo ingresado no tiene un formato valido'],
            ['email', 'email_valido'],
            ['avatar','image'],
            ['password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
            ['phone', 'number'],
        ];
    }
    
    public function email_valido($attribute, $params)
    {
      $table = Users::find()
        ->where("email2=:email", [":email" => $this->email])
        ->andWhere("id=:id", [":id" => Yii::$app->user->identity->id]);
      if ($table->count() == 0){
        $table = Users::find()->where("email2=:email", [":email" => $this->email]);
        if ($table->count() == 1)
            $this->addError($attribute, "Esta cuenta de email ya se encuentra registrada");
      }
    }
 
}
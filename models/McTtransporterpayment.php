<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_ttransporterpayment".
 *
 * @property integer $pktpayment
 * @property string $reference
 * @property integer $value
 * @property integer $fkstatus
 * @property integer $fkcontractemp
 *
 * @property McTcontractemp $fkcontractemp0
 * @property McStatus $fkstatus0
 */
class McTtransporterpayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_ttransporterpayment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reference', 'value', 'fkstatus', 'fkcontractemp'], 'required'],
            [['value', 'fkstatus', 'fkcontractemp'], 'integer'],
            [['reference'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pktpayment' => 'Pktpayment',
            'reference' => 'Reference',
            'value' => 'Value',
            'fkstatus' => 'Fkstatus',
            'fkcontractemp' => 'Fkcontractemp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontractemp0()
    {
        return $this->hasOne(McTcontractemp::className(), ['pk_contract' => 'fkcontractemp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }
}

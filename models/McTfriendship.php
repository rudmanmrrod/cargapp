<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tfriendship".
 *
 * @property integer $pkfriendship
 * @property integer $fkuser_send
 * @property integer $fkuser_recive
 * @property string $send_date
 * @property integer $status
 *
 * @property McTlogin $fkuserRecive
 * @property McTlogin $fkuserSend
 */
class McTfriendship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tfriendship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser_send', 'fkuser_recive', 'send_date'], 'required'],
            [['fkuser_send', 'fkuser_recive', 'status'], 'integer'],
            [['send_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkfriendship' => 'Pkfriendship',
            'fkuser_send' => 'Fkuser Send',
            'fkuser_recive' => 'Fkuser Recive',
            'send_date' => 'Send Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuserRecive()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser_recive']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuserSend()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser_send']);
    }
}

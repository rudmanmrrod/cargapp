<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tlogin".
 *
 * @property integer $id
 * @property string $email
 * @property string $email2
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property string $activate
 * @property string $verification_code
 * @property integer $role
 * @property string $last_connection
 *
 * @property McTcompany[] $mcTcompanies
 * @property McTrol $role0
 * @property McTuser[] $mcTusers
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tlogin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role'], 'required'],
            [['role'], 'integer'],
            [['last_connection'], 'safe'],
            [['email', 'email2'], 'string', 'max' => 80],
            [['password', 'authKey', 'accessToken', 'activate', 'verification_code'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'email2' => 'Email2',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'activate' => 'Activate',
            'verification_code' => 'Verification Code',
            'role' => 'Role',
            'last_connection' => 'Last Connection',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcompanies()
    {
        return $this->hasMany(McTcompany::className(), ['fklogin' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(McTrol::className(), ['pkrol' => 'role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTusers()
    {
        return $this->hasMany(McTuser::className(), ['fklogin' => 'id']);
    }
}

<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormCotizarEmp extends Model{
 
    public $origin;   
    public $destination;
    public $fk_vehicletype;
    public $weight;
    public $payment;
    public $vehicle_number;
    public $comment;
    public $latitude_orig;
    public $longitude_orig;
    public $latitude_dest;
    public $longitude_dest;
    public $alltcheck;

    public function rules()
    {
        return [
            ['origin', 'required', 'message'=> 'Ingresa el origen del envío'],
            ['destination', 'required', 'message'=> 'Ingresa el destino del envío'],
            ['origin', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['destination', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['fk_vehicletype', 'required', 'message'=> 'Selecciona un tipo de vehículo'],
            ['weight', 'required', 'message'=> 'Ingresa el peso de la carga'],
            ['payment', 'required', 'message'=> 'Ingresa el valor del pago'],
            ['vehicle_number', 'required', 'message'=> 'Ingresa el número de vehículos'],
            ['comment', 'required', 'message'=> 'Ingresa un comentario breve'],
            [['payment','vehicle_number'], 'number', 'message'=>"Indica el valor en números"],
            [['latitude_orig','longitude_orig'], 'required', 'message'=> 'Se debe ingresar el punto de origen'],
            [['latitude_dest','longitude_dest'], 'required', 'message'=> 'Se debe ingresar el punto de destino'],
            ['alltcheck','safe'],
        ];
    }
 
}
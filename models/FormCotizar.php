<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormCotizar extends Model{
 
    public $origin;   
    public $destination;
    public $category;
    public $subcategory;
    public $weight;
    public $value;
    public $dimensions;
    public $comment;
    public $valueaseg;
    public $latitude_orig;
    public $longitude_orig;
    public $latitude_dest;
    public $longitude_dest;

    public function rules()
    {
        return [
            ['origin', 'required', 'message'=> 'Ingresa el origen del envío'],
            ['destination', 'required', 'message'=> 'Ingresa el destino del envío'],
            ['origin', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['destination', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['category', 'required', 'message'=> 'Selecciona una categoria'],
            ['subcategory', 'required', 'message'=> 'Selecciona una subcategoria'],
            ['weight', 'required', 'message'=> 'Ingresa el peso de la carga'],
            ['value', 'required', 'message'=> 'Ingresa el valor'],
            ['comment', 'required', 'message'=> 'Ingresa un comentario breve'],
            ['dimensions', 'required', 'message'=> 'Ingresa las dimensiones de la carga'],
            ['value', 'number', 'message'=>"Indica el valor en pesos"],
            ['valueaseg', 'number', 'message'=>"Indica el valor a asegurar en pesos"],
            [['latitude_orig','longitude_orig'], 'required', 'message'=> 'Se debe ingresar el punto de origen'],
            [['latitude_dest','longitude_dest'], 'required', 'message'=> 'Se debe ingresar el punto de destino'],
        ];
    }
 
}
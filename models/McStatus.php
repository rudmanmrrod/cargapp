<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_status".
 *
 * @property integer $pkstatus
 * @property string $status_name
 *
 * @property McTcarga[] $mcTcargas
 * @property McTcontract[] $mcTcontracts
 */
class McStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'required'],
            [['status_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkstatus' => 'Pkstatus',
            'status_name' => 'Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcargas()
    {
        return $this->hasMany(McTcarga::className(), ['fkstatus' => 'pkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontracts()
    {
        return $this->hasMany(McTcontract::className(), ['fk_status' => 'pkstatus']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tvehicle".
 *
 * @property string $pkvehicle
 * @property integer $fkvehicletype
 * @property string $fkuser
 * @property string $vehicle_bodywork
 * @property string $vehicle_licenceplate
 * @property integer $vehicle_model
 * @property string $vehicle_brand
 * @property string $vehicle_color
 *
 * @property McTuser $fkuser0
 * @property McTvehicletype $fkvehicletype0
 */
class McTvehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tvehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkvehicletype', 'fkuser'], 'required'],
            [['fkvehicletype', 'fkuser', 'vehicle_model'], 'integer'],
            [['vehicle_bodywork', 'vehicle_licenceplate', 'vehicle_brand', 'vehicle_color'], 'string', 'max' => 100],
            [['vehicle_bodywork', 'vehicle_licenceplate', 'vehicle_brand', 'vehicle_color','fkvehicletype'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkvehicle' => 'Pkvehicle',
            'fkvehicletype' => 'Fkvehicletype',
            'fkuser' => 'Fkuser',
            'vehicle_bodywork' => 'Vehicle Bodywork',
            'vehicle_licenceplate' => 'Vehicle Licenceplate',
            'vehicle_model' => 'Vehicle Model',
            'vehicle_brand' => 'Vehicle Brand',
            'vehicle_color' => 'Vehicle Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkvehicletype0()
    {
        return $this->hasOne(McTvehicletype::className(), ['pkvehicletype' => 'fkvehicletype']);
    }
}

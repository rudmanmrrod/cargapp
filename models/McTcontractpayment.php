<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcontractpayment".
 *
 * @property integer $pkcontractpayment
 * @property string $paymentvalue
 * @property string $fkcontract
 * @property integer $fkstatus
 * @property string $paymentdate
 * @property string $paymentdesc
 *
 * @property McStatus $fkstatus0
 * @property McTcontract $fkcontract0
 */
class McTcontractpayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcontractpayment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentvalue', 'fkcontract', 'fkstatus'], 'required'],
            [['paymentvalue', 'fkcontract', 'fkstatus'], 'integer'],
            [['paymentdate'], 'safe'],
            [['paymentdesc'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcontractpayment' => 'Pkcontractpayment',
            'paymentvalue' => 'Paymentvalue',
            'fkcontract' => 'Fkcontract',
            'fkstatus' => 'Fkstatus',
            'paymentdate' => 'Paymentdate',
            'paymentdesc' => 'Paymentdesc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontract0()
    {
        return $this->hasOne(McTcontract::className(), ['pk_contract' => 'fkcontract']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_treferencetype".
 *
 * @property integer $pkrefencetype
 * @property string $reference_name
 */
class McTreferencetype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_treferencetype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reference_name'], 'required'],
            [['reference_name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkrefencetype' => 'Pkrefencetype',
            'reference_name' => 'Reference Name',
        ];
    }
}

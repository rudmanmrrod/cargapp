<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcategory_subcategory".
 *
 * @property integer $pktcategory_subcategory
 * @property integer $fkcategory
 * @property integer $fksubcategory
 *
 * @property McTcategory $fkcategory0
 * @property McTsubcategory $fksubcategory0
 */
class McTcategorySubcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcategory_subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcategory', 'fksubcategory'], 'required'],
            [['fkcategory', 'fksubcategory'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pktcategory_subcategory' => 'Pktcategory Subcategory',
            'fkcategory' => 'Fkcategory',
            'fksubcategory' => 'Fksubcategory',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcategory0()
    {
        return $this->hasOne(McTcategory::className(), ['pkcategory' => 'fkcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFksubcategory0()
    {
        return $this->hasOne(McTsubcategory::className(), ['pksubcategory' => 'fksubcategory']);
    }
}

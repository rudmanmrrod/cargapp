<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tattachmenttype".
 *
 * @property integer $pkattachtype
 * @property string $name
 *
 * @property McTattachments[] $mcTattachments
 */
class McTattachmenttype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tattachmenttype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkattachtype' => 'Pkattachtype',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTattachments()
    {
        return $this->hasMany(McTattachments::className(), ['attachment_type' => 'pkattachtype']);
    }
}

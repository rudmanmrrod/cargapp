<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tpaymentload".
 *
 * @property string $pkpaymentload
 * @property string $payment_date
 * @property string $payment_number
 * @property integer $payment_value
 * @property string $payment_signature
 * @property string $fkload
 * @property string $fkproposal
 * @property integer $fkstatus
 *
 * @property McTcarga $fkload0
 * @property McTproposal $fkproposal0
 * @property McStatus $fkstatus0
 */
class McTpaymentload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tpaymentload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_date'], 'safe'],
            [['payment_value', 'fkload', 'fkproposal', 'fkstatus'], 'integer'],
            [['fkload', 'fkproposal', 'fkstatus'], 'required'],
            [['payment_number', 'payment_signature'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkpaymentload' => 'Pkpaymentload',
            'payment_date' => 'Payment Date',
            'payment_number' => 'Payment Number',
            'payment_value' => 'Payment Value',
            'payment_signature' => 'Payment Signature',
            'fkload' => 'Fkload',
            'fkproposal' => 'Fkproposal',
            'fkstatus' => 'Fkstatus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkload0()
    {
        return $this->hasOne(McTcarga::className(), ['pkload' => 'fkload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproposal0()
    {
        return $this->hasOne(McTproposal::className(), ['pkproposal' => 'fkproposal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }
}

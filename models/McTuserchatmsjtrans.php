<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tuserchatmsjtrans".
 *
 * @property integer $pkmsj
 * @property integer $fkuser
 * @property integer $fkuserchattrans
 * @property string $message
 * @property string $date
 *
 * @property McTlogin $fkuser0
 * @property McTuserchattrans $fkuserchattrans0
 */
class McTuserchatmsjtrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tuserchatmsjtrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'fkuserchattrans', 'message'], 'required'],
            [['fkuser', 'fkuserchattrans'], 'integer'],
            [['message'], 'string'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkmsj' => 'Pkmsj',
            'fkuser' => 'Fkuser',
            'fkuserchattrans' => 'Fkuserchattrans',
            'message' => 'Message',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuserchattrans0()
    {
        return $this->hasOne(McTuserchattrans::className(), ['pkuserchat' => 'fkuserchattrans']);
    }
}

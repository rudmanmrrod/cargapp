<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcontracteval".
 *
 * @property integer $pkcontracteval
 * @property string $fkcontract
 * @property double $eval
 *
 * @property McTcontract $fkcontract0
 */
class McTcontracteval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcontracteval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontract', 'eval'], 'required'],
            [['fkcontract'], 'integer'],
            [['eval'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcontracteval' => 'Pkcontracteval',
            'fkcontract' => 'Fkcontract',
            'eval' => 'Eval',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontract0()
    {
        return $this->hasOne(McTcontract::className(), ['pk_contract' => 'fkcontract']);
    }
}

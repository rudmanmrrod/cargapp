<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormChangePassword extends Model{
 
    public $name; 
    public $acc_name;  
    public $email;
    public $phone;
    public $password;
    public $new_password;
    public $new_password_repeat;
    public $avatar;
    
    public function rules()
    {
        return [
            ['name', 'required', 'message'=> 'Ingresa tu nombre'],
            ['acc_name', 'required', 'message'=> 'Ingresa el nombre de tu cuenta'],
            ['email', 'required', 'message'=> 'Ingresa tu correo'],
            ['phone', 'required', 'message'=> 'Ingresa tu número de contácto'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'El correo ingresado no tiene un formato valido'],
            ['avatar','image'],
            ['password', 'compare_password'],
            ['new_password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Las contraseñas no coinciden'],
            [['new_password','new_password_repeat'],'required_pass'],
            ['phone', 'number'],
        ];
    }
    
    public function compare_password($attribute, $params)
    {
      $table = Users::find()->where("email=:email", [":email" => $this->name])->one();
      $new_pass = crypt($this->password, Yii::$app->params["salt"]);
      if ($new_pass!=$table->password)
        $this->addError($attribute, "El password no coincide con el registrado");
    }

    public function required_pass($attribute, $params)
    {
      if ($this->new_password=='')
      {
        $this->addError('new_password_repeat', 'Este campo es requerido');
      }
      else if($this->new_password_repeat=='')
      {
        $this->addError('new_password', 'Este campo es requerido');
      }
    }
 
}
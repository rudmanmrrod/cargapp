<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcompany".
 *
 * @property integer $pkcompany
 * @property string $company_rz
 * @property string $company_nit
 * @property string $company_attendant
 * @property string $company_address
 * @property string $company_phone
 * @property string $company_review
 * @property string $company_accountnumber
 * @property string $company_accountname
 * @property integer $company_accounttype
 * @property integer $fkbank
 * @property integer $fklogin
 * @property string $company_avatar
 *
 * @property McTbank $fkbank0
 * @property McTlogin $fklogin0
 * @property McTcompanyservices[] $mcTcompanyservices
 */
class McTcompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcompany';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_accounttype', 'fkbank', 'fklogin'], 'integer'],
            [['fklogin'], 'required'],
            [['company_rz', 'company_nit', 'company_attendant', 'company_address', 'company_phone', 'company_review', 'company_accountname'], 'string', 'max' => 100],
            [['company_accountnumber'], 'string', 'max' => 50],
            [['company_avatar'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcompany' => 'Pkcompany',
            'company_rz' => 'Company Rz',
            'company_nit' => 'Company Nit',
            'company_attendant' => 'Company Attendant',
            'company_address' => 'Company Address',
            'company_phone' => 'Company Phone',
            'company_review' => 'Company Review',
            'company_accountnumber' => 'Company Accountnumber',
            'company_accountname' => 'Company Accountname',
            'company_accounttype' => 'Company Accounttype',
            'fkbank' => 'Fkbank',
            'fklogin' => 'Fklogin',
            'company_avatar' => 'Company Avatar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkbank0()
    {
        return $this->hasOne(McTbank::className(), ['pkbank' => 'fkbank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFklogin0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fklogin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcompanyservices()
    {
        return $this->hasMany(McTcompanyservices::className(), ['fkcompany' => 'pkcompany']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_uloadproposal".
 *
 * @property integer $pkuloadproposal
 * @property string $fkcontract
 * @property integer $fktransportador
 * @property integer $status
 * @property integer $hide
 * @property integer $payment
 *
 * @property McTcontract $fkcontract0
 * @property McTlogin $fktransportador0
 */
class McUloadproposal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_uloadproposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontract', 'fktransportador', 'payment'], 'required'],
            [['fkcontract', 'fktransportador', 'status', 'hide', 'payment'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkuloadproposal' => 'Pkuloadproposal',
            'fkcontract' => 'Fkcontract',
            'fktransportador' => 'Fktransportador',
            'status' => 'Status',
            'hide' => 'Hide',
            'payment' => 'Payment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontract0()
    {
        return $this->hasOne(McTcontract::className(), ['pk_contract' => 'fkcontract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFktransportador0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fktransportador']);
    }
}

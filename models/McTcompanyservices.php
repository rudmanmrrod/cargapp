<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcompanyservices".
 *
 * @property integer $pkcompanyservice
 * @property double $companyservices_percent
 * @property string $fkcompany
 * @property integer $fksubcategory
 *
 * @property McTcategorySubcategory $fksubcategory0
 * @property McTcompany $fkcompany0
 */
class McTcompanyservices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcompanyservices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companyservices_percent'], 'number'],
            [['fkcompany', 'fksubcategory'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcompanyservice' => 'Pkcompanyservice',
            'companyservices_percent' => 'Companyservices Percent',
            'fkcompany' => 'Fkcompany',
            'fksubcategory' => 'Fksubcategory',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFksubcategory0()
    {
        return $this->hasOne(McTcategorySubcategory::className(), ['pktcategory_subcategory' => 'fksubcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcompany0()
    {
        return $this->hasOne(McTcompany::className(), ['pkcompany' => 'fkcompany']);
    }
}

<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;

class FormChangePasswordCompany extends Model{
 
    public $rz;   
    public $nit;
    public $name;
    public $address;
    public $email;
    public $phone;
    public $password;
    public $new_password;
    public $new_password_repeat;
    public $avatar;
    public $type;
    public $user;
    public $review;
    public $bank;
    public $typeaccount;
    public $numberaccount;
    public $nameaccount;
    public $mcTsubcategories;
    public $mcTpercent;
    
    public function rules()
    {
        return [
            ['rz', 'required', 'message'=> 'Ingresa la razón social'],
            ['nit', 'required', 'message'=> 'Ingresa el NIT'],
            ['name', 'required', 'message'=> 'Ingresa nombre del encargado'],
            ['address', 'required', 'message'=> 'Ingresa la dirección'],
            ['review', 'required', 'message'=> 'Ingresa una corta reseña sobre la empresa'],
            ['review', 'match', 'pattern' => "/^.{20,80}$/", 'message' => 'Mínimo 20 y máximo 80 caracteres'],
            ['email', 'required', 'message'=> 'Ingresa el correo'],
            ['bank', 'required', 'message'=> 'Seleccione el banco'],
            ['typeaccount', 'required', 'message'=> 'Seleccione el tipo de cuenta'],
            ['numberaccount', 'required', 'message'=> 'Ingresa el número de cuenta'],
            ['mcTsubcategories', 'required', 'message'=> 'Ingresa al menos una categoria'],
            ['mcTpercent', 'required', 'message'=> 'Ingresa el porcentaje de las categorias'],
            ['bank', 'required', 'message'=> 'Seleccione el banco'],
            ['user', 'required', 'message'=> 'Asigna un nombre de usuario'],
            ['nameaccount', 'required', 'message'=> 'Asigna un titular a la cuenta'],
            ['password', 'compare_password'],
            ['new_password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Las contraseñas no coinciden'],
            ['phone', 'required', 'message'=> 'Ingresa tu número de contácto'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'El correo ingresado no tiene un formato valido'],
            ['avatar','image'],
            ['password', 'match', 'pattern' => "/^.{8,16}$/", 'message' => 'Mínimo 8 y máximo 16 caracteres'],
            ['phone', 'number'],
            ['type', 'number']
        ];
    }

    public function compare_password($attribute, $params)
    {
      $table = Users::find()->where("email=:email", [":email" => $this->name])->one();
      $new_pass = crypt($this->password, Yii::$app->params["salt"]);
      if ($new_pass!=$table->password)
        $this->addError($attribute, "El password no coincide con el registrado");
    }

    public function required_pass($attribute, $params)
    {
      if ($this->new_password=='')
      {
        $this->addError('new_password_repeat', 'Este campo es requerido');
      }
      else if($this->new_password_repeat=='')
      {
        $this->addError('new_password', 'Este campo es requerido');
      }
    }
 
}
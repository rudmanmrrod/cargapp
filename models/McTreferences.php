<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_treferences".
 *
 * @property string $pkreference
 * @property string $fkuser
 * @property string $reference_name
 * @property string $reference_address
 * @property string $reference_phone
 * @property integer $fk_referencetype
 *
 * @property McTreferencetype $fkReferencetype
 * @property McTuser $fkuser0
 */
class McTreferences extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_treferences';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'reference_name', 'reference_address', 'reference_phone', 'fk_referencetype'], 'required'],
            [['fkuser', 'fk_referencetype'], 'integer'],
            [['reference_name', 'reference_address', 'reference_phone'], 'string', 'max' => 100],
            [['reference_name', 'reference_address', 'reference_phone'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkreference' => 'Pkreference',
            'fkuser' => 'Fkuser',
            'reference_name' => 'Reference Name',
            'reference_address' => 'Reference Address',
            'reference_phone' => 'Reference Phone',
            'fk_referencetype' => 'Fk Referencetype',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkReferencetype()
    {
        return $this->hasOne(McTreferencetype::className(), ['pkrefencetype' => 'fk_referencetype']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }
}

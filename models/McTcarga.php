<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcarga".
 *
 * @property string $pkload
 * @property integer $fkstatus
 * @property string $load_citydestination
 * @property string $load_cityorigin
 * @property integer $fksubcategory
 * @property string $fkuser
 * @property double $load_weight
 * @property double $load_value
 * @property string $load_dimensions
 * @property string $load_comment
 * @property double $load_ensure
 * @property string $load_create
 * @property string $latitude_origin
 * @property string $longitude_origin
 * @property string $latitude_destination
 * @property string $longitude_destination
 *
 * @property McStatus $fkstatus0
 * @property McTcategorySubcategory $fksubcategory0
 * @property McTuser $fkuser0
 * @property McTcontract[] $mcTcontracts
 * @property McThideload[] $mcThideloads
 * @property McTpaymentload[] $mcTpaymentloads
 * @property McTproposal[] $mcTproposals
 */
class McTcarga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcarga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkstatus', 'fksubcategory', 'fkuser', 'latitude_origin', 'longitude_origin', 'latitude_destination', 'longitude_destination'], 'required'],
            [['fkstatus', 'fksubcategory', 'fkuser'], 'integer'],
            [['load_weight', 'load_value', 'load_ensure'], 'number'],
            [['load_comment'], 'string'],
            [['load_create'], 'safe'],
            [['load_citydestination', 'load_cityorigin', 'load_dimensions', 'latitude_origin', 'longitude_origin', 'latitude_destination', 'longitude_destination'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkload' => 'Pkload',
            'fkstatus' => 'Fkstatus',
            'load_citydestination' => 'Load Citydestination',
            'load_cityorigin' => 'Load Cityorigin',
            'fksubcategory' => 'Fksubcategory',
            'fkuser' => 'Fkuser',
            'load_weight' => 'Load Weight',
            'load_value' => 'Load Value',
            'load_dimensions' => 'Load Dimensions',
            'load_comment' => 'Load Comment',
            'load_ensure' => 'Load Ensure',
            'load_create' => 'Load Create',
            'latitude_origin' => 'Latitude Origin',
            'longitude_origin' => 'Longitude Origin',
            'latitude_destination' => 'Latitude Destination',
            'longitude_destination' => 'Longitude Destination',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFksubcategory0()
    {
        return $this->hasOne(McTcategorySubcategory::className(), ['pktcategory_subcategory' => 'fksubcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontracts()
    {
        return $this->hasMany(McTcontract::className(), ['fk_carga' => 'pkload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcThideloads()
    {
        return $this->hasMany(McThideload::className(), ['fkload' => 'pkload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTpaymentloads()
    {
        return $this->hasMany(McTpaymentload::className(), ['fkload' => 'pkload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTproposals()
    {
        return $this->hasMany(McTproposal::className(), ['fkload' => 'pkload']);
    }
}

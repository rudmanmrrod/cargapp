<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcategory".
 *
 * @property integer $pkcategory
 * @property string $category_name
 * @property string $caregory_location
 *
 * @property McTcategorySubcategory[] $mcTcategorySubcategories
 */
class McTcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'required'],
            [['category_name'], 'string', 'max' => 100],
            [['caregory_location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcategory' => 'Pkcategory',
            'category_name' => 'Category Name',
            'caregory_location' => 'Caregory Location',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcategorySubcategories()
    {
        return $this->hasMany(McTcategorySubcategory::className(), ['fkcategory' => 'pkcategory']);
    }
}

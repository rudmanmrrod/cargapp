<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcontract".
 *
 * @property string $pk_contract
 * @property integer $fkuser_company
 * @property string $signature_time
 * @property integer $fk_status
 * @property string $fk_carga
 * @property string $fk_proposal
 *
 * @property McTproposal $fkProposal
 * @property McStatus $fkStatus
 * @property McTcarga $fkCarga
 * @property McTlogin $fkuserCompany
 * @property McTcontractemp[] $mcTcontractemps
 * @property McTcontracteval[] $mcTcontractevals
 * @property McTcontractpayment[] $mcTcontractpayments
 * @property McTuserchat[] $mcTuserchats
 * @property McUloadproposal[] $mcUloadproposals
 */
class McTcontract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcontract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser_company', 'fk_status', 'fk_carga', 'fk_proposal'], 'required'],
            [['fkuser_company', 'fk_status', 'fk_carga', 'fk_proposal'], 'integer'],
            [['signature_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_contract' => 'Pk Contract',
            'fkuser_company' => 'Fkuser Company',
            'signature_time' => 'Signature Time',
            'fk_status' => 'Fk Status',
            'fk_carga' => 'Fk Carga',
            'fk_proposal' => 'Fk Proposal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkProposal()
    {
        return $this->hasOne(McTproposal::className(), ['pkproposal' => 'fk_proposal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkStatus()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fk_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCarga()
    {
        return $this->hasOne(McTcarga::className(), ['pkload' => 'fk_carga']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuserCompany()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontractemps()
    {
        return $this->hasMany(McTcontractemp::className(), ['fkcontract' => 'pk_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontractevals()
    {
        return $this->hasMany(McTcontracteval::className(), ['fkcontract' => 'pk_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontractpayments()
    {
        return $this->hasMany(McTcontractpayment::className(), ['fkcontract' => 'pk_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTuserchats()
    {
        return $this->hasMany(McTuserchat::className(), ['fkcontract' => 'pk_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcUloadproposals()
    {
        return $this->hasMany(McUloadproposal::className(), ['fkcontract' => 'pk_contract']);
    }
}

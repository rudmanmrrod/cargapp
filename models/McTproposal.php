<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tproposal".
 *
 * @property integer $pkproposal
 * @property integer $fkuser
 * @property integer $fkload
 * @property double $proposal_total
 * @property double $proposal_paymin
 * @property string $proposal_date
 * @property integer $fkcompany
 *
 * @property McTcarga $fkload0
 * @property McTuser $fkuser0
 * @property McTcompany $fkcompany0
 */
class McTproposal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tproposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'fkload', 'fkcompany'], 'integer'],
            [['fkload', 'proposal_total', 'proposal_paymin'], 'required'],
            [['proposal_total', 'proposal_paymin'], 'number'],
            [['proposal_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkproposal' => 'Pkproposal',
            'fkuser' => 'Fkuser',
            'fkload' => 'Fkload',
            'proposal_total' => 'Proposal Total',
            'proposal_paymin' => 'Proposal Paymin',
            'proposal_date' => 'Proposal Date',
            'fkcompany' => 'Fkcompany',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkload0()
    {
        return $this->hasOne(McTcarga::className(), ['pkload' => 'fkload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcompany0()
    {
        return $this->hasOne(McTcompany::className(), ['pkcompany' => 'fkcompany']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcity".
 *
 * @property integer $pkcity
 * @property string $city_name
 *
 * @property McTcarga[] $mcTcargas
 * @property McTcarga[] $mcTcargas0
 */
class McTcity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_name'], 'required'],
            [['city_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcity' => 'Pkcity',
            'city_name' => 'City Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcargas()
    {
        return $this->hasMany(McTcarga::className(), ['fkcity_destination' => 'pkcity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcargas0()
    {
        return $this->hasMany(McTcarga::className(), ['fkcity_origin' => 'pkcity']);
    }
}

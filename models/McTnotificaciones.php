<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tnotificaciones".
 *
 * @property integer $pk_notificaciones
 * @property integer $fk_login
 * @property string $msj_notificacion
 * @property integer $activa
 * @property integer $type
 * @property integer $fkchattrans
 *
 * @property McTlogin $fkLogin
 * @property McTuserchattrans $fkchattrans0
 */
class McTnotificaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tnotificaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_login', 'msj_notificacion'], 'required'],
            [['fk_login', 'activa', 'type', 'fkchattrans'], 'integer'],
            [['msj_notificacion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_notificaciones' => 'Pk Notificaciones',
            'fk_login' => 'Fk Login',
            'msj_notificacion' => 'Msj Notificacion',
            'activa' => 'Activa',
            'type' => 'Type',
            'fkchattrans' => 'Fkchattrans',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkLogin()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fk_login']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkchattrans0()
    {
        return $this->hasOne(McTuserchattrans::className(), ['pkuserchat' => 'fkchattrans']);
    }
}

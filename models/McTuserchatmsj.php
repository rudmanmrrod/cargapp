<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tuserchatmsj".
 *
 * @property integer $pkchatmsj
 * @property integer $fkuser
 * @property integer $fkuserchat
 * @property string $message
 * @property string $date
 *
 * @property McTlogin $fkuser0
 * @property McTuserchat $fkuserchat0
 */
class McTuserchatmsj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tuserchatmsj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'fkuserchat', 'message'], 'required'],
            [['fkuser', 'fkuserchat'], 'integer'],
            [['message'], 'string'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkchatmsj' => 'Pkchatmsj',
            'fkuser' => 'Fkuser',
            'fkuserchat' => 'Fkuserchat',
            'message' => 'Message',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTlogin::className(), ['id' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuserchat0()
    {
        return $this->hasOne(McTuserchat::className(), ['pkuserchat' => 'fkuserchat']);
    }
}

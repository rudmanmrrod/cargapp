<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tattachments".
 *
 * @property string $pkattachment
 * @property string $fkuser
 * @property string $attachment_location
 * @property integer $attachment_type
 *
 * @property McTuser $fkuser0
 * @property McTattachmenttype $attachmentType
 */
class McTattachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tattachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'attachment_location', 'attachment_type'], 'required'],
            [['fkuser', 'attachment_type'], 'integer'],
            [['attachment_location'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkattachment' => 'Pkattachment',
            'fkuser' => 'Fkuser',
            'attachment_location' => 'Attachment Location',
            'attachment_type' => 'Attachment Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(McTuser::className(), ['pkuser' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentType()
    {
        return $this->hasOne(McTattachmenttype::className(), ['pkattachtype' => 'attachment_type']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tuserchattrans".
 *
 * @property integer $pkuserchat
 * @property integer $fkcontractemp
 *
 * @property McTuserchatmsjtrans[] $mcTuserchatmsjtrans
 * @property McTcontractemp $fkcontractemp0
 */
class McTuserchattrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tuserchattrans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontractemp'], 'required'],
            [['fkcontractemp'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkuserchat' => 'Pkuserchat',
            'fkcontractemp' => 'Fkcontractemp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTuserchatmsjtrans()
    {
        return $this->hasMany(McTuserchatmsjtrans::className(), ['fkuserchattrans' => 'pkuserchat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontractemp0()
    {
        return $this->hasOne(McTcontractemp::className(), ['pk_contract' => 'fkcontractemp']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tcotizaremp".
 *
 * @property integer $pkcotizaremp
 * @property string $origen
 * @property string $destino
 * @property integer $fk_vehicletype
 * @property string $weight
 * @property integer $payment
 * @property integer $vehicle_number
 * @property string $comment
 * @property string $origin_latitude
 * @property string $origin_longitude
 * @property string $destination_latitude
 * @property string $destination_longitude
 * @property string $fkcompany
 * @property integer $fkstatus
 * @property integer $send_all
 *
 * @property McTcontractemp[] $mcTcontractemps
 * @property McStatus $fkstatus0
 * @property McTcompany $fkcompany0
 * @property McTvehicletype $fkVehicletype
 * @property McTloadproposal[] $mcTloadproposals
 */
class McTcotizaremp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tcotizaremp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origen', 'destino', 'fk_vehicletype', 'weight', 'payment', 'vehicle_number', 'comment', 'origin_latitude', 'origin_longitude', 'destination_latitude', 'destination_longitude', 'fkcompany', 'fkstatus'], 'required'],
            [['fk_vehicletype', 'payment', 'vehicle_number', 'fkcompany', 'fkstatus', 'send_all'], 'integer'],
            [['comment'], 'string'],
            [['origen', 'destino', 'weight'], 'string', 'max' => 50],
            [['origin_latitude', 'origin_longitude', 'destination_latitude', 'destination_longitude'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcotizaremp' => 'Pkcotizaremp',
            'origen' => 'Origen',
            'destino' => 'Destino',
            'fk_vehicletype' => 'Fk Vehicletype',
            'weight' => 'Weight',
            'payment' => 'Payment',
            'vehicle_number' => 'Vehicle Number',
            'comment' => 'Comment',
            'origin_latitude' => 'Origin Latitude',
            'origin_longitude' => 'Origin Longitude',
            'destination_latitude' => 'Destination Latitude',
            'destination_longitude' => 'Destination Longitude',
            'fkcompany' => 'Fkcompany',
            'fkstatus' => 'Fkstatus',
            'send_all' => 'Send All',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcontractemps()
    {
        return $this->hasMany(McTcontractemp::className(), ['fkcotizaremp' => 'pkcotizaremp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcompany0()
    {
        return $this->hasOne(McTcompany::className(), ['pkcompany' => 'fkcompany']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkVehicletype()
    {
        return $this->hasOne(McTvehicletype::className(), ['pkvehicletype' => 'fk_vehicletype']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTloadproposals()
    {
        return $this->hasMany(McTloadproposal::className(), ['fkcotizacionemp' => 'pkcotizaremp']);
    }
}

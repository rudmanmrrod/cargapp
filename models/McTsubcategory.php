<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tsubcategory".
 *
 * @property integer $pksubcategory
 * @property string $subcategory_name
 *
 * @property McTcarga[] $mcTcargas
 * @property McTcategorySubcategory[] $mcTcategorySubcategories
 * @property McTcompanyservices[] $mcTcompanyservices
 */
class McTsubcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tsubcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pksubcategory' => 'Pksubcategory',
            'subcategory_name' => 'Subcategory Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcargas()
    {
        return $this->hasMany(McTcarga::className(), ['fksubcategory' => 'pksubcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcategorySubcategories()
    {
        return $this->hasMany(McTcategorySubcategory::className(), ['fksubcategory' => 'pksubcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTcompanyservices()
    {
        return $this->hasMany(McTcompanyservices::className(), ['fksubcategory' => 'pksubcategory']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tvehicletype".
 *
 * @property integer $pkvehicletype
 * @property string $vehicletype_name
 *
 * @property McTreferences[] $mcTreferences
 * @property McTvehicle[] $mcTvehicles
 */
class McTvehicletype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tvehicletype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicletype_name'], 'required'],
            [['vehicletype_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkvehicletype' => 'Pkvehicletype',
            'vehicletype_name' => 'Vehicletype Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTreferences()
    {
        return $this->hasMany(McTreferences::className(), ['fk_referencetype' => 'pkvehicletype']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcTvehicles()
    {
        return $this->hasMany(McTvehicle::className(), ['fkvehicletype' => 'pkvehicletype']);
    }
}

<?php

namespace app\models;
use Yii;
use yii\base\Model;

class FormAdminTransporterPayment extends Model{
 
    public $reference;   
    public $value;


    public function rules()
    {
        return [
            ['reference', 'required', 'message'=> 'Ingresa el número de referencia'],
            ['value', 'required', 'message'=> 'Ingresa monto a pagar'],
        ];
    }
 
}
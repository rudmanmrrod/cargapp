<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_tloadstatus".
 *
 * @property integer $pkloadstatus
 * @property integer $fkcontractemp
 * @property integer $fkstatus
 * @property string $latitude
 * @property string $longitude
 * @property string $codigo
 * @property string $sello1
 * @property string $sello2
 *
 * @property McStatus $fkstatus0
 * @property McTcontractemp $fkcontractemp0
 */
class McTloadstatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mc_tloadstatus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcontractemp', 'fkstatus'], 'required'],
            [['fkcontractemp', 'fkstatus'], 'integer'],
            [['sello1', 'sello2'],'string'],
            [['latitude', 'longitude'], 'string', 'max' => 100],
            [['codigo'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkloadstatus' => 'Pkloadstatus',
            'fkcontractemp' => 'Fkcontractemp',
            'fkstatus' => 'Fkstatus',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'codigo' => 'Codigo',
            'sello1' => 'Sello1',
            'sello2' => 'Sello2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(McStatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcontractemp0()
    {
        return $this->hasOne(McTcontractemp::className(), ['pk_contract' => 'fkcontractemp']);
    }
}

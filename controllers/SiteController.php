<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormRegister;
use app\models\Users;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
use app\models\User;
use app\models\McTuser;
use kartik\widgets\FileInput;
use app\models\McTcategory;
use app\models\McTnew;
use app\models\McTpaymentload;
use app\models\McTcarga;
use app\models\McTproposal;
use app\models\McTcontract;
use app\models\McTcontractpayment;
use app\models\McTuserchat;

class SiteController extends Controller
{
    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    public function actionNoticias($perfil = null)
    {
        $this->layout = "index";
        $news= McTnew::find()
        ->where('new_type = :type', [':type' => 1])->orderBy('new_date DESC')
        ->all();
        Yii::$app->session->set('option', 2);
        return $this->render('news',
            [
                'news'=>$news
            ]
        );
    }
  
     public function actionConfirm()
     {
        $table = new Users;
        if (Yii::$app->request->get())
        {
       
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                //Realizamos la consulta para obtener el registro
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
     
                //Si el registro existe
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = '1';
                    if ($activar->update()){
                        $utmp = $activar->getMcTusers()->One();
                        /*$subject = $utmp->user_name . ", Bienvenido a Mi Cargapp";
                        Yii::$app->mailer->compose('welcome', ['name'=>$utmp->user_name])
                         ->setTo($activar->email2)
                         ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                         ->setSubject($subject)
                         ->send();*/
                        echo "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                    }else
                    {
                        echo "Ha ocurrido un error ...";
                        print_r($activar->getErrors());
                    }
                 }
                else
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else
            {
                return $this->redirect(["site/login"]);
            }
        }
     }

    public function beforeAction($action)
    {            
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

     public function actionJkh8ab4negk4q9ttpx6g3f3kd(){
     	if($_SERVER['REQUEST_METHOD'] === 'POST'){
	        try {
	        	$file = fopen(Yii::getAlias('@webroot') . "/archivo.txt", "w");
	            fwrite($file, json_encode($_POST));
	            fclose($file);
	            echo "Se escribio el archivo - No Errors!!";
	            /*$p = new McTpaymentload;
	            $save = true;
	            $c = McTcarga::find()
	                ->where("pkload=:id", [":id" => $_POST['campoExtra1']])
	                ->One();
	            $p->fkload = $_POST['campoExtra1'];
	            $p->fkproposal = $_POST['campoExtra2'];
	            if($_POST['transaccionAprobada'] == 1){
	                $p->payment_signature = $_POST['firmaTuCompra'];
	                $p->payment_number = $_POST['codigoFactura'];
	                $p->payment_value = $_POST['valorFactura'];
	                $c->fkstatus = 3;
	                $c->save();
	                $p->save();
	            }else
	                if($_POST['transaccionAprobada'] == 0){
	                    $c->fkstatus = 2;
	                    $c->save();
	                }*/
	        } catch (Exception $e) {
	            $file = fopen(Yii::getAlias('@webroot') . "/archivo.txt", "w");
	            fwrite($file, print_r($e));
	            fclose($file);
	            echo "Se escribio el archivo - Error!!";
	        }
	    }
     }

     public function actionConfirmpay(){
        Yii::$app->session->set('status',$_GET['s']);
        $proposal = McTproposal::find()->where(['fkload'=>$_GET['key']])->one();
        //Mensaje
        $msj = '';
        //Se crea el contrato
        if($_GET['s'])
        {
            $contract = new McTcontract;
            $contract->fkuser_company = $proposal->fkcompany0->fklogin;
            $contract->fk_status = 1;
            $contract->fk_carga  = $proposal->fkload;
            $contract->fk_proposal = $proposal->pkproposal;
            if($contract->save())
            {
                $msj = "Se creó el contrato con éxito"; 
                //Se crea el pago   
                $payment = new McTpaymentload;
                $payment->payment_value = $proposal->proposal_paymin;
                $payment->fkload = $proposal->fkload;
                $payment->fkproposal = $proposal->pkproposal;
                $payment->fkstatus = 4;
                $payment->payment_number = $_GET['cod'];
                $payment->payment_signature = $_GET['firma'];
                $payment->save();
                //Se cargan los datos necesarios
                $load = $contract->getFkCarga()->One();
                $company = $contract->getFkuserCompany()->One();
                $comp_profile = $company->getMcTcompanies()->One();
                $proposal = McTproposal::find()->where(['fkload'=>$load->pkload,'fkcompany'=>$comp_profile->pkcompany])->one();
                $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
                $service = McTcompanyservices::find()->where(['fkcompany'=>$comp_profile->pkcompany,
                  'fksubcategory'=>$catg->getMcTcategorySubcategories()->One()->pktcategory_subcategory])->one();
                //Se crea el pago para el admin
                $adminpayment = new McTcontractpayment;
                $adminpayment->paymentvalue = $proposal->proposal_total*($service->companyservices_percent/100);
                $adminpayment->fkcontract = $contract->pk_contract;
                $adminpayment->fkstatus = 6;
                $adminpayment->save();
                //Se crea el chat
                $chat = new McTuserchat;
                $chat->fkcontract = $contract->pk_contract;
                $chat->save();
                //Se crea una notificación para la empresa
                $user = $load->fkuser0->user_name;
                $model_notificacion = new McTnotificaciones();
                $model_notificacion->fk_login=$company->id;
                $model_notificacion->msj_notificacion="El Usuario <b>". $user ."</b> te aceptó en su carga <a href='".Yii::getAlias('@web')."/accountc/miscargas' onclick='modalClick(event)'> Ver Cargas</a>";
                $model_notificacion->activa=1;
                $model_notificacion->save();
                //Se crean notificaciones para los administradores del sitio
                $zeros = '000000';
                $contract_type = ''.$contract->pk_contract;
                $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                $admins = McTlogin::find()->where(['role'=>1,'activate'=>1])->all();
                foreach ($admins as $admin) {
                    $model_notificacion = new McTnotificaciones();
                    $model_notificacion->fk_login=$admin->id;
                    $model_notificacion->msj_notificacion="Se realizo el pago del contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/admin/cargas' onclick='modalClick(event)'> Ver Contrato</a>";
                    $model_notificacion->activa=1;
                    $model_notificacion->save();
                }

            }
            else
            {
                $msj = "Ocurrío un error.";
            }
        }
        else
        {
            $msj = "Hubo un problema al realizar el pago";
        }
        Yii::$app->session->set('message', $msj);
        return $this->redirect(["account/detalle/" . $_GET['key']]);
     }

     public function actionConfirm2()
     {
        $table = new Users;
        if (Yii::$app->request->get())
        {
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = '1';
                    if ($activar->update()){
                        $utmp = $activar->getMcTusers()->One();
                        $subject = $utmp->user_name . ", Bienvenido a Mi Cargapp";
                        /*Yii::$app->mailer->compose('welcome_2', ['name'=>$utmp->user_name, 'username'=>$activar->email, 'password'=>$password])
                         ->setTo($activar->email2)
                         ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                         ->setSubject($subject)
                         ->send();*/
                        echo "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                    }else
                    {
                        echo "Ha ocurrido un error ...";
                        //echo "<meta http-equiv='refresh' content='1; ".Url::toRoute("site/login")."'>";
                        print_r($activar->getErrors());
                    }
                 }
                else
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else
            {
                return $this->redirect(["site/login"]);
            }
        }
     }

     public function actionConfirm3()
     {
        $table = new Users;
        if (Yii::$app->request->get())
        {
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = '1';
                    if ($activar->update()){
                        $utmp = $activar->getMcTcompanies()->One();
                        /*$subject = $utmp->company_rz . ", Bienvenido a Mi Cargapp";
                        Yii::$app->mailer->compose('welcome_2', ['name'=>$utmp->company_rz, 'username'=>$activar->email, 'password'=>$password])
                         ->setTo($activar->email2)
                         ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                         ->setSubject($subject)
                         ->send();*/
                        echo "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                    }else
                    {
                        echo "Ha ocurrido un error ...";
                        //echo "<meta http-equiv='refresh' content='1; ".Url::toRoute("site/login")."'>";
                        print_r($activar->getErrors());
                    }
                 }
                else
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else
            {
                return $this->redirect(["site/login"]);
            }
        }
     }
     
     public function actionRegistro()
     {
      if (!\Yii::$app->user->isGuest)
        return $this->redirect([User::validateRole()]);
      //Creamos la instancia con el model de validación
      $model = new FormRegister;
       
      $msg = null;
      $t = "";
      $ti = "";
       
      if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
       
      if ($model->load(Yii::$app->request->post()))
      {
           if($model->validate())
           {
                $table = new Users;
                $table->email = $model->email;
                $table->email2 = $model->email;
                $table->password = crypt($model->password, Yii::$app->params["salt"]);
                $table->authKey = $this->randKey("abcdef0123456789", 200);
                $table->accessToken = $this->randKey("abcdef0123456789", 200);
                $table->role = $model->type;
                if ($table->insert())
                {
                     $user = $table->find()->where(["email2" => $model->email])->one();
                     $id = urlencode($user->id);
                     $authKey = urlencode($user->authKey);
                     $ti = \yii\web\UploadedFile::getInstance($model, 'avatar');
                     $mc_user = new McTuser;
                     if($ti){
                         $rnd = $this->randKey("abcdef0123456789", 50);
                         $fileName = $rnd . str_replace(' ','_',$ti->name);
                         $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                         $mc_user->user_avatar = '/img/system_imgs/' . $fileName;
                     }else
                        $mc_user->user_avatar = "/img/system_imgs/pp.png";

                     $mc_user->user_name = $model->name;
                     $mc_user->user_phone = $model->phone;
                     $mc_user->fklogin = $id;

                     if(!$mc_user->insert()){
                        $error = "Error al guardar!!";
                        $t = $mc_user->getErrors();
                     }
                     /*$subject = "Confirmar registro";
                      
                     Yii::$app->mailer->compose('verify', ['name'=>$model->name, 'link'=>'http://micargapp.com'. Yii::getAlias('@web').'/site/confirm?id='.$id."&authKey=".$authKey])
                     ->setTo($user->email2)
                     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                     ->setSubject($subject)
                     ->send();*/
                     
                     $model->name = null;   
                     $model->email = null;
                     $model->phone = null;
                     $model->password = null;
                     $model->password_repeat = null;
                     $model->avatar = null;
                     $model->type = null;

                     $msg = "Se ha registrado correctamente, un correo de verificación ha sido enviado " . $model->email;
                }
                else
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
        }
        else
            $model->getErrors();
      }
      $this->layout = "register";
      Yii::$app->session->set('option',3);
      return $this->render("register", ["model" => $model, "msg" => $msg]);
     }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionRecoverpass()
    {
     //Instancia para validar el formulario
     $model = new FormRecoverPass;
     
     //Mensaje que será mostrado al usuario en la vista
     $msg = null;
     
     if ($model->load(Yii::$app->request->post()))
     {
      if ($model->validate())
      {
       //Buscar al usuario a través del email
       $table = Users::find()->where("email=:email", [":email" => $model->email]);
       
       //Si el usuario existe
       if ($table->count() == 1)
       {
        //Crear variables de sesión para limitar el tiempo de restablecido del password
        //hasta que el navegador se cierre
        $session = new Session;
        $session->open();
        
        //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
        $session["recover"] = $this->randKey("abcdef0123456789", 200);
        $recover = $session["recover"];
        
        //También almacenaremos el id del usuario en una variable de sesión
        //El id del usuario es requerido para generar la consulta a la tabla users y 
        //restablecer el password del usuario
        $table = Users::find()->where("email=:email", [":email" => $model->email])->one();
        $session["id_recover"] = $table->id;
        
        //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
        //para que lo introduzca en un campo del formulario de reseteado
        //Es guardada en el registro correspondiente de la tabla users
        $verification_code = $this->randKey("abcdef0123456789", 8);
        //Columna verification_code
        $table->verification_code = $verification_code;
        //Guardamos los cambios en la tabla users
        $table->save();
        
        //Creamos el mensaje que será enviado a la cuenta de correo del usuario
        $subject = "Recuperar password";
        $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
        $body .= "<strong>".$verification_code."</strong></p>";
        $body .= "<p><a href='http://localhost/micargapp_v1/web/site/resetpass'>Recuperar password</a></p>";

        //Enviamos el correo
        Yii::$app->mailer->compose()
        ->setTo($model->email)
        ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
        ->setSubject($subject)
        ->setHtmlBody($body)
        ->send();
        
        //Vaciar el campo del formulario
        $model->email = null;
        
        //Mostrar el mensaje al usuario
        $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
       }
       else //El usuario no existe
       {
        $msg = "Ha ocurrido un error";
       }
      }
      else
      {
       $model->getErrors();
      }
     }
     return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
    }

    public function actionResetpass()
    {
     //Instancia para validar el formulario
     $model = new FormResetPass;
     
     //Mensaje que será mostrado al usuario
     $msg = null;
     
     //Abrimos la sesión
     $session = new Session;
     $session->open();
     
     //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
     if (empty($session["recover"]) || empty($session["id_recover"]))
     {
      return $this->redirect(["site/index"]);
     }
     else
     {
      
      $recover = $session["recover"];
      //El valor de esta variable de sesión la cargamos en el campo recover del formulario
      $model->recover = $recover;
      
      //Esta variable contiene el id del usuario que solicitó restablecer el password
      //La utilizaremos para realizar la consulta a la tabla users
      $id_recover = $session["id_recover"];
      
     }
     
     //Si el formulario es enviado para resetear el password
     if ($model->load(Yii::$app->request->post()))
     {
      if ($model->validate())
      {
       //Si el valor de la variable de sesión recover es correcta
       if ($recover == $model->recover)
       {
        //Preparamos la consulta para resetear el password, requerimos el email, el id 
        //del usuario que fue guardado en una variable de session y el código de verificación
        //que fue enviado en el correo al usuario y que fue guardado en el registro
        $table = Users::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
        
        //Encriptar el password
        $table->password = crypt($model->password, Yii::$app->params["salt"]);
        
        //Si la actualización se lleva a cabo correctamente
        if ($table->save())
        {
         
         //Destruir las variables de sesión
         $session->destroy();
         
         //Vaciar los campos del formulario
         $model->email = null;
         $model->password = null;
         $model->password_repeat = null;
         $model->recover = null;
         $model->verification_code = null;
         
         $msg = "Enhorabuena, password reseteado correctamente, redireccionando a la página de login ...";
         $msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
        }
        else
        {
         $msg = "Ha ocurrido un error";
        }
        
       }
       else
       {
        $model->getErrors();
       }
      }
     }
     
     return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
     
    }

    public function actionIndex()
    {
        $this->layout = "index";
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRole()]);
        
        Yii::$app->session->set('option', 1);
        $category = McTcategory::find()->all();
        return $this->render('index', ['categorys'=>$category]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRole()]);
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
            return $this->redirect([User::validateRole()]);
        
        $this->layout = "login";
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->set('ak','');
        return $this->goHome();
    }
}

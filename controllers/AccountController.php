<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\McTcategory;
use app\models\McTsubcategory;
use app\models\FormCotizar;
use app\models\McTcarga;
use app\models\McTcity;
use app\models\McTproposal;
use app\models\McTuser;
use app\models\FormUpdateU;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Users;
use app\models\McTlogin;
use app\models\McTnew;
use app\models\McTcategorySubcategory;
use app\models\McTcontract;
use app\models\McTcontracteval;

class AccountController extends \yii\web\Controller
{

    public $layout = "session-user";

    public function behaviors()
    {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['index', 'cotizar', 'getsubs','cargas', 'detalle', 'perfil', 'noticias', 'payment',
                      'enterproposal','endproposal','paymentreturn','cancelload','evaluatecompany','deleteproposal'],
                      'allow' => true,
                      'roles' => ['@'],
                      'matchCallback' => function ($rule, $action) {
                          return User::isUser(Yii::$app->user->identity->id);
                      },
                  ]
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['get'],
              ],
          ],
      ];
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    public function actionIndex()
    {
    	Yii::$app->session->set('option', 1);
    	$category = McTcategory::find()->all();
      return $this->render('index', ['categorys'=>$category]);
    }

    public function actionCargas($status = null)
    {
        if($status != null && $status == '1' || $status == "3"){
          Yii::$app->session->set('option', 3);
          $this->layout = "index-no-ben";
          $option = $status == "1" ? 1 : 0;
          $u = McTuser::find()
            ->where('fklogin = :login', [':login' => Yii::$app->user->identity->id])
            ->One();
          $cargas = McTcarga::find()
                ->where("fkuser=:id", [":id" => $u->pkuser])
                ->andWhere('fkstatus = :status', [':status' => $status])
                ->all();
          if(count($cargas) == 0 and $status!="3")
            return $this->render('no-loads',["option"=>$option]);
          else if(count($cargas) == 0 and $status=="3")
            return $this->render('no-loads-end',["option"=>$option]);
          else
            return $this->render('cargas',["option"=>$option, "cargas"=>$cargas]);
        }else
          return $this->redirect(["account/cargas?status=1"]);
    }

    public function actionCotizar()
    {
      Yii::$app->session->set('option', 2);
    	$model = new FormCotizar;
    	if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()))
      	{
           if($model->validate())
           {
           		$table = new McTcarga;
           		$table->fkstatus = 1;
           		$table->load_citydestination = $model->destination;
           		$table->load_cityorigin = $model->origin;
           		$table->fksubcategory = $model->subcategory;
           		$table->fkuser = Yii::$app->session->get('userid');
           		$table->load_weight = $model->weight;
           		$table->load_value = $model->value;
           		$table->load_dimensions = $model->dimensions;
           		$table->load_comment = $model->comment;
              $table->load_ensure = $model->valueaseg;
              $table->latitude_origin = $model->latitude_orig;
              $table->longitude_origin = $model->longitude_orig;
              $table->latitude_destination = $model->latitude_dest;
              $table->longitude_destination = $model->longitude_dest;
           		if($table->insert()){
                //Se crea una notificación para todas las empresas
                $empresas = McTlogin::find()->where(['role'=>3,'activate'=>1])->all();
                $user = McTuser::find()->where(['fklogin'=>Yii::$app->user->id])->one();
                foreach ($empresas as $empresa) {
                  $model_notificacion = new McTnotificaciones();
                  $model_notificacion->fk_login=$empresa->id;
                  $model_notificacion->msj_notificacion="El Usuario <b>". $user->user_name ."</b> agregó una nueva carga <a href='".Yii::getAlias('@web')."/accountc/cotizar' onclick='modalClick(event)'> Ver Cargas</a>";
                  $model_notificacion->activa=1;
                  $model_notificacion->save();
                }
			    	    $this->layout = "index-no-ben";
			          return $this->render('confirm_new_cot');
           		}else{
    			    	$category = McTcategory::find($_GET["category"]);
	        		return $this->render('cotizar', ['category'=>$category, "model" => $model, 'error'=>$table->getErrors()]);	
           		}
           }
       	}	

      $citys = McTcity::find()->all();  
      $category = McTcategory::find()->all();
    	if(isset($_GET["category"])){
	      return $this->render('cotizar', ['citys'=>$citys, 'catid'=>$_GET["category"], 'category'=>$category, "model" => $model]);
	    }else
        return $this->render('cotizar', ['citys'=>$citys, 'catid'=>0, 'category'=>$category, "model" => $model]);
    }

    public function actionGetsubs(){
      $r = "";
        $subs = McTcategorySubcategory::find()
              ->where('fkcategory = :category', [':category' => $_GET["id"]])
              ->all();
        if(count($subs)>0){
            foreach($subs as $s)
                $r .= "<option value='".$s->pktcategory_subcategory."'>".$s->fksubcategory0->subcategory_name."</option>";
        }
        else{
            $r = "<option value=''>No hay subcategorias</option>";
        }
        return $r;
    }

    public function actionDetalle($id){
      $this->layout = "index-no-ben";
      $msj = Yii::$app->session->get('message');
      Yii::$app->session->set('message',null);
      if($id != null){
        $load = McTcarga::find()
          ->where('pkload = :code', [':code' => $id])
          ->One();
        if($load){
          if($load->fkstatus == 1){
            Yii::$app->session->set('option', 3);
            $proposals = McTproposal::find()
              ->where('fkload = :code', [':code' => $id])
              ->all();
            if(!$proposals || $load->load_ensure == 0)
              return $this->render('no_proposal', ['option'=>1]);
            else
              return $this->render('list_proposals', ['option'=>1, 'proposals'=>$proposals, 'msj' => $msj]);
          }else
          if($load->fkstatus == 3){
            Yii::$app->session->set('option', 3);
            $proposal = $load->getMcTpaymentloads()->One()->getFkproposal0()->One();
            return $this->render('list_proposals', ['option'=>0, 'proposal'=>$proposal, 'msj' => $msj]);
          }
        }else
          return $this->redirect(["account/cargas?status=1"]);
      }else
        return $this->redirect(["account/cargas?status=1"]);
    }

    public function actionPayment($pid = -1){
      $this->layout = "empty";
      $proposal = McTproposal::find()
        ->where('pkproposal = :id', [':id' => $pid])
        ->One();
      if($proposal){
        $u = McTuser::find()
            ->where('fklogin = :login', [':login' => Yii::$app->user->identity->id])
            ->One();
        return $this->render('payment',
          [
            'value'=>$proposal->proposal_paymin,
            'id'=>$proposal->pkproposal,
            'description'=> 'Pago de Transporte para el Contrato 000' . $proposal->pkproposal,
            'name'=> $u->user_name,
            'email'=> Yii::$app->user->identity->email2,
            'cid'=>$proposal->fkload
          ]);
      }else
        return $this->redirect(["account/cargas?status=1"]);
    }

    public function actionPaymentreturn(){
      $data = Yii::$app->request->post();
      $u = McTuser::find()
        ->where('fklogin = :login', [':login' => Yii::$app->user->identity->id])
        ->One();
      $cargas = McTcarga::find()
            ->where("fkuser=:id", [":id" => $u->pkuser])
            ->andWhere('fkstatus = :status', [':status' => 1])
            ->all();
      return $this->render('cargas',["option"=>1, "cargas"=>$cargas,'data'=>$data]);
    }

    public function beforeAction($action)
    {            
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionPerfil()
     {
      Yii::$app->session->set('option', 4);
      $model = new FormUpdateU;
       
      //Mostrará un mensaje en la vista cuando el usuario se haya registrado
      $msg = null;
      $t = "";
      $ti = "";
       
      //Validación mediante ajax
      if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
      {
          Yii::$app->response->format = Response::FORMAT_JSON;
          return ActiveForm::validate($model);
      }
       
      if ($model->load(Yii::$app->request->post()))
      {
           if($model->validate())
           {
                $table = McTlogin::findOne(Yii::$app->user->identity->id);
                $table->email = $model->email;
                $table->email2 = $model->email;
                if($model->password)
                  $table->password = crypt($model->password, Yii::$app->params["salt"]);
                $table->update();
                 $user = $table->find()->where(["email2" => $model->email])->one();
                 $id = urlencode($user->id);
                 $authKey = urlencode($user->authKey);
                 $ti = \yii\web\UploadedFile::getInstance($model, 'avatar');
                 $mc_user = McTuser::find()
                 ->where('fklogin = :id', [':id' => Yii::$app->user->identity->id])
                 ->One();
                 if($ti){
                     $rnd = $this->randKey("abcdef0123456789", 50);
                     $fileName = $rnd . str_replace(' ','_',$ti->name);
                     $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                     $mc_user->user_avatar = '/img/system_imgs/' . $fileName;
                     //$mc_user->user_avatar = "NA";
                 }
                $mc_user->user_name = $model->name;
                $mc_user->user_phone = $model->phone;
                $mc_user->fklogin = $id;
                $mc_user->update();
                $msg = "Se ha actualizado correctamente";
                $model->id = null;
                $model->name = null;   
                $model->email = null;
                $model->phone = null;
                $model->password = null;
                $model->password_repeat = null;
                $model->avatar = null;
                
        }
        else
            $model->getErrors();
      }
      $this->layout = "index-no-ben";
      $user = McTuser::find()
      ->where('fklogin = :id', [':id' => Yii::$app->user->identity->id])
      ->One();
      $email = $user->getFklogin0()->One()->email2;
      return $this->render("perfil", ["model" => $model, "msg" => $msg, 'user'=>$user, 'email'=>$email]);
     }

    public function actionNoticias($perfil = null)
    {
        $news= McTnew::find()
        ->where('new_type = :type', [':type' => 1])
        ->orderBy(['new_date'=>SORT_DESC])
        ->all();
        Yii::$app->session->set('option', 5);
        return $this->render('news',
            [
                'news'=>$news
            ]
        );
    }

    /*
    *   Funcion que permite entrar en una cotización
    *   Creado por: Rodrigo Boet
    *   Fecha: 30/06/2016
    *   @param $id Recibe el id de la propuesta
    *   @return Regresa un render de la vista proposalenter
    */
    public function actionEnterproposal($id)
    {
      $this->layout = "index-no-ben";
      $proposal = McTproposal::findOne($id);
      $msg = '';
      return $this->render('proposalenter',['proposal'=>$proposal,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite finalizar una cotización
    *   Creado por: Rodrigo Boet
    *   Fecha: 30/06/2016
    *   @param $id Recibe el id de la propuesta
    *   @return Regresa un render de la vista proposalenter
    */
    public function actionEndproposal($id)
    {
      $this->layout = "index-no-ben";
      $msg = '';
      $proposal = McTproposal::findOne($id);
      $load = $proposal->getFkload0()->one();
      $contrato = $load->getMcTcontracts()->one();
      if($load->fkstatus==3)
      {
        $msg = "Ya marcó como finalizada esta carga";
      }
      else
      {
        $load->fkstatus = 3;
        $load->save();
        $contrato->fk_status = 3;
        $contrato->save();
        $msg = 'Se finalizó correctamente la carga';
        $user = $load->fkuser0->user_name;
        //Se obtiene el número de contrato
        $zeros = '000000';
        $contract_type = ''.$contrato->pk_contract;
        $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
        //Se crea la notificación para la empresa
        $model_notificacion = new McTnotificaciones();
        $model_notificacion->fk_login=$company->id;
        $model_notificacion->msj_notificacion="El Usuario <b>". $user ."</b> finalizó el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/accountc/miscargas' onclick='modalClick(event)'> Ver Cargas</a>";
        $model_notificacion->activa=1;
        $model_notificacion->save();
      }
      return $this->render('proposalenter',['proposal'=>$proposal,'msg'=>$msg]);

    }

    /*
    *   Funcion que permite cancelar una carga
    *   Creado por: Rodrigo Boet
    *   Fecha: 28/07/2016
    *   @param $id Recibe el id de la carga
    *   @return Regresa un render de la vista cargas
    */
    public function actionCancelload($id)
    {
      $this->layout = "index-no-ben";
      $load = McTcarga::findOne($id);
      $load->fkstatus = 14;
      $load->save();
      $msj = "Se canceló la carga con éxito";
      $u = McTuser::find()
        ->where('fklogin = :login', [':login' => Yii::$app->user->identity->id])
        ->One();
      $cargas = McTcarga::find()
            ->where("fkuser=:id", [":id" => $u->pkuser])
            ->andWhere('fkstatus = :status', [':status' => 1])
            ->all();
      if(count($cargas) == 0)
        return $this->render('no-loads-end',["option"=>1,"msg"=>$msj]);
      else
        return $this->render('cargas',["option"=>1, "cargas"=>$cargas,"msg"=>$msj]);
    }

    /*
    *   Funcion que permite evaluar una empresa
    *   Creado por: Rodrigo Boet
    *   @param $id Recibe el id del contrato
    *   @param $nota Recibe la nota
    *   Fecha: 06/08/2016
    *   @return Regresa una respuesta ajax
    */
    public function actionEvaluatecompany($id,$nota)
    {
      Yii::$app->response->format = Response::FORMAT_JSON;
      $msj = '';
      $contrato = McTcontract::findOne($id);
      if (McTcontracteval::find()->where(['fkcontract'=>$id])->one()) 
      {
        $msj = "Ya evaluó a esta empresa";
      }
      else if (count($contrato)==0) {
        $msj = "Este contrato no existe";
      }
      else
      {
        $evaluation = new McTcontracteval;
        $evaluation->fkcontract = $id;
        $evaluation->eval = $nota;
        $evaluation->save();
        $msj = "Se envió su evaluación con éxito";
        $company_user = $contrato->getFkuserCompany()->one();
        $company = $company_user->getMcTcompanies()->one();
        if((float)$company->company_avg==0)
        {
          $company->company_avg = $nota;
        }
        else
        {
          $company->company_avg = ((float)$company->company_avg+$nota)/2.0;
        }
        $company->save();
      }
      return ["mensaje"=>$msj];
    }

    /*
    *   Funcion que eliminar una propuesta no pagada
    *   Creado por: Rodrigo Boet
    *   @param $id Recibe el id de la propuesta
    *   Fecha: 07/08/2016
    *   @return Regresa un render a las cargas
    */
    public function actionDeleteproposal($id)
    {
      $proposal = McTproposal::findOne($id);
      $proposal->delete();
      return $this->redirect(["account/cargas?status=1"]);
    }

}

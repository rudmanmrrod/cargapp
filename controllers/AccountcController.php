<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Users;
use app\models\McTcarga;
use app\models\McThideload;
use app\models\McTproposal;
use app\models\McTcompany;
use app\models\McTbank;
use app\models\McTlogin;
use app\models\McTcategory;
use app\models\McTsubcategory;
use app\models\McTcategorySubcategory;
use app\models\McTcompanyservices;
use app\models\McTfriendship;
use app\models\McTvehicle;
use app\models\McTvehicletype;
use app\models\McTreferences;
use app\models\McTcontract;
use app\models\FormChangePasswordCompany;
use app\models\McTcotizaremp;
use app\models\FormCotizarEmp;
use app\models\McTloadproposal;
use app\models\McUloadproposal;
use app\models\FormTransporterPayment;
use app\models\McTcontractemp;
use app\models\McTloadstatus;
use app\models\McTuserchattrans;
use app\models\McTuserchat;
use yii\web\ForbiddenHttpException;
use app\models\McTnotificaciones;

class AccountcController extends \yii\web\Controller
{

	public $layout = "session-company";
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'sendproposal', 'hideload', 'cotizar','update','delete',
                        'friendship','tranporterprofile','miscargas','usertcontract','userucontract',
                        'seecotization','tranportershortprofile','sendtproposal','listtproposal',
                        'acepttproposal','deletetproposal','listuproposal','senduproposal',
                        'aceptutproposal','deleteutproposal','confirm','transporterchat',
                        'endtproposal'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                          return User::isCompany(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        Yii::$app->session->set('company', $company->pkcompany);
    	Yii::$app->session->set('option', 1);
        $transporter = McTlogin::find()->where(['role'=>4])->all();
        return $this->render('index',['transporter'=>$transporter,'msg'=>'']);
    }

    /*
    *   Funcion que permite ver cotizaciones de usuario
    *   y enviar cotizaciones a transportadores
    *   Creado por: Rodrigo Boet
    *   Fecha: 25/06/2016
    *   @param $type Recibe el tipo de acción
    *   @return Dependiendo del tipo regresa un render a las vistas cotizar o cotizart
    */
    public function actionCotizar($type=null)
    {
    	Yii::$app->session->set('option', 2);
    	$option = 1;
        if($type)
        {
            $msg = '';
            $model = new FormCotizarEmp;
            if(Yii::$app->request->post())
            {   
                $model->load(Yii::$app->request->post());
                if($model->validate()){
                    $company = McTcompany::find()->where(['fklogin' => Yii::$app->user->id])->one();
                    $cotizaremp = new McTcotizaremp;
                    $cotizaremp->origen = $model->origin;
                    $cotizaremp->destino = $model->destination;
                    $cotizaremp->fk_vehicletype = $model->fk_vehicletype;
                    $cotizaremp->weight = $model->weight;
                    $cotizaremp->payment = $model->payment;
                    $cotizaremp->vehicle_number = $model->vehicle_number;
                    $cotizaremp->comment = $model->comment;
                    $cotizaremp->origin_latitude = $model->latitude_orig;
                    $cotizaremp->origin_longitude = $model->longitude_orig;
                    $cotizaremp->destination_latitude = $model->latitude_dest;
                    $cotizaremp->destination_longitude = $model->longitude_dest;
                    $cotizaremp->fkcompany = $company->pkcompany;
                    $cotizaremp->fkstatus = 1;
                    $cotizaremp->send_all = $model->alltcheck;
                    $cotizaremp->save();
                    $msg = 'Se creo la cotización éxitosamente';
                }
                else
                {
                    $msg = 'Ocurrió un error';
                }
                $model = new FormCotizarEmp;
                $vtype = McTvehicletype::find()->all();
                return $this->render('cotizart', ['option'=>$option,'model'=>$model,'vtype'=>$vtype,
                    'msg'=>$msg]);
            }
            else
            {
                $vtype = McTvehicletype::find()->all();
                return $this->render('cotizart', ['option'=>$option,'model'=>$model,'vtype'=>$vtype,
                    'msg'=>$msg]);
            }
        }
        else
        {
        	$sql = 'SELECT carga.* FROM mc_tcarga carga';
        	$sql .= " WHERE NOT EXISTS (SELECT 1 FROM mc_thideload hide 
                      WHERE hide.fkload = carga.pkload AND hide.fkcompany =
                      ".Yii::$app->session->get("userid").")";
    		$sql .= " GROUP BY carga.pkload ORDER BY carga.load_create DESC";
    		$loads = McTcarga::findBySql($sql)->all();
            return $this->render('cotizar', ['option'=>$option, 'loads'=>$loads]);
        }
    }

    public function actionHideload(){
    	if(isset($_POST["lid"])){
    		$hide = new McThideload;
    		$hide->fkload = $_POST["lid"];
    		$hide->fkcompany = Yii::$app->session->get("userid");
    		$hide->insert();
    		return $this->redirect(['accountc/cotizar']);
    	}else{
        	return $this->redirect(['accountc/']);
        }
    }

    public function actionSendproposal(){
    	if(isset($_POST["lid"])){
    		$option = 1;
    		$prop = new McTproposal;
    		$prop->fkload = $_POST["lid"];
    		$prop->fkcompany = Yii::$app->session->get("userid");
    		$prop->proposal_total = $_POST["vp"];
    		$prop->proposal_paymin = $_POST["pm"];
    		$prop->insert();
    		$sql = 'SELECT carga.* FROM mc_tcarga carga';
	    	$sql .= " WHERE NOT EXISTS (SELECT 1 FROM mc_thideload hide 
	                  WHERE hide.fkload = carga.pkload)";
			$sql .= " GROUP BY carga.pkload";
			$loads = McTcarga::findBySql($sql)->all();
            $load =  McTcarga::find()->where(["pkload" => $_POST["lid"]])->one();
            $utmp = $load->getFkuser0()->One();
			$msg = "La cotización ha sido enviada";
            //Notificación
            $user = McTlogin::find()->where(['id'=> $load->fkuser0->fklogin0->id])->one();
            $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
            $model_notificacion = new McTnotificaciones();
            $model_notificacion->fk_login=$user->id;
            $model_notificacion->msj_notificacion="La empresa <b>". $company->company_rz ."</b> te envío una propuesta <a href='".Yii::getAlias('@web')."/account/detalle/".$prop->fkload."' onclick='modalClick(event)'> Ver Propuesta</a>";
            $model_notificacion->activa=1;
            $model_notificacion->save();
            /*$subject = "Has recibido la Cotización para el servicio de transporte que necesitas.";
            Yii::$app->mailer->compose('new_cot_company', 
                [
                    'name'=>$utmp->user_name,
                    'origin'=>$load->load_cityorigin,
                    'destination'=>$load->load_citydestination,
                    'sub'=>$load->getFksubcategory0()->One()->subcategory_name,
                    'link'=>'http://micargapp.com'. Yii::getAlias('@web').'/account/detalle/'.$_POST["lid"]
                ])
             ->setTo($utmp->getFklogin0()->One()->email2)
             ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
             ->setSubject($subject)
             ->send();*/
             $_POST = array();    		
             return $this->render('cotizar',['option'=>$option, 'loads'=>$loads, 'msg'=>$msg]);
    	}else{
        	return $this->redirect(['accountc/']);
        }
    }

    /*
    *   Funcion que permite actualizar el perfil de la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 06/06/2016
    *   @param $id Recibe el id de la compañia
    *   @param $id_login Recibe el id del usuario
    *   @return Regresa un render de la vista update
    */
    public function actionUpdate($id,$id_login)
    {
        Yii::$app->session->set('option', 4);
        if(Yii::$app->user->id==$id_login)
        {
            $msg = '';   
            $form_register = new FormChangePasswordCompany;
            $model = McTcompany::findOne($id);
            $user = McTlogin::findOne($id_login);
            $services = McTcompanyservices::find()->where(['fkcompany'=>$id])->asArray()->all();
            if(Yii::$app->request->post())
            { 
                $previous_avatar = $model->company_avatar;
                if ($form_register->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($form_register);
                }
                if ($form_register->load(Yii::$app->request->post()))
                {
                    if($form_register->validate())
                    {
                        $user->email2 = $form_register->email;
                        $model->company_rz = $form_register->rz;
                        $model->company_nit = $form_register->nit;
                        $model->company_attendant = $form_register->name;
                        $model->company_address = $form_register->address;
                        $model->company_phone = $form_register->phone;
                        $model->company_review = $form_register->review;
                        $model->company_accountnumber = $form_register->numberaccount;
                        $model->company_accountname = $form_register->nameaccount;
                        $model->company_accounttype = $form_register->typeaccount;
                        $model->fkbank = $form_register->bank;
                        $ti = \yii\web\UploadedFile::getInstance($form_register, 'avatar');
                        if($ti)
                        {
                            $rnd = $this->randKey("abcdef0123456789", 50);
                            $fileName = $rnd . str_replace(' ','_',$ti->name);
                            $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                            $model->company_avatar = '/img/system_imgs/' . $fileName;
                         }else
                            $model->company_avatar = $previous_avatar;
                        if($form_register->new_password)
                        {
                            $user->password = crypt($form_register->new_password, Yii::$app->params["salt"]);
                        }
                        $model->save();
                        $user->save();
                        $msg = "Se actualizó éxitosamente";
                    }
                    else
                    {
                        $msg = "Ha ocurrido un error al llevar a cabo tu actualización";
                    }
                }            
                $form_register->rz = $model->company_rz;
                $form_register->nit = $model->company_nit;
                $form_register->name = $model->company_attendant;
                $form_register->address = $model->company_address;
                $form_register->phone = $model->company_phone;
                $form_register->review = $model->company_review;
                $form_register->numberaccount = $model->company_accountnumber;
                $form_register->nameaccount = $model->company_accountname;
                $form_register->typeaccount = $model->company_accounttype;
                $form_register->avatar = $model->company_avatar;
                $form_register->bank = $model->fkbank;
                $form_register->user = $user->email;
                $form_register->email = $user->email2;
                $listBanks = McTbank::find()->all(); 
                $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all(); 
                $categories = McTcategory::find()->all();
                $services = McTcompanyservices::find()->where(['fkcompany'=>$id])->asArray()->all();     
                return $this->render('update', [
                        'model' => $form_register,
                        'id' => $id,
                        'id_login' => $id_login,
                        'msg' => $msg,
                        'listBanks' => $listBanks,
                        'mc_tsubcategory' => $mc_tsubcategory,
                        'categories' => $categories,
                        'services' => $services,
                    ]);
            }
            else
            {
                $form_register->rz = $model->company_rz;
                $form_register->nit = $model->company_nit;
                $form_register->name = $model->company_attendant;
                $form_register->address = $model->company_address;
                $form_register->phone = $model->company_phone;
                $form_register->review = $model->company_review;
                $form_register->numberaccount = $model->company_accountnumber;
                $form_register->nameaccount = $model->company_accountname;
                $form_register->typeaccount = $model->company_accounttype;
                $form_register->avatar = $model->company_avatar;
                $form_register->bank = $model->fkbank;
                $form_register->user = $user->email;
                $form_register->email = $user->email2;
                $listBanks = McTbank::find()->all(); 
                $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all(); 
                $categories = McTcategory::find()->all();     
                return $this->render('update', [
                        'model' => $form_register,
                        'id' => $id,
                        'id_login' => $id_login,
                        'msg' => $msg,
                        'listBanks' => $listBanks,
                        'mc_tsubcategory' => $mc_tsubcategory,
                        'categories' => $categories,
                        'services' => $services,
                    ]);
            }
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite enviar una solicitud de amistad a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 13/06/2016
    *   Modificado para agregar la notificacion al momento de generar la amistad
    *   Modificado por: Ing. Leonel P. Hernandez M.
    *   Fecha: 14/07/2016
    *   @param $friendId Recibe el id del usuario al que se le envía la solicitud
    *   @return Regresa un render de la vista index
    */
    public function actionFriendship($friendId)
    {
        $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        Yii::$app->session->set('company', $company->pkcompany);
        Yii::$app->session->set('option', 1);
        $transporter = McTlogin::find()->where(['role'=>4])->all();
        $msg='';
        if(!McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'fkuser_recive'=>$friendId])->one())
        {
            $model = new McTfriendship();
            $model->fkuser_send = Yii::$app->user->id;
            $model->fkuser_recive = $friendId;
            $model->send_date = date('Y-m-d H:i:s');
            //Notificación
            $model_notificacion = new McTnotificaciones();
            $model_notificacion->fk_login=$friendId;
            $model_notificacion->msj_notificacion="La empresa ". $company->company_rz ." quiere ser tu amigo";
            $model_notificacion->activa=1;
            $model_notificacion->type=5;
            $model->save();
            $model_notificacion->save();
            $msg = 'Se envió la solicitud con éxito';
        }
        else
        {
            $msg='Ya envió una solicitud a este transportador';
        }
        return $this->render('index',['transporter'=>$transporter,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite confirmar una solicitud de amistad enviada por un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 15/07/2016
    *   @param $id Recibe el id del transportador
    *   @return Regresa un render de la vista index
    */
    public function actionConfirm($id)
    {
        $model = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'fkuser_recive'=>$id])->one();
        if($model)
        {
            $model->status = 1;
            $model->save();
            //Se crea la notificacion de amistad
            $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
            $model_notificacion = new McTnotificaciones();
            $model_notificacion->fk_login=$id;
            $model_notificacion->msj_notificacion="La empresa ". $company->company_rz ." acepto tu solicitud de amistad";
            $model_notificacion->activa=1;
            $model_notificacion->type=5;
            $model_notificacion->save();
            $msg = 'Se acepto la solicitud éxitosamente';
        }
        else
        {
            $msg = 'No existe esta solicitud de amistad';
        }
        $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        Yii::$app->session->set('company', $company->pkcompany);
        Yii::$app->session->set('option', 1);
        $transporter = McTlogin::find()->where(['role'=>4])->all();
        return $this->render('index',['transporter'=>$transporter,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite elimnar una solicitud y/o amistad con un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 16/06/2016
    *   @param $id Recibe el id del usuario con el que se elimina la solictud
    *   @return Regresa un render de la vista index
    */
    public function actionDelete($id)
    {
        $model = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'fkuser_recive'=>$id])->one();
        if($model)
        {
            $model->delete();
            $msg = 'Se borró la solictud y/o amistad';
        }
        else
        {
            $msg = 'Este usuario y usted no son amigos';
        }
        $company = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        Yii::$app->session->set('company', $company->pkcompany);
        Yii::$app->session->set('option', 1);
        $transporter = McTlogin::find()->where(['role'=>4])->all();
        return $this->render('index',['transporter'=>$transporter,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite revisar el perfil de un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 16/06/2016
    *   @param $id Recibe el id del usuario con el que se revisará
    *   @return Regresa un render por ajax de la vista profile
    */
    public function actionTranporterprofile($id)
    {
        $model = Users::findOne($id);
        $profile = $model->getMcTusers()->One(); 
        $vehicle = McTvehicle::find()->where(['fkuser' => $profile->pkuser])->one();
        $references = McTreferences::find()->where(['fkuser'=>$profile->pkuser])->all();
        $attachment = $profile->getMcTattachments()->all();
        return $this->renderAjax('profile', [
            'model' => $model,
            'profile' => $profile,
            'vehicle' => $vehicle,
            'attachment' => $attachment,
            'references' => $references,
        ]);
    }

    /*
    *   Funcion que permite revisar las cargas de la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 22/06/2016
    *   @param $id Recibe el id de la vista a la que se accedera
    *   @return Regresa un render de la vista miscargas
    */
    public function actionMiscargas($perfil = null)
    {
        Yii::$app->session->set('option', 3);
        $contract = McTcontract::find()->where(['fkuser_company'=>Yii::$app->user->id])->all();
        if(!$perfil)
        {
            $perfil = '1';
        }
        else
        {
            $contract = McTcotizaremp::find()->all();
        }
        return $this->render('miscargas'.$perfil, ['contract'=>$contract]);
    }

    /*
    *   Funcion que permite ver asignar transportadores a una cotización de usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 23/06/2016
    *   @param $id_contract Recibe el id del contrato
    *   @return Regresa un render de la vista miscargastcontract
    */
    public function actionUsertcontract($id_contract)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $model = new FormTransporterPayment;
        if(McTcontract::find()->where(['fkuser_company'=>Yii::$app->user->id,'pk_contract'=>$id_contract])->one())
        {
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            return $this->render('miscargastcontract', ['option'=>$option,'msg'=>$msg,
                'id_contract'=>$id_contract,'transporter'=>$transporter,'model'=>$model,
                'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite ver el usuario con el que la empresa tiene un contrato
    *   Creado por: Rodrigo Boet
    *   Fecha: 23/06/2016
    *   @param $id_contract Recibe el id del contrato
    *   @return Regresa un render de la vista miscargasucontract
    */
    public function actionUserucontract($id_contract)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $contract = McTcontract::find()->where(['fkuser_company'=>Yii::$app->user->id])->one();
        if($contract)
        {
            $carga = $contract->getFkCarga()->one();
            $user = $carga->getFkuser0()->one();
            $company = $contract->fkuserCompany->getMcTcompanies()->one()->pkcompany;
            $company_percent = McTcompanyservices::find()
            ->where(['fksubcategory'=>$carga->fksubcategory0->fksubcategory0->pksubcategory,
                'fkcompany'=>$company])->one();
            return $this->render('miscargasucontract', ['option'=>$option,'msg'=>$msg,
                'id_contract'=>$id_contract,'contrato'=>$contract, 'carga'=>$carga,
                'user'=>$user,'company_percent'=>$company_percent]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite ver una cotización propia enviada
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @return Regresa un render de la vista miscargasucontract
    */
    public function actionSeecotization($id_cot)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        $model = new FormTransporterPayment;
        if($cotization && $empresa)
        {
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            return $this->render('miscargastmicarga', ['msg'=>$msg, 'cotization' => $cotization,
                'id_cot'=>$id_cot,'transporter'=>$transporter,'loadproposal'=>$loadproposal,'model'=>$model]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite cargar de forma resumida el perfil de un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $plate Recibe la placa de vehículo
    *   @return Regresa un render por ajax de la vista shortprofile
    */
    public function actionTranportershortprofile($plate)
    {
        $vehicle = McTvehicle::find()->where(['vehicle_licenceplate' => $plate])->one();
        $profile = $vehicle->getFkuser0()->One();
        $model = $profile->getFklogin0()->one();
                 
        return $this->renderAjax('shortprofile', [
            'model' => $model,
            'profile' => $profile,
            'vehicle' => $vehicle,
        ]);
    }

    /*
    *   Funcion que permite enviar una cotización a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @param $plate Recibe la placa del transportador
    *   @return Regresa un render de la vista miscargasucontract
    */
    public function actionSendtproposal($id_cot,$plate)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        $loadproposal = '';
        if($cotization && $empresa)
        {
            $vehicle = McTvehicle::find()->where(['vehicle_licenceplate' => $plate])->one();
            $profile = $vehicle->getFkuser0()->One();
            $model = $profile->getFklogin0()->one();
            $friend = McTfriendship::find()->where(['fkuser_recive'=>$model->id,'status'=>1])->one();
            if($friend)
            {
                if(McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'fktransportador'=>$model->id])->one())
                {
                    $msg = "Ya envío esta carga a este transportador";
                }
                else
                {
                    $proposals = count(McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot])->all());
                    if($proposals>=$cotization->vehicle_number)
                    {
                        $msg = 'No puede agregar más de '.$cotization->vehicle_number.' transportador(es) a la carga';
                    }
                    else
                    {
                        $loadproposal = new McTloadproposal();
                        $notificacion_proposal = new McTnotificaciones();
                        $loadproposal->fkcotizacionemp = $id_cot;
                        $loadproposal->fktransportador = $model->id;
                        $loadproposal->status = 2;
                        $loadproposal->payment = $cotization->payment;
                        $loadproposal->save();
                        //Notificación
                        $notificacion_proposal->fk_login = $model->id;
                        $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." te agrego a una carga";
                        $notificacion_proposal->activa = 1;
                        $notificacion_proposal->type = 1;
                        $notificacion_proposal->save();

                        $msg = "Se envío la carga con éxito";
                    }
                }
            }
            else
            {
                $msg = 'No se puede envíar la solicitud a ese transportador';
            }
            $model = new FormTransporterPayment;
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            return $this->render('miscargastmicarga', ['msg'=>$msg, 'cotization' => $cotization,
                'id_cot'=>$id_cot,'transporter'=>$transporter,'loadproposal'=>$loadproposal,'model'=>$model]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite listar el estado de solicitudes de un transportador
    *   a una cotización
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionListtproposal($id_cot)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        if($cotization && $empresa)
        {
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'hide'=>0])->all();
            return $this->render('miscargastlist', ['msg'=>$msg,
                'id_cot'=>$id_cot,'cotization'=>$cotization,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite aceptar un transportador en una cotización envíada
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @param $id_trans Recibe el id del transportador
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionAcepttproposal($id_cot,$id_trans)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        if($cotization && $empresa)
        {
            if(McTcontractemp::find()->where(['fkcotizaremp'=>$id_cot,'fkuser'=>$id_trans])->one())
            {
                $msg = 'Ya este usuario tiene un contrato';
            }
            else
            {
                $proposals = count(McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot])->all());
                if($proposals>=$cotization->vehicle_number)
                {
                    $msg = 'No puede agregar más de '.$cotization->vehicle_number.' transportadores a la carga';
                }
                else
                {
                    $transportador = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'fktransportador'=>$id_trans])->one();
                    $transportador->status = 1;
                    $transportador->save();
                    $msg = 'Se acepto la solicitud éxitosamente';
                    //Se crea el contrato
                    $contract = new McTcontractemp;
                    $contract->fk_status = 1;
                    $contract->fkcotizaremp = $id_cot;
                    $contract->fkuser = $id_trans;
                    $contract->save();
                    //Se crea el estado de la carga
                    $loadstatus = new McTloadstatus;
                    $loadstatus->fkcontractemp = $contract->pk_contract;
                    $loadstatus->fkstatus = 11;
                    $loadstatus->save();
                    //Se crea la instancia del chat
                    $chat = new McTuserchattrans;
                    $chat->fkcontractemp = $contract->pk_contract;
                    $chat->save();
                    //Notificación
                    $notificacion_proposal->fk_login = $id_trans;
                    $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." te aceptó en la carga";
                    $notificacion_proposal->activa = 1;
                    $notificacion_proposal->type = 2;
                    $notificacion_proposal->save();
                }
            }
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'hide'=>0])->all();
            return $this->render('miscargastlist', ['msg'=>$msg,
                'id_cot'=>$id_cot,'cotization'=>$cotization,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite borrar la cotización envíada a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 26/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @param $id_trans Recibe el id del transportador
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionDeletetproposal($id_cot,$id_trans)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        if($cotization && $empresa)
        {
            $transportador = $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'fktransportador'=>$id_trans])->one();
            $transportador->hide = 1;
            $transportador->save();
            $msg = 'Se elimino la solicitud éxitosamente';
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'hide'=>0])->all();
            return $this->render('miscargastlist', ['msg'=>$msg,
                'id_cot'=>$id_cot,'cotization'=>$cotization,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite listar el estado de solicitudes de un transportador
    *   a una cotización
    *   Creado por: Rodrigo Boet
    *   Fecha: 28/06/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionListuproposal($id_contract)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $contract = McTcontract::find()->where(['fkuser_company'=>Yii::$app->user->id,'pk_contract'=>$id_contract])->one();
        if($contract && $empresa)
        {
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'hide'=>0])->all();
            return $this->render('miscargasulist', ['msg'=>$msg,
                'id_contract'=>$id_contract,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite enviar una cotización a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 28/06/2016
    *   @param $id_contract Recibe el id del contrato
    *   @return Regresa un render de la vista miscargastcontract
    */
    public function actionSenduproposal($id_contract)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $model = new FormTransporterPayment;
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $contract = McTcontract::find()->where(['pk_contract'=>$id_contract,'fkuser_company'=>Yii::$app->user->id])->one();
        $loadproposal = '';
        if($contract && $empresa)
        {
            $model->load(Yii::$app->request->post());
            $vehicle = McTvehicle::find()->where(['vehicle_licenceplate' => $model->plate])->one();
            $profile = $vehicle->getFkuser0()->One();
            $user = $profile->getFklogin0()->one();
            $friend = McTfriendship::find()->where(['fkuser_recive'=>$user->id,'status'=>1])->one();
            if($friend)
            {
                if(McUloadproposal::find()->where(['fkcontract'=>$id_contract,'fktransportador'=>$user->id])->one())
                {
                    $msg = "Ya envío esta carga a este transportador";
                }
                else
                {
                    $proposal_total = $contract->getFkProposal()->one()->proposal_total;
                    $carga_category = $contract->getFkCarga()->one()->fksubcategory;
                    $empresa_comision = $empresa->getMcTcompanyservices()->where(['fksubcategory'=>$carga_category])->one()->companyservices_percent;
                    $msg = $proposal_total;
                    $pago_minimo = (($proposal_total*$empresa_comision)/100);
                    if($model->payment<$pago_minimo)
                    {
                        $msg = 'El monto mínimo que debe pagar al transportador es '.$pago_minimo.' COP';
                    }
                    else
                    {
                        $loadproposal = new McUloadproposal();
                        $loadproposal->fkcontract = $id_contract;
                        $loadproposal->fktransportador = $user->id;
                        $loadproposal->payment = $model->payment;
                        $loadproposal->status = 2;
                        $loadproposal->save();
                        $msg = "Se envío la carga con éxito";
                        //Notificación
                        $notificacion_proposal = new McTnotificaciones();
                        $notificacion_proposal->fk_login = $user->id;
                        $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." te agrego a una carga";
                        $notificacion_proposal->type = 1;
                        $notificacion_proposal->save();
                    }
                }
            }
            else
            {
                $msg = 'No se puede envíar la solicitud a ese transportador';
            }
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            $model = new FormTransporterPayment;
            return $this->render('miscargastcontract', ['msg'=>$msg, 'contract' => $contract,
                'id_contract'=>$id_contract,'transporter'=>$transporter,'loadproposal'=>$loadproposal,
                'model'=>$model]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite aceptar un transportador en una cotización envíada
    *   Creado por: Rodrigo Boet
    *   Fecha: 28/06/2016
    *   @param $id_contract Recibe el id del contrato
    *   @param $id_trans Recibe el id del transportador
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionAceptutproposal($id_contract,$id_trans)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $model = new FormTransporterPayment;
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $contract = McTcontract::find()->where(['pk_contract'=>$id_contract,'fkuser_company'=>Yii::$app->user->id])->one();
        if($contract && $empresa)
        {
            if(McTcontractemp::find()->where(['fkcontract'=>$id_contract,'fkuser'=>$id_trans])->one())
            {
                $msg = 'Ya este usuario tiene un contrato';
            }
            else
            {
                $proposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'fktransportador'=>$id_trans])->one();
                $proposal->status = 1;
                $proposal->save();
                $msg = 'Se acepto la solicitud éxitosamente';
                //Se crea el contrato
                $contract = new McTcontractemp;
                $contract->fk_status = 1;
                $contract->fkcontract = $id_contract;
                $contract->fkuser = $id_trans;
                $contract->save();
                //Se crea el estado de la carga
                $loadstatus = new McTloadstatus;
                $loadstatus->fkcontractemp = $contract->pk_contract;
                $loadstatus->fkstatus = 11;
                $loadstatus->save();
                //Se crea la instancia del chat
                $chat = new McTuserchattrans;
                $chat->fkcontractemp = $contract->pk_contract;
                $chat->save();
                //Notificación
                $notificacion_proposal->fk_login = $id_trans;
                $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." te aceptó en la carga";
                $notificacion_proposal->activa = 1;
                $notificacion_proposal->type = 2;
                $notificacion_proposal->save();
            }
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'hide'=>0])->all();
            return $this->render('miscargasulist', ['msg'=>$msg,
                'id_contract'=>$id_contract,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion que permite borrar la cotización envíada a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 28/06/2016
    *   @param $id_cot Recibe el id del contrato
    *   @param $id_trans Recibe el id del transportador
    *   @return Regresa un render de la vista miscargastlist
    */
    public function actionDeleteutproposal($id_contract,$id_trans)
    {
        Yii::$app->session->set('option', 3);
        $option = 1;
        $msg = '';
        $model = new FormTransporterPayment;
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $contract = McTcontract::find()->where(['pk_contract'=>$id_contract,'fkuser_company'=>Yii::$app->user->id])->one();
        if($contract && $empresa)
        {
            $proposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'fktransportador'=>$id_trans])->one();
            $proposal->hide = 1;
            $proposal->save();
            $msg = 'Se elimino la solicitud éxitosamente';
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'hide'=>0])->all();
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'hide'=>0])->all();
            return $this->render('miscargasulist', ['msg'=>$msg,
                'id_contract'=>$id_contract,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion para abrir una instancia de chat con el transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 01/08/2016
    *   @param $id_contract Recibe el id del contrato
    *   @param $id_transcontract Recibe el id contrato con el transportador
    *   @param $action Recibe la acción a la que rediccionará
    *   @return Regresa un render de la vista transporterchat
    */
    public function actionTransporterchat($id_contract,$id_transcontract,$action)
    {
        Yii::$app->session->set('option', 3);
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        if($action == "seecotization")
        {
            $sql = "SELECT * FROM mc_tcontractemp INNER JOIN mc_tcotizaremp WHERE fkcotizaremp=pkcotizaremp ";
            $sql .= "AND (SELECT 1 FROM mc_tcompany WHERE mc_tcompany.pkcompany=mc_tcotizaremp.fkcompany ";
            $sql .= "AND mc_tcompany.fklogin=".Yii::$app->user->id.") AND mc_tcontractemp.pk_contract=".$id_contract;
            $contract = McTcontractemp::findBySql($sql)->one();
        }
        else
        {
            $contract = McTcontract::find()->where(['pk_contract'=>$id_contract,'fkuser_company'=>Yii::$app->user->id])->one();
        }
        if($contract && $empresa)
        {
            return $this->render('transporterchat', ['id_contract'=>$id_contract,'id_transcontract'=>$id_transcontract,
                'action'=>$action]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion para finalizar una cotización enviada por la empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 14/08/2016
    *   @param $id_cot Recibe el id de la cotización
    *   @param $id_load Recibe el id del estado de la carga
    *   @return Regresa un render de la vista miscargastmicarga
    */
    public function actionEndtproposal($id_cot,$id_load)
    {
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $cotization = McTcotizaremp::find()->where(['pkcotizaremp'=>$id_cot,'fkcompany'=>$empresa->pkcompany])->one();
        $msg = "";
        if($cotization && $empresa)
        {
            $loadstatus = McTloadstatus::findOne($id_load);
            if($loadstatus->fkstatus == 3)
            {
                $msg = "Ya finalizó esta carga";
            }
            else
            {
                $loadstatus->fkstatus = 3;
                $loadstatus->save();
                $msg = "Se marco la carga como entregada con éxito";
                $contrato = $loadstatus->getFkcontractemp0()->one();
                $contrato->fk_status = 3;
                $contrato->save();
                $sql = "SELECT * FROM mc_tloadstatus INNER JOIN mc_tcontractemp ";
                $sql .= "WHERE mc_tloadstatus.fkcontractemp=mc_tcontractemp.pk_contract ";
                $sql .= "AND mc_tcontractemp.fkcotizaremp=".$cotization->pkcotizaremp;
                $status = McTloadstatus::findBySql($sql)->all();;
                $end = True;
                //Se crea la notificación
                $notificacion_proposal->fk_login = $contrato->fkuser;
                $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." finalizó la carga";
                $notificacion_proposal->type = 3;
                $notificacion_proposal->save();
                //Se comprueba si se finalizaron todas las cargas
                foreach ($status as $stat) {
                    if($stat->fkstatus!=3)
                    {
                        $end = False;
                    }
                }
                //Si se finalizaron todas las cargas la cotización se toma for finalizada
                if($end)
                {
                    $cotization->fkstatus = 3;
                    $cotization->save();
                    $msg .="<br>Se finalizó la cotización con éxito";
                }
            }
            $loadproposal = McTloadproposal::find()->where(['fkcotizacionemp'=>$id_cot,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            return $this->render('miscargastmicarga', ['msg'=>$msg, 'cotization' => $cotization,
                'id_cot'=>$id_cot,'transporter'=>$transporter,'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }

    /*
    *   Funcion para finalizar una carga de transportador enviada por una usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 14/08/2016
    *   @param $id_contract Recibe el id del contrato
    *   @param $id_load Recibe el id del estado de la carga
    *   @return Regresa un render de la vista miscargastcontract
    */
    public function actionEnduproposal($id_contract,$id_load)
    {
        $empresa = McTcompany::find()->where(['fklogin'=>Yii::$app->user->id])->one();
        $contract = McTcontract::find()->where(['pk_contract'=>$id_contract,'fkuser_company'=>Yii::$app->user->id])->one();
        $msg = "";
        if($contract && $empresa)
        {
            $loadstatus = McTloadstatus::findOne($id_load);
            if($loadstatus->fkstatus == 3)
            {
                $msg = "Ya finalizó esta carga";
            }
            else
            {
                $loadstatus->fkstatus = 3;
                $loadstatus->save();
                $msg = "Se marco la carga como entregada con éxito";
                $contrato = $loadstatus->getFkcontractemp0()->one();
                $contrato->fk_status = 3;
                $contrato->save();
                //Se crea la notificación
                $notificacion_proposal->fk_login = $contrato->fkuser;
                $notificacion_proposal->msj_notificacion="La empresa ". $empresa->company_rz ." finalizó la carga";
                $notificacion_proposal->type = 3;
                $notificacion_proposal->save();
            }
            $loadproposal = McUloadproposal::find()->where(['fkcontract'=>$id_contract,'status'=>1])->all();
            $transporter = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'status'=>1])->all();
            return $this->render('miscargastcontract', ['option'=>$option,'msg'=>$msg,
                'id_contract'=>$id_contract,'transporter'=>$transporter,'model'=>$model,
                'loadproposal'=>$loadproposal]);
        }
        else
        {
            throw new  ForbiddenHttpException("No Tiene permisos para acceder a este sitio");
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Users;
use app\models\FormRegister;
use app\models\FormRegisterCompany;
use yii\widgets\ActiveForm;
use yii\web\Response;
use app\models\McTuser;
use app\models\McTbank;
use app\models\McTcompany;
use app\models\McTcategory;
use app\models\McTsubcategory;
use app\models\McTcompanyservices;
use app\models\FormNews;
use app\models\McTnew;
use app\models\McTlogin;
use app\models\LoginForm;
use app\models\FormChangePassword;
use app\models\FormChangePasswordCompany;
use app\models\McTcategorySubcategory;
use app\models\McTcontract;
use app\models\McTcontractemp;
use app\models\McTcontractpayment;
use app\models\McTtransporterpayment;
use app\models\FormAdminTransporterPayment;
use app\models\FormDevolucion;
use app\models\McTadminlog;
use yii\web\ForbiddenHttpException;

class AdminController extends \yii\web\Controller
{

	public $layout = "session-admin";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'perfiles', 'noticias', 'inhabilitar','update','updatecompany','delete',
                        'deletecompany','cargas','cargast','cargasg','paycontract','refundcontract','transporterpayment',
                        'transportermakepaid','repaycontract','noticiasedit','noticiasdelete','habilitar','historylog'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

	private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
            $key .= $str[rand($start, $limit)];
        return $key;
    }

    public function actionIndex($type=null,$contract=null)
    {
    	Yii::$app->session->set('option', 1);
    	$o1 = 1;
    	$o2 = 1;
    	$o3 = null;
        if($contract==1 or $contract==3){
            $contratos = McTcontract::find()->where(['fk_status'=>$contract])->all();
        }
        else{
            $contratos = McTcontract::find()->all();
            $contract = '';
        }
        if($type)
        {
            $contratos = McTcontractemp::find()->all();
        }
        return $this->render('index'.$type,['contratos'=>$contratos,'option1'=>$o1,'option2'=>$o2,'option3'=>$o3,
            'contract'=>$contract]);
    }

    /*
    *   Funcion que permite ver los contratos empresa-usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 22/06/2016
    *   @return Regresa un render de la vista cargas
    */
    public function actionCargas()
    {   
        Yii::$app->session->set('option', 3);
        $msg = '';
        $contract = McTcontract::find()->all();
        return $this->render('cargas',['contratos'=>$contract,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite ver los contratos de los transportadores
    *   Creado por: Rodrigo Boet
    *   Fecha: 29/06/2016
    *   @return Regresa un render de la vista cargast
    */
    public function actionCargast()
    {  
        $msg = '';   
        Yii::$app->session->set('option', 3);
        $contratos = McTcontractemp::find()->all();
        return $this->render('cargast',['contratos'=>$contratos,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite ver las garantías de los contratos
    *   Creado por: Rodrigo Boet
    *   Fecha: 22/06/2016
    *   @return Regresa un render de la vista cargasg
    */
     public function actionCargasg()
    {   
        Yii::$app->session->set('option', 3);
        $contract = McTcontract::find()->all();
        return $this->render('cargasg',['contratos'=>$contract]);
    }

    public function actionNoticias($perfil = null)
    {
    	$model = new FormNews;
        $message = "";
        $type = "";
    	if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
	    {
	        Yii::$app->response->format = Response::FORMAT_JSON;
	        return ActiveForm::validate($model);
	    }
        if ($model->load(Yii::$app->request->post()))
        {
           if($model->validate())
           {
           	    $new = new McTnew;
                $new->new_message = $model->message;
                $new->new_type = $model->type;
                $new->new_title = $model->title;
                $u = McTuser::find()
                ->where('fklogin = :login', [':login' => Yii::$app->user->identity->id])
                ->One();
                $new->fkuser = $u->pkuser;
                $ti = \yii\web\UploadedFile::getInstance($model, 'image');
                if($ti){
                    $rnd = $this->randKey("abcdef0123456789", 50);
                    $fileName = $rnd . str_replace(' ','_',$ti->name);
                    $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                    $new->new_image = '/img/system_imgs/' . $fileName;
                }
                if($new->insert()){
                    $message = "Se ha publicado correctamente";
                    //Se crea la notificación para todos los transportadores
                    if($perfil==2)
                    {
                        $transporters = McTlogin::find()->where(['role'=>4,'activate'=>1])->all();
                        foreach ($transporters as $transporter) {
                            $notificacion_proposal->fk_login = $transporter->id;
                            $notificacion_proposal->msj_notificacion="Existe una nueva noticia para el transportador";
                            $notificacion_proposal->type = 4;
                            $notificacion_proposal->save();
                        }
                    }
                    $type = "true";
                    $model->message = null;
                    $model->type = null;
                    $model->title = null;
                    //Log
                    $log = new McTadminlog;
                    $log->fklogin = Yii::$app->user->id;
                    $log->message = "Creó la noticia con el id #".$new->pknew;
                    $log->save();
                }else{
                    $message = "Hubo un error al intentar publicar";
                    $type = "false";
                }
           }
        }

        $news = McTnew::find()->all();
        if($perfil != null && $perfil == '1' || $perfil == "2"){
            $news= McTnew::find()
            ->where('new_type = :type', [':type' => $perfil])->orderBy('new_date DESC')
            ->all();
            Yii::$app->session->set('option', 4);
        	return $this->render('news',
        		[
        			'model'=>$model,
        			'option'=>$perfil,
                    'type'=>$type,
                    'message'=>$message,
                    'news'=>$news
        		]
        	);
        }else
          return $this->redirect(["admin/noticias?perfil=1"]);
    }

    
    /*
    *   Funcion que permite editar una noticia
    *   Creado por: Rodrigo Boet
    *   Fecha: 29/07/2016
    *   @param $id Recibe el id de la noticia
    *   @return Regresa un render de la vista actualizar noticia
    */
    public function actionNoticiasedit($id)
    {
        $new = McTnew::findOne($id);
        $model = new FormNews();
        $msg = '';
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if(Yii::$app->request->post())
        {
            $model->load(Yii::$app->request->post());
            $new->new_title = $model->title;
            $new->new_message = $model->message;
            $ti = \yii\web\UploadedFile::getInstance($model, 'image');
            if($ti){
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$ti->name);
                $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                $new->new_image = '/img/system_imgs/' . $fileName;
            }
            $new->save();
            $msg = "Se actualizó la noticia con éxito";
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Actualizó la noticia con el id #".$new->pknew;
            $log->save();

            $model->title = $new->new_title;
            $model->message = $new->new_message;
            $model->image = $new->new_image;
            $model->type = $new->new_type;
            return $this->render('updatenew',['model'=>$model,'msg'=>$msg,'id'=>$id]);
        }
        else
        {
            $model->title = $new->new_title;
            $model->message = $new->new_message;
            $model->image = $new->new_image;
            $model->type = $new->new_type;
            return $this->render('updatenew',['model'=>$model,'msg'=>$msg,'id'=>$id]);
        }
    }

    /*
    *   Funcion que permite eliminar una noticia
    *   Creado por: Rodrigo Boet
    *   Fecha: 29/07/2016
    *   @param $id Recibe el id de la noticia
    *   @return Regresa un render de la vista actualizar noticia
    */
    public function actionNoticiasdelete($id,$perfil)
    {
        $new = McTnew::findOne($id);
        if($new)
        {
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Borró la noticia con el id #".$new->pknew;
            $log->save();
            //Se elimina la noticia
            if($new->new_image)
                unlink(Yii::getAlias('@webroot').$new->new_image);
            $new->delete();
            $msg = "Se borró la noticia con éxito";
        }
        else
        {
            $msg = "La noticia solicitada no existe";
        }
        $message = "";
        $model = new FormNews;
        $type = "";
        $news= McTnew::find()
        ->where('new_type = :type', [':type' => $perfil])->orderBy('new_date DESC')
        ->all();
        Yii::$app->session->set('option', 4);
        return $this->render('news',
            [
                'model'=>$model,
                'option'=>$perfil,
                'type'=>$type,
                'message'=>$message,
                'msg'=>$msg,
                'news'=>$news
            ]
        );
    }

    public function actionPerfiles($perfil = null)
    {
    	$listBanks = [];
    	$errors = "";
    	if($perfil != 3)
    		$model = new FormRegister;
    	else{
    		$model = new FormRegisterCompany;
    		$listBanks = McTbank::find()->all();
    	}
    	$msg = "";
    	Yii::$app->session->set('option', 2);

    	if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
	    {
	        Yii::$app->response->format = Response::FORMAT_JSON;
	        return ActiveForm::validate($model);
	    }
        if ($model->load(Yii::$app->request->post()))
        {
          $model->type = $_GET["perfil"];
           if($model->validate())
           {
           		$table = new Users;
                $table->email = $model->user;
                $table->email2 = $model->email;
                $table->password = crypt($model->password, Yii::$app->params["salt"]);
                $table->authKey = $this->randKey("abcdef0123456789", 200);
                $table->accessToken = $this->randKey("abcdef0123456789", 200);
                $table->role = $model->type;
                if ($table->insert())
                {
                     $user = Users::find()->where(["email2" => $model->email])->one();
                     $id = urlencode($user->id);
                     $authKey = urlencode($user->authKey);
                     $ti = \yii\web\UploadedFile::getInstance($model, 'avatar');

                     if($perfil == 3){
                     	$mc_comp = new McTcompany;
                         if($ti){
                             $rnd = $this->randKey("abcdef0123456789", 50);
                             $fileName = $rnd . str_replace(' ','_',$ti->name);
                             $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                             $mc_comp->company_avatar = '/img/system_imgs/' . $fileName;
                         }else
                            $mc_comp->company_avatar = "/img/system_imgs/pp.png";
	                     $mc_comp->company_rz = $model->rz;
	                     $mc_comp->company_nit = $model->nit;
	                     $mc_comp->company_attendant = $model->name;
	                     $mc_comp->company_address = $model->address;
	                     $mc_comp->company_phone = $model->phone;
	                     $mc_comp->company_review = $model->review;
	                     $mc_comp->company_accountnumber = $model->numberaccount;
	                     $mc_comp->company_accountname = $model->nameaccount;
	                     $mc_comp->company_accounttype = $model->typeaccount;
	                     $mc_comp->fkbank = $model->bank;
	                     $mc_comp->fklogin = $id;
	                     if(!$mc_comp->insert()){
	                        $error = "Error al guardar!!";
	                        $errors = $mc_comp->getErrors();
	                     }else{
                            //Log
                            $log = new McTadminlog;
                            $log->fklogin = Yii::$app->user->id;
                            $log->message = "Creó la compañia".$mc_comp->company_rz;
                            $log->save();
	                     	$company = McTcompany::find()->where(["fklogin" => $id])->one();
	                     	$model->rz = null;
	                     	$model->nit = null;
	                     	$model->address = null;
	                     	$model->review = null;
	                     	$model->bank = null;
	                     	$model->typeaccount = null;
	                     	$model->nameaccount = null;
	                     	$model->numberaccount = null;
                            $subPercent = split(';',$model->mcTpercent);
	                     	foreach ($model->mcTsubcategories as $index => $value) {
	                     		$sc = new McTcompanyservices;
	                     		$sc->companyservices_percent = $subPercent[$index];
	                     		$sc->fkcompany = $company->pkcompany;
	                     		$sc->fksubcategory = $value;
	                     		$sc->insert();
	                     	}
	                     }
                     }else{
	                     $mc_user = new McTuser;
                         if($ti){
                             $rnd = $this->randKey("abcdef0123456789", 50);
                             $fileName = $rnd . str_replace(' ','_',$ti->name);
                             $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                             $mc_user->user_avatar = '/img/system_imgs/' . $fileName;
                         }else
                            $mc_user->user_avatar = "/img/system_imgs/pp.png";
	                     $mc_user->user_name = $model->name;
	                     $mc_user->user_phone = $model->phone;
	                     $mc_user->fklogin = $id;
	                     if(!$mc_user->insert()){
	                        $error = "Error al guardar!!";
	                        $errors = $mc_user->getErrors();
	                     }
                         else
                         {
                            $role = $perfil== 1 ? "Administrador":"Usuario";
                            //Log
                            $log = new McTadminlog;
                            $log->fklogin = Yii::$app->user->id;
                            $log->message = "Creó el usuario ".$mc_user->user_name."con rol ".$role;
                            $log->save();
                         }
	                 }
	                 $cc = $perfil == 3 ? 3 : 2;
                     print_r(Yii::getAlias('@web').'/site/confirm' .$cc. '?id='.$id."&authKey=".$authKey);
                     /*Yii::$app->mailer->compose('verify', ['name'=>$model->name, 'link'=>'http://micargapp.com'. Yii::getAlias('@web').'/site/confirm' .$cc. '?id='.$id."&authKey=".$authKey])
                     ->setTo($user->email2)
                     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                     ->setSubject('Confirmar registro')
                     ->send();*/
                     $msg = "El usuario ha sido creado, un correo de verificación ha sido enviado a " . $table->email2;
                     $model->name = null;   
                     $model->email = null;
                     $model->phone = null;
                     $model->user = null;
                     $model->password = null;
                     $model->password_repeat = null;
                     $model->avatar = null;
                     $model->type = null;
                }
                else
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
           }else
           	  $msg = "Error: No se valido el usuario";
        }

        if($perfil != null && $perfil == '1' || $perfil == "2" || $perfil == "3" || $perfil == "4"){
	    	$this->layout = "session-admin";
            $sql = "SELECT mc_tlogin.* FROM mc_tlogin
                    INNER JOIN mc_tuser WHERE mc_tlogin.id=mc_tuser.fklogin
                    AND mc_tlogin.role= ".$perfil." ORDER BY mc_tuser.user_createat DESC";   
            $users = Users::findBySql($sql)->all();
            $cats = McTcategory::find()->all();
            $companys = "";
            $companys= McTcompany::find()->all();
            $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all(); 
	        return $this->render('perfil'.$perfil,
	        	[
	        		'option'=>$perfil,  
	        		'cats'=>$cats, 
	        		'comps'=>$companys,
	        		'error'=>$errors, 
	        		'users'=>$users, 
	        		'model'=>$model, 
	        		'msg'=>$msg, 
	        		'listBanks'=>$listBanks,
                    'mc_tsubcategory'=>$mc_tsubcategory,
	        	]
	        );
        }else
          return $this->redirect(["admin/perfiles?perfil=1"]);
    }

    /*
    *   Funcion que permite inhabilitar un usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 07/06/2016
    *   @param $id Recibe el id del usuario con el que se inhabilitará
    *   @param $perfil Recibe el número de perfil del usuario
    *   @return Regresa un render de la vista perfil
    */
    public function actionInhabilitar($id,$perfil)
    {
        $model = McTlogin::findOne($id);
        $msg = '';
        $model->activate = '0';
        $msg = "La cuenta se deshabilitó con éxito";
        $model->save();        
        Yii::$app->getSession()->setFlash('warning',$msg);
        //$this->layout = "session-admin";
        if($perfil!='3')
        {
            $user_profile = $model->getMcTusers()->one();
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Deshabilitó el usuario ".$user_profile->user_name;
            $log->save();
            $sql = "SELECT mc_tlogin.* FROM mc_tlogin
                    INNER JOIN mc_tuser WHERE mc_tlogin.id=mc_tuser.fklogin
                    AND mc_tlogin.role= ".$perfil." ORDER BY mc_tuser.user_createat DESC";   
            $users = Users::findBySql($sql)->all();
            $cats = McTcategory::find()->all();
            $companys = "";
            $companys= McTcompany::find()->all();
            $model = new FormRegister; 
            return $this->render('perfil'.$perfil,
                [
                    'option'=>$perfil,  
                    'cats'=>$cats, 
                    'comps'=>$companys,
                    'users'=>$users, 
                    'model'=>$model,
                    'msg'=>$msg, 
                ]
            );
        }
        else
        {
            $user_profile = $model->getMcTcompanies()->one();
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Deshabilitó el usuario ".$user_profile->company_rz;
            $log->save();
            $model = new FormRegisterCompany;
            $listBanks = McTbank::find()->all();
            $sql = "SELECT mc_tlogin.* FROM mc_tlogin
                    INNER JOIN mc_tuser WHERE mc_tlogin.id=mc_tuser.fklogin
                    AND mc_tlogin.role= ".$perfil." ORDER BY mc_tuser.user_createat DESC";   
            $users = Users::findBySql($sql)->all();
            $cats = McTcategory::find()->all();
            $companys = "";
            $companys= McTcompany::find()->all();
            $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all();
            return $this->render('perfil'.$perfil,
                [
                    'option'=>$perfil,  
                    'cats'=>$cats, 
                    'comps'=>$companys,
                    'users'=>$users, 
                    'model'=>$model,
                    'listBanks' => $listBanks,
                    'mc_tsubcategory'=>$mc_tsubcategory,
                    'msg'=>$msg, 
                ]
            );
        }

    }

    /*
    *   Funcion que permite habilitar un usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 04/08/2016
    *   @param $id Recibe el id del usuario con el que se habilitará
    *   @param $perfil Recibe el número de perfil del usuario
    *   @return Regresa un render de la vista perfil
    */
    public function actionHabilitar($id,$perfil)
    {
        $model = McTlogin::findOne($id);
        $msg = '';
        $model->activate = '1';
        $msg = "La cuenta se habilitó con éxito";
        $model->save();
        
        Yii::$app->getSession()->setFlash('warning',$msg);
        //$this->layout = "session-admin";
        if($perfil!='3')
        {
            $user_profile = $model->getMcTusers()->one();
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Habilitó el usuario ".$user_profile->user_name;
            $log->save();
            $sql = "SELECT mc_tlogin.* FROM mc_tlogin
                    INNER JOIN mc_tuser WHERE mc_tlogin.id=mc_tuser.fklogin
                    AND mc_tlogin.role= ".$perfil." ORDER BY mc_tuser.user_createat DESC";   
            $users = Users::findBySql($sql)->all();
            $cats = McTcategory::find()->all();
            $companys = "";
            $companys= McTcompany::find()->all();
            $model = new FormRegister; 
            return $this->render('perfil'.$perfil,
                [
                    'option'=>$perfil,  
                    'cats'=>$cats, 
                    'comps'=>$companys,
                    'users'=>$users, 
                    'model'=>$model,
                    'msg'=>$msg, 
                ]
            );
        }
        else
        {
            $user_profile = $model->getMcTcompanies()->one();
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Habilitó el usuario ".$user_profile->company_rz;
            $log->save();
            $model = new FormRegisterCompany;
            $listBanks = McTbank::find()->all();
            $sql = "SELECT mc_tlogin.* FROM mc_tlogin
                    INNER JOIN mc_tuser WHERE mc_tlogin.id=mc_tuser.fklogin
                    AND mc_tlogin.role= ".$perfil." ORDER BY mc_tuser.user_createat DESC";   
            $users = Users::findBySql($sql)->all();
            $cats = McTcategory::find()->all();
            $companys = "";
            $companys= McTcompany::find()->all();
            $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all();
            return $this->render('perfil'.$perfil,
                [
                    'option'=>$perfil,  
                    'cats'=>$cats, 
                    'comps'=>$companys,
                    'users'=>$users, 
                    'model'=>$model,
                    'listBanks' => $listBanks,
                    'mc_tsubcategory'=>$mc_tsubcategory,
                    'msg'=>$msg, 
                ]
            );
        }

    }

    /*
    *   Funcion que permite actualizar un usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 07/06/2016
    *   @param $id Recibe el id del usuario con el que se actualizara
    *   @return Regresa un render de la vista update
    */
    public function actionUpdate($id)
    {
        $msg = '';   
        $form_register = new FormChangePassword;
        $Pass = new FormChangePassword;
        $perfil = McTuser::find()->where(['fklogin'=>$id])->one();
        $model = McTlogin::findOne($id);
        if(Yii::$app->request->post())
        { 
            $form_register->name = $model->email;
            $form_register->email = $model->email2;
            $form_register->password = $model->password;
            $form_register->acc_name = $perfil->user_name;
            $form_register->phone = $perfil->user_phone;
            $form_register->avatar = $perfil->user_avatar;
            $current_avatar = $perfil->user_avatar;
            if ($form_register->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form_register);
            }
            if ($form_register->load(Yii::$app->request->post()))
            {
                if($form_register->validate())
                {
                    $model->email2 = $form_register->email;
                    $perfil->user_phone = $form_register->phone;
                    $perfil->user_name = $form_register->acc_name;
                    $ti = \yii\web\UploadedFile::getInstance($form_register, 'avatar');
                    if($ti)
                    {
                        $rnd = $this->randKey("abcdef0123456789", 50);
                        $fileName = $rnd . str_replace(' ','_',$ti->name);
                        $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                        $perfil->user_avatar = '/img/system_imgs/' . $fileName;
                     }else
                        $perfil->user_avatar = $current_avatar;
                    if($form_register->new_password)
                    {
                        $model->password = crypt($form_register->new_password, Yii::$app->params["salt"]);
                    }
                    $model->save();
                    $perfil->save();
                    $msg = "Se actualizó éxitosamente";
                    //Log
                    $log = new McTadminlog;
                    $log->fklogin = Yii::$app->user->id;
                    $log->message = "Actualizó el usuario ".$perfil->user_name;
                    $log->save();
                }
                else
                {
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                }
            }            
            $form_register->name = $model->email;
            $form_register->email = $model->email2;
            $form_register->acc_name = $perfil->user_name;
            $form_register->phone = $perfil->user_phone;
            $form_register->avatar = $perfil->user_avatar;
            return $this->render('update', [
                    'model' => $form_register,
                    'id' => $id,
                    'rol' =>$model->role,
                    'msg' => $msg,
                ]);
        }
        else
        {
            $perfil = McTuser::find()->where(['fklogin'=>$id])->one();
            $form_register->name = $model->email;
            $form_register->email = $model->email2;
            $form_register->acc_name = $perfil->user_name;
            $form_register->phone = $perfil->user_phone;
            $form_register->avatar = $perfil->user_avatar;
            return $this->render('update', [
                    'model' => $form_register,
                    'id' => $id,
                    'rol' =>$model->role,
                    'msg' => $msg,
                ]);
        }
    }

    /*
    *   Funcion que permite eliminar un usuario
    *   Creado por: Rodrigo Boet
    *   Fecha: 07/06/2016
    *   @param $id Recibe el id del usuario con el que se eliminará
    *   @param $perfil Recibe el número de perfil del usuario
    *   @return Regresa un render de la vista perfil
    */
    public function actionDelete($id, $perfil)
    {
        /*$profile = McTuser::find()->where(['fklogin'=>$id])->one();
        if($profile->user_avatar)
            unlink(Yii::getAlias('@webroot').$profile->user_avatar);
        $profile->delete();
        McTlogin::findOne($id)->delete();
        $msg = "Se borró con éxito el usuario";
        $users = Users::find()
            ->where("role=:rol", [":rol" => $perfil])
            ->all();
        $cats = McTcategory::find()->all();
        $companys = "";
        $companys= McTcompany::find()->all();
        $model = new FormRegister;
        return $this->render('perfil'.$perfil,
            [
                'option'=>$perfil,  
                'cats'=>$cats, 
                'comps'=>$companys,
                'users'=>$users, 
                'model'=>$model, 
                'msg'=>$msg, 
            ]
        );*/
    }

    /*
    *   Funcion que permite eliminar una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 08/06/2016
    *   @param $id Recibe el id del usuario con el que se eliminará
    *   @param $perfil Recibe el número de perfil de la empresa
    *   @return Regresa un render de la vista perfil
    */
    public function actionDeletecompany($id, $perfil)
    {
        /*$profile = McTcompany::find()->where(['fklogin'=>$id])->one();
        $companyid = $profile->pkcompany;
        $services = McTcompanyservices::find()->where(['fkcompany'=>$companyid])->all();
        foreach ($services as $key => $value) {
            $value->delete();
        }
        if($profile->company_avatar)
            unlink(Yii::getAlias('@webroot').$profile->company_avatar);
        $profile->delete();
        McTlogin::findOne($id)->delete();
        $msg = "Se borró con éxito la empresa";
        $model = new FormRegisterCompany;
        $listBanks = McTbank::find()->all();
        $users = Users::find()
            ->where("role=:rol", [":rol" => $perfil])
            ->all();
        $cats = McTcategory::find()->all();
        $companys = "";
        $companys= McTcompany::find()->all();
        $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all();
        return $this->render('perfil'.$perfil,
            [
                'option'=>$perfil,  
                'cats'=>$cats, 
                'comps'=>$companys,
                'users'=>$users, 
                'model'=>$model,
                'listBanks' => $listBanks,
                'msg'=>$msg, 
                'mc_tsubcategory'=>$mc_tsubcategory,
            ]
        );*/
    }

    /*
    *   Funcion que permite actualizar una empresa
    *   Creado por: Rodrigo Boet
    *   Fecha: 08/06/2016
    *   @param $id Recibe el id del usuario con el que se actualizará
    *   @param $id_login Recibe el id del perfil de la empresa
    *   @return Regresa un render de la vista updatecompany
    */
    public function actionUpdatecompany($id,$id_login)
    {
        if(User::isAdmin(Yii::$app->user->identity->id) or (Yii::$app->user->id==$id_login))
        {
            $msg = '';   
            $form_register = new FormChangePasswordCompany;
            $model = McTcompany::findOne($id);
            $user = McTlogin::findOne($id_login);
            $services = McTcompanyservices::find()->where(['fkcompany'=>$id])->asArray()->all();
            if(Yii::$app->request->post())
            { 
                $previous_avatar = $model->company_avatar;
                if ($form_register->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($form_register);
                }
                if ($form_register->load(Yii::$app->request->post()))
                {
                    if($form_register->validate())
                    {
                        $user->email2 = $form_register->email;
                        $model->company_rz = $form_register->rz;
                        $model->company_nit = $form_register->nit;
                        $model->company_attendant = $form_register->name;
                        $model->company_address = $form_register->address;
                        $model->company_phone = $form_register->phone;
                        $model->company_review = $form_register->review;
                        $model->company_accountnumber = $form_register->numberaccount;
                        $model->company_accountname = $form_register->nameaccount;
                        $model->company_accounttype = $form_register->typeaccount;
                        $model->fkbank = $form_register->bank;
                        $subPercent = split(';',$form_register->mcTpercent);
                        foreach ($form_register->mcTsubcategories as $index => $value) {
                            $c_serv = McTcompanyservices::find()->where(['fkcompany'=>$model->pkcompany,'fksubcategory'=>$value])->one();
                            if($c_serv)
                            {
                                $c_serv->companyservices_percent = $subPercent[$index];
                                $c_serv->save();
                            }
                            else
                            {
                                $sc = new McTcompanyservices;
                                $sc->companyservices_percent = $subPercent[$index];
                                $sc->fkcompany = $model->pkcompany;
                                $sc->fksubcategory = $value;
                                $sc->save();
                            }
                        }
                        $ti = \yii\web\UploadedFile::getInstance($form_register, 'avatar');
                        if($ti)
                        {
                            $rnd = $this->randKey("abcdef0123456789", 50);
                            $fileName = $rnd . str_replace(' ','_',$ti->name);
                            $ti->saveAs(Yii::getAlias('@webroot') . '/img/system_imgs/' . $fileName);
                            $model->company_avatar = '/img/system_imgs/' . $fileName;
                         }else
                            $model->company_avatar = $previous_avatar;
                        if($form_register->new_password)
                        {
                            $user->password = crypt($form_register->new_password, Yii::$app->params["salt"]);
                        }
                        $model->save();
                        $user->save();
                        $msg = "Se actualizó éxitosamente";
                        //Log
                        $log = new McTadminlog;
                        $log->fklogin = Yii::$app->user->id;
                        $log->message = "Actualizó la compañia ".$model->company_rz;
                        $log->save();
                    }
                    else
                    {
                        $msg = "Ha ocurrido un error al llevar a cabo tu actualización";
                    }
                }            
                $form_register->rz = $model->company_rz;
                $form_register->nit = $model->company_nit;
                $form_register->name = $model->company_attendant;
                $form_register->address = $model->company_address;
                $form_register->phone = $model->company_phone;
                $form_register->review = $model->company_review;
                $form_register->numberaccount = $model->company_accountnumber;
                $form_register->nameaccount = $model->company_accountname;
                $form_register->typeaccount = $model->company_accounttype;
                $form_register->avatar = $model->company_avatar;
                $form_register->bank = $model->fkbank;
                $form_register->user = $user->email;
                $form_register->email = $user->email2;
                $listBanks = McTbank::find()->all(); 
                $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all(); 
                $categories = McTcategory::find()->all();
                $services = McTcompanyservices::find()->where(['fkcompany'=>$id])->asArray()->all();     
                return $this->render('updatecompany', [
                        'model' => $form_register,
                        'id' => $id,
                        'id_login' => $id_login,
                        'msg' => $msg,
                        'listBanks' => $listBanks,
                        'mc_tsubcategory' => $mc_tsubcategory,
                        'categories' => $categories,
                        'services' => $services,
                    ]);
            }
            else
            {
                $form_register->rz = $model->company_rz;
                $form_register->nit = $model->company_nit;
                $form_register->name = $model->company_attendant;
                $form_register->address = $model->company_address;
                $form_register->phone = $model->company_phone;
                $form_register->review = $model->company_review;
                $form_register->numberaccount = $model->company_accountnumber;
                $form_register->nameaccount = $model->company_accountname;
                $form_register->typeaccount = $model->company_accounttype;
                $form_register->avatar = $model->company_avatar;
                $form_register->bank = $model->fkbank;
                $form_register->user = $user->email;
                $form_register->email = $user->email2;
                $listBanks = McTbank::find()->all(); 
                $mc_tsubcategory = McTcategorySubcategory::find()->asArray()->all(); 
                $categories = McTcategory::find()->all();     
                return $this->render('updatecompany', [
                        'model' => $form_register,
                        'id' => $id,
                        'id_login' => $id_login,
                        'msg' => $msg,
                        'listBanks' => $listBanks,
                        'mc_tsubcategory' => $mc_tsubcategory,
                        'categories' => $categories,
                        'services' => $services,
                    ]);
            }
        }
        else
        {
            throw new  ForbiddenHttpException;
        }
    }

    /*
    *   Funcion que permite pagar un contrato
    *   Creado por: Rodrigo Boet
    *   Fecha: 27/06/2016
    *   @param $id Recibe el id del contrato
    *   @return Regresa un render de la vista cargas
    */
    public function actionPaycontract($id)
    {
        $contract = McTcontractpayment::findOne($id);
        $contract->fkstatus = 5;
        $contract->paymentdate = date('Y-m-d');
        $contract->save();
        $msg = 'Se pagó el contrato con éxito';
        //Datos necesarios para la notificación
        $contrato = $contract->getFkcontract0()->one();
        $zeros = '000000';
        $contract_type = ''.$contrato->pk_contract;
        $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
        //Log
        $log = new McTadminlog;
        $log->fklogin = Yii::$app->user->id;
        $log->message = "Marcó como pagado el contrato UE".$contract_number;
        $log->save();
        //Notificación
        $model_notificacion = new McTnotificaciones();
        $model_notificacion->fk_login=$contrato->fkuser_company;
        $model_notificacion->msj_notificacion="<b>El Administrador</b> te realizó el pago del contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/accountc/miscargas' onclick='modalClick(event)'> Ver Contrato</a>";
        $model_notificacion->save();
        $o1 = 1;
        $o2 = 1;
        $o3 = null;
        $contract = McTcontract::find()->all();
        return $this->render('cargas',['contratos'=>$contract,'option1'=>$o1,'option2'=>$o2,'option3'=>$o3,
            'msg'=>$msg]);
    }

    /*
    *   Funcion que permite devolver el pago de un contrato
    *   Creado por: Rodrigo Boet
    *   Fecha: 27/06/2016
    *   @param $id Recibe el id del contrato
    *   @return Regresa un render de la vista cargasg
    */
    public function actionRefundcontract($id)
    {
        $model = new FormDevolucion;
        $model->load(Yii::$app->request->post());
        $contract = McTcontractpayment::findOne($id);
        $contract->fkstatus = 7;
        $contract->paymentdesc = $model->motivo;
        $contract->save();
        $msg = 'Se realizó la devolución con éxito';
        //Datos necesarios para la notificación
        $contrato = $contract->getFkcontract0()->one();
        $zeros = '000000';
        $contract_type = ''.$contrato->pk_contract;
        $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
        //Log
        $log = new McTadminlog;
        $log->fklogin = Yii::$app->user->id;
        $log->message = "Devolvió el contrato UE".$contract_number;
        $log->save();
        //Notificación
        $admins = McTlogin::find()->where(['role'=>1,'activate'=>1])->all();
        foreach ($admins as $admin) {
            $model_notificacion = new McTnotificaciones();
            $model_notificacion->fk_login=$admin->id;
            $model_notificacion->msj_notificacion="Se devolvió el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/admin/cargasg' onclick='modalClick(event)'> Ver Contrato</a>";
            $model_notificacion->activa=1;
            $model_notificacion->save();
        }
        $o1 = 1;
        $o2 = 1;
        $o3 = null;
        $contract = McTcontract::find()->all();
        return $this->redirect(["admin/cargasg"]);
    }

    /*
    *   Funcion que permite reembolsar el pago de un contrato
    *   Creado por: Rodrigo Boet
    *   Fecha: 03/07/2016
    *   @param $id Recibe el id del contrato
    *   @return Regresa un render de la vista cargasg
    */
    public function actionRepaycontract($id)
    {
        $model = new FormDevolucion;
        $model->load(Yii::$app->request->post());
        $contract = McTcontractpayment::findOne($id);
        $contract->fkstatus = 8;
        $contract->paymentdesc = $model->motivo;
        $contract->save();
        $msg = 'Se realizó el reembolso con éxito';
        //Datos necesarios para la notificación
        $contrato = $contract->getFkcontract0()->one();
        $zeros = '000000';
        $contract_type = ''.$contrato->pk_contract;
        $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
        //Log
        $log = new McTadminlog;
        $log->fklogin = Yii::$app->user->id;
        $log->message = "Reembolsó el contrato UE".$contract_number;
        $log->save();
        //Notificación
        $admins = McTlogin::find()->where(['role'=>1,'activate'=>1])->all();
        foreach ($admins as $admin) {
            $model_notificacion = new McTnotificaciones();
            $model_notificacion->fk_login=$admin->id;
            $model_notificacion->msj_notificacion="Se reembolsó el contrato UE".$contract_number." <a href='".Yii::getAlias('@web')."/admin/cargasg' onclick='modalClick(event)'> Ver Contrato</a>";
            $model_notificacion->activa=1;
            $model_notificacion->save();
        }
        $o1 = 1;
        $o2 = 1;
        $o3 = null;
        $contract = McTcontract::find()->all();
        return $this->redirect(["admin/cargasg"]);
    }

    /*
    *   Funcion que permite enviar el pago de refencia a un transportador
    *   Creado por: Rodrigo Boet
    *   Fecha: 29/06/2016
    *   @param $id Recibe el id del contrato
    *   @return Regresa un render de la vista cargast
    */
    public function actionTransporterpayment($id)
    {    
        $msg = '';
        if(McTtransporterpayment::find()->where(['fkcontractemp'=>$id])->one())
        {
            $msg = 'Ya se envío la refencia y pago a este transportador';
        }
        else
        {
            $model = new McTtransporterpayment;
            $form = new FormAdminTransporterPayment;
            $form->load(Yii::$app->request->post());
            $model->reference = $form->reference;
            $model->value = $form->value;
            $model->fkstatus = 9;
            $model->fkcontractemp = $id;
            $model->save();
            $msg = 'Se envío la refencia y el pago correctamente';
            //Datos necesarios para el log
            $contract = $model->getFkcontractemp0()->one();
            $zeros = '000000';
            $contract_type = ''.$contract->pk_contract;
            $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
            //Log
            $log = new McTadminlog;
            $log->fklogin = Yii::$app->user->id;
            $log->message = "Envío la refencia de pago ET".$contract_number;
            $log->save();
        }
        $contratos = McTcontractemp::find()->all();
        return $this->render('cargast',['contratos'=>$contratos,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite confirmar que un transportar realizó el pago
    *   Creado por: Rodrigo Boet
    *   Fecha: 30/06/2016
    *   @param $id Recibe el id del pago
    *   @return Regresa un render de la vista cargast
    */
    public function actionTransportermakepaid($id)
    {    
        $msg = '';
        $model = McTtransporterpayment::findOne($id);
        $model->fkstatus = 10;
        $model->save();
        $msg = 'Se marcó como pagado correctamente';
        //Datos necesarios para el log
        $contract = $model->getFkcontractemp0()->one();
        $zeros = '000000';
        $contract_type = ''.$contract->pk_contract;
        $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
        //Log
        $log = new McTadminlog;
        $log->fklogin = Yii::$app->user->id;
        $log->message = "Marcó que le transportador pagó en ET".$contract_number;
        $log->save();
        $contratos = McTcontractemp::find()->all();
        return $this->render('cargast',['contratos'=>$contratos,'msg'=>$msg]);
    }

    /*
    *   Funcion que permite ver el historial de un admin
    *   Creado por: Rodrigo Boet
    *   Fecha: 11/08/2016
    *   @param $id Recibe del admin
    *   @return Regresa un render de la vista historial
    */
    public function actionHistorylog($id)
    {    
        $logs = McTadminlog::find()->where(['fklogin'=>$id])->limit(20)->orderBy('date DESC')->all();
        $user = McTuser::find()->where(['fklogin'=>$id])->one();
        return $this->render('logs',['logs'=>$logs,'user'=>$user,'rol'=>1]);
    }

}

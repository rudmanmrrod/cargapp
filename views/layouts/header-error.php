<!-- Header -->

<header>
  <div class="container">
    <div class="row header-wrapper">
      <div class="col-md-3 col-xs-5 logo"> <img class="img-responsive col-md-12" src="<?php echo Yii::getAlias('@web') ?>/img/logo.png" alt="logo"> </div>
      
      <!-- menu -->
      
      <div class="col-md-9 col-smf menu">
        <nav class="navbar navbar-default"> 
          
          <!-- Brand and toggle get grouped for better mobile display -->
          
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
                    
        </nav>
      </div>
      <!-- /.menu --> 
      
    </div>
  </div>
</header>

<!-- End Header -->
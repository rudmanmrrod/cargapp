<?php

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Mi Cargapp:. - Zona de administrador</title>

<!-- Bootstrap -->
<link href="<?php echo Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::getAlias('@web') ?>/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Maven+Pro" />
<link rel="shortcut icon" href="<?php echo Yii::getAlias('@web') ?>/img/favicons.png">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/jquery-ui.css">
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery-ui.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/functions.js"></script>

</head>
<body>
<?php $this->beginBody() ?>
<?php include 'header-admin.php'; ?>
<div class="content">
  <div class="container">
	<?php echo $content; ?>
  </div>
</div>
<?php $this->registerJs("var url = ".json_encode(Yii::getAlias('@web')).";", \yii\web\View::POS_END, 'urls'); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
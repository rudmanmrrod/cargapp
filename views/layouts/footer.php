
<footer>
<div class="container">
  <div class="row">
      <div class="col-sm-2">
          <div class="footer-contact text-center">
              <img src="<?php echo Yii::getAlias('@web') ?>/img/logo-vertical.png" class="img-responsive footer-logo" alt=""/>
                <p class="copyright">All Right Reserved <br>Copyright &copy; 2015.</p>
                <p><img src="<?php echo Yii::getAlias('@web') ?>/img/designby.png" class="img-responsive designby" alt=""/></p>
            </div>
        </div>
        <div class="col-sm-3">
          <h4 class="widget-title">MI CARGAPP</h4>
            <ul class="widget-menu">
              <li><a href="">Inicio</a></li>
        <li><a href="">Perfiles de usuario</a></li>
        <li><a href="">Inversionistas</a></li>
        <li><a href="">Quienes Somos</a></li>
            </ul>
        </div>
        <div class="col-sm-3">
        <h4 class="widget-title">LEGAL</h4>
            <ul class="widget-menu">
              <li><a href="">T&eacute;rminos de Servicio</a></li>
        <li><a href="">Pol&iacute;ticas de Privacidad</a></li>
            </ul>
        </div>
        <div class="col-sm-4">
        <h4 class="widget-title">SOPORTE</h4>
            <ul class="widget-menu">
              <li><a href="">Cont&aacute;ctenos</a></li>
        <li><a href="">Preguntas Frecuentes</a></li>
            </ul>
            <div class="footer-social">
              <a href="#"><img src="<?php echo Yii::getAlias('@web') ?>/img/facebook.png" alt="facebook"></a>
                <a href="#"><img src="<?php echo Yii::getAlias('@web') ?>/img/linked.png" alt="linkedin"></a>
                <a href="#"><img src="<?php echo Yii::getAlias('@web') ?>/img/skype.png" alt="skype"></a>
                <a href="#"><img src="<?php echo Yii::getAlias('@web') ?>/img/youtube.png" alt="youtube"></a>
            </div>
        </div>
    </div>
</div>
</footer>
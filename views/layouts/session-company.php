<?php

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" ng-app="MyApp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>.:Mi Cargapp:. - Zona de empresas</title>

<!-- Bootstrap -->
<link href="<?php echo Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::getAlias('@web') ?>/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Maven+Pro" />
<link rel="shortcut icon" href="<?php echo Yii::getAlias('@web') ?>/img/favicons.png">
<link href="<?php echo Yii::getAlias('@web') ?>/css/sweetalert.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/leaflet.css">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/leaflet.awesome-markers.css">
<script src="<?php echo Yii::getAlias('@web') ?>/js/leaflet.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/leaflet.awesome-markers.min.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/sweetalert.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery-ui.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/functions.js"></script>
<!-- Angular -->
<script src="<?php echo Yii::getAlias('@web'); ?>/js/angular/angular.js" ></script>
<script src="<?php echo Yii::getAlias('@web'); ?>/js/angular/modules.js" ></script>
<script src="<?php echo Yii::getAlias('@web'); ?>/js/angular/controllers/notificaciones.js"></script>
<script src="<?php echo Yii::getAlias('@web'); ?>/js/angular/controllers/chat.js"></script>
<script src="<?php echo Yii::getAlias('@web'); ?>/js/angular/controllers/chattransporter.js"></script>
<script src="<?php echo Yii::getAlias('@web'); ?>/js/not-menu.js"></script>


</head>
<body>
<?php $this->beginBody() ?>
<?php include 'header-company.php'; ?>
<div class="content">
  <div class="container">
	<?php echo $content; ?>
  </div>
</div>
<script src="<?php echo Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
<?php $this->registerJs("var url = ".json_encode(Yii::getAlias('@web')).";", \yii\web\View::POS_END, 'urls'); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
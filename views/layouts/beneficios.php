<div class="page-header-2">
  <h1>
        <strong>
            Beneficios de <span>Mi Cargapp</span>
            <small class="small-block"><strong>¿Porqué somos mejor que cualquier cosa que hayas visto?</strong></small>
        </strong>
    </h1>
</div>
    <div class="bottom-boxs clearfix">
      <div class="col-sm-6 col-md-3">
          <div class="box">
              <div class="icon">
                  <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon3.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                  <p>Pueden elegir las mejores tarifas para todo tipo de transporte</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="box color2">
              <div class="icon">
                  <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon2.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                  <p>Pago de un anticipo mínimo y el saldo del transporte contraentrega</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="box color3">
              <div class="icon">
                  <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon1.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                  <p>Si la empresa que eligió incumple puede cancelar el servicio y solocitar otra</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="box color4">
              <div class="icon">
                  <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon1.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                  <p>Si no se siente satisfecho y ninguna empresa le cumplio, devolvemos el dinero</p>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="clearfix"></div>
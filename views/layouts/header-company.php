<!-- Header -->

<header>
  <div class="container">
    <div class="row header-wrapper">
      <div class="col-md-3 logo"> <img class="img-responsive col-md-12" src="<?php echo Yii::getAlias('@web') ?>/img/logo.png" alt="logo"> </div>
      
      <!-- menu -->
      
      <div class="col-md-9 col-smf menu">
        <nav class="navbar navbar-default"> 
          
          <!-- Brand and toggle get grouped for better mobile display -->
          
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-menu">
            <ul class="nav navbar-nav navbar-right">
              <li class="<?php echo Yii::$app->session->get('option') == 1? 'active' : ''; ?>">
                <a href="<?php echo Yii::getAlias('@web') ?>/accountc">CONDUCTORES <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="<?php echo Yii::$app->session->get('option') == 2? 'active' : ''; ?>"><a href="<?php echo Yii::getAlias('@web') ?>/accountc/cotizar">COTIZAR</a></li>
              <li class="<?php echo Yii::$app->session->get('option') == 3? 'active' : ''; ?>"><a href="<?php echo Yii::getAlias('@web') ?>/accountc/miscargas">MIS CARGAS</a></li>
              <li class="<?php echo Yii::$app->session->get('option') == 4? 'active' : ''; ?>"><a href=<?php echo Yii::$app->urlManager->createUrl(['accountc/update/'.Yii::$app->session->get('company').'?id_login='.Yii::$app->user->id]); ?> >PERFIL</a></li>
              <li><a href="<?php echo Yii::getAlias('@web') ?>/site/logout">SALIR</a></li>
            </ul>
            <div class="col-lg-1 notification-icon navbar-right" ng-controller="GetNot"> 
              <a href="#">
                <li class="glyphicon glyphicon-bell"></li>
              </a>
              <div id="before-not" style="display:none;"> 
                <div id="arrow-not" style="width:200px;height:25px"></div> 
                <div id="inside-not"></div>
              </div> 
            </div>          
          </div>
          <!-- /.navbar-collapse --> 
          
        </nav>
      </div>
      <!-- /.menu --> 
      
    </div>
  </div>
</header>
<?php $this->registerJs("var ak = ".json_encode(Yii::$app->session->get('ak')).";", \yii\web\View::POS_END, 'aks'); ?>
<!-- End Header -->
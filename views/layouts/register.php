<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Registro - Mi Cargapp</title>

<!-- Bootstrap -->
<link href="<?php echo Yii::getAlias('@web'); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::getAlias('@web'); ?>/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Maven+Pro" />
<link rel="shortcut icon" href="<?php echo Yii::getAlias('@web'); ?>/img/favicons.png">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>

<?php $option = "3"; ?>
<?php include 'header-nosession.php'; ?>

<?php echo $content; ?>;

<?php include 'footer.php'; ?>
<!-- jQuery --> 
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
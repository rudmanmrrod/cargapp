<?php 
  use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <div class="main">

        <div class="row ">
          <div class="col-lg-12">
            <?= Html::a('CLIENTES', ['miscargas'], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
            <?= Html::a('TRANSPORTADOR', ['miscargas','perfil'=>2], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div>

        <div class="blocks">
          <?php if(count($contract)>1){
            foreach ($contract as $c) {
              
            ?>
          <div class="col-lg-12 qoute-main-block">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-3">
                <div class="type col-log-6">
                  <p><b>Lugar de Origen:</b></p>
                  <p><b>Lugar de Destino:</b></p>
                  <p><b>Tipo de Vehículo:</b></p>
                  <p><b>Peso:</b></p>
                  <p><b># de Vehículos:</b></p>
                  <p><b>Valor a Pagar:</b></p>
                  <p><b>Comentarios:</b></p>
                </div>
                <div class="col-log-6">
                <p><?php echo $c->origen; ?></p>
                <p><?php echo $c->destino; ?></p>
                <p><?php echo $c->fkVehicletype->vehicletype_name; ?></p>
                <p><?php echo $c->weight < 1000 ? $c->weight . " Kl" : ($c->weight / 1000) . " Ton" ?></p>
                <p><?php echo $c->vehicle_number; ?></p>
                <p class="blue"><?php echo number_format($c->payment).' COP'; ?></p>
                <p><?php echo $c->comment; ?></p>
                </div>
              </div>
              <div class="col-md-3">
                <br>
                <?php
                if($c->fkstatus!=3) 
                {
                  echo Html::a('ENTRAR', ['seecotization','id_cot'=>$c->pkcotizaremp], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-7 col-lg-offset-3 col-xs-12']); 
                  echo '<button type="button" class="btn btn-danger elimn col-lg-7 col-lg-offset-3 col-xs-12">CANCELAR</button>';
                }
                else
                {
                  echo '<button type="button" class="btn btn-danger elimn col-lg-7 col-lg-offset-3 col-xs-12">FINALIZADA</button>';
                }
                ?>
                
              </div>
              <div class="col-md-3"></div>
            </div>
          </div>
          <?php 
            }
          }
          else {?>
            <div class="row" style="background-color: #fff;"> 
              <div class="page-header">
                <img class="ant" src="<?php echo Yii::getAlias('@web'); ?>/img/ant.png">
                <h1>
                    <small class="small-block mt"><strong>No has realizado ninguna cotizaci&oacute;n</strong></small>
                </h1>
                <h4>
                  <strong>Haz clic <a class="mred" href="<?php echo Yii::getAlias('@web'); ?>/accountc/cotizar?type=1">aqui</a> para hacer una cotizaci&oacute;n</strong>
                </h4>
                <br><br><br>
              </div> 
            </div>
          <?php }?>
        </div>

      </div>
    </div>
  </div>
</div>

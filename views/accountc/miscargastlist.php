<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->

        <div class="row ">
          <div class="col-lg-12">
            <?= Html::a('CLIENTES', ['miscargas'], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
            <?= Html::a('TRANSPORTADOR', ['miscargas','perfil'=>2], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div><br>

        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div><br>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="body-main col-lg-12 white">
        <div class="col-lg-6 col-sm-6">
          <?= Html::a('Mi Carga', ['seecotization','id_cot'=>$id_cot], ['class' => 'btn btn-desactive btn-sub2 col-md-5 col-xs-5 top-button']) ?>
          <?= Html::a('Solicitudes a Conductores', ['listtproposal','id_cot'=>$id_cot], ['class' => 'btn btn-active btn-sub2 col-md-5 col-xs-5 top-button']) ?>
        </div>
      </div>
      <?php 
      if($loadproposal){
      foreach ($loadproposal as $load) {
        $user = $load->getFktransportador0()->one();
        $profile = $user->getMcTusers()->one();
      ?>
      <div class="row administration-side administration-side-activity">
        <div class="col-sm-7 col-xs-12 administration-activity-info">
          <div class="row">
            <div class="col-xs-3">
              <img src="<?php if($profile->user_avatar){echo Yii::getAlias('@web'). $profile->user_avatar;}
              else {echo Yii::getAlias('@web').'/img/pp.png';} ?>">
            </div>
            <div class="col-xs-9">
              <ul class="list-unstyled">
                <li><b><?php echo $profile->user_name; ?></b></li>
                <li class="red"><b><?php echo strtoupper($profile->getMcTvehicles()->one()->vehicle_licenceplate); ?></b></li>
                <li>Valor a pagar: <span class="green"><?php echo number_format($load->payment).' COP' ?></span></li>
                <li class="blue"><?php echo $profile->user_phone; ?></li>
                <li class="blue"><?php echo $user->email; ?></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-5 col-xs-12 administration-activity-buttons top-button">
          <div class="row">
            <div class="col-md-4">
                <?php 
                  if($load->status==0)
                  {
                    echo Html::a('ACEPTAR', ['acepttproposal', 'id_cot' => $id_cot , 'id_trans' => $user->id ],
                      ['class' => 'btn btn-desactive btn-green btn-sub2 col-xs-12']);
                  }
                  else if($load->status==2)
                  {
                    echo '<button class="btn btn-desactive col-xs-12">ESPERANDO</button>';
                  }
                  else
                  {
                    echo '<button class="btn btn-desactive col-xs-12">ACEPTADO</button>';
                  }
                ?>
            </div>
            <div class="col-md-4">
              <?= Html::button('VER PERFIL', ['class' => 'btn btn-active col-xs-12 seeprofile', 'name' => $user->id ]) ?>
            </div>
            <div class="col-md-4">
              <?= Html::a('ELIMINAR', ['deletetproposal', 'id_cot' => $id_cot , 'id_trans' => $user->id ], ['class' => 'btn btn-desactive btn-sub2 btn-red col-xs-12',
                  'onclick'=>'SeeModal(event,deletesolicitude,'. $id_cot.',this,"a","href")',
              ]) ?>
            </div>
          </div>
        </div>
      </div>
      <?php 
        }
      }
      else {?>
        <div class="row" style="background-color: #fff;"> 
          <div class="page-header">
            <img class="ant" src="<?php echo Yii::getAlias('@web'); ?>/img/ant.png">
            <h1>
                <small class="small-block mt"><strong>No tienes ninguna solicitud para esta cotización</strong></small>
            </h1>
            <br><br><br>
          </div> 
        </div>
      <?php }?>
    </div>
  </div>        
</div>
<?php 
  $pub = Yii::$app->assetManager->publish('@app/web/js/modal-profile.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 

Modal::begin([
  'id' => 'profile',
  'header' => '<h2>Perfil del Transportador</h2>',
  'size'=> 'modal-lg',
  ]); 
  echo '<div id="modalContent"></div>';
Modal::end();

Modal::begin([
  'id' => 'deletesolicitude',
  'header' => '<h3>¿Deseas eliminar esta solicitud?</h3>',
  'size'=> 'modal-md',
  ]); 
  echo '<div class="row"><div class="col-md-12 text-center">Ésta acción no se puede deshacer</div></div><br>';
  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['deletetproposal', 'id_cot' => $id_cot , 'id_trans' => $user->id ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>
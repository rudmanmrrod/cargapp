<?php 
  use app\models\McTcategory;
  use app\models\McTcompanyservices;
  use app\models\McTproposal;
  use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <div class="main">

        <div class="row ">
          <div class="col-lg-12">
          <?= Html::a('CLIENTES', ['miscargas'], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
          <?= Html::a('TRANSPORTADOR', ['miscargas','perfil'=>2], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div>

        <div class="body-main col-lg-12">
          <div class="col-lg-6 col-sm-6">
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Activo">ACTIVOS</button>
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Finalizado">FINALIZADOS</button>
          </div>
          <div class="col-lg-2 col-sm-2"></div>
          <div class="col-lg-4 col-xs-4">
            <div class="form-group col-md-8 col-xs-7 top-button">
              <input class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
            </div>
            <button class="btn btn-primary col-md-4 col-xs-5 submit top-button">BUSCAR</button>
          </div>
        </div><br><br>

        <div class="blocks">
          <?php 
            if(isset($msg)) 
              echo "<center><h3 class='confirm'>$msg</h3></center><br>"; 
          ?>
          <?php foreach ($contract as $c) {?>
            <?php
              $load = $c->getFkCarga()->One(); 
              $user = $load->getFkuser0()->One(); 
              $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); ?>
            <?php 
              $comi = McTcompanyservices::find()
                    ->where("fkcompany=:id", [":id" => Yii::$app->session->get("userid")])
                    ->andWhere('fksubcategory = :sub', [':sub' => $load->getFksubcategory0()->One()->pktcategory_subcategory])
                    ->one();
              $pexist = McTproposal::find()
                    ->where("fkcompany=:id", [":id" => Yii::$app->session->get("userid")])
                    ->andWhere('fkload = :load', [':load' => $load->pkload])
                    ->one();
              if($comi){
            ?>
            <div class="col-lg-12 qoute-main-block block">
                <div class="row administration-side administration-side-activity qoute-inner-block margin-4">
              <div class="col-sm-6 col-xs-12 administration-activity-info ">
                <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>">
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
                       <p class="large-title"><?php echo $user->user_name; ?></p>
                      <div class="type col-log-6">
                       <p><span class="type-text">Categoria:</span>  <span class="green"><?php echo $catg->category_name;?></span></p>
                       <p><span class="type-text">Sub-Catg:</span> <span class="green"><?php echo $load->getFksubcategory0()->One()->fksubcategory0->subcategory_name; ?></span></p>
                       <p><span class="type-text">Asegura:</span>  <span class="red">$ <?php echo number_format($load->load_ensure); ?></span></p>
                      </div>                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-sm-offset-0 col-xs-offset-2 quote-box">
                  <div class="contract-box ">
              <form>
                <div class="type col-sm-7">
                <p><span class="width-box">Fecha orden:</span>    <span id="fecha" style="font-weight:bolder" class="blue"><?php echo date("d-m-Y",  strtotime($load->load_create));?></span></p>
                <p>
                  <span class="width-box">Valor a Pagar:</span>  
                  <span>
                    <?php 
                        echo '$ ' . number_format($pexist->proposal_total) . ' COP';
                     ?>
                  </span>
                </p>
                <p>
                  <span class="width-box">Pago Minimo:</span> 
                  <span>
                    <?php 
                        echo number_format($pexist->proposal_paymin) . ' COP';
                      ?>
                  </span>
                </p>
                <p><span class="width-box">% Comisión:</span>  <span class="blue bold-large"> <?php echo $comi->companyservices_percent; ?>%</span> </p>
              </div>
              <div class="type col-sm-4">
                <?php 
                  $zeros = '000000';
                  $contract_type = ''.$c->pk_contract;
                  $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                ?>
                <div class="row">
                  <b><span class="width-box">Contrato: </span> <span class="blue contract-name"><?php echo 'UE'.$contract_number; ?></span></b>
                  <p class="status" style="display:none;"><?php echo $c->fkStatus->status_name; ?></p>
                </div><br>
                <div class="col-lg-8 col-sm-8">
                  <?php 
                  if($load->fkstatus==14)
                  {
                     echo '<a type="button" class="btn btn-desactive btn-sub2 btn-red">CANCELADO</a>';
                  }
                  if($load->fkstatus==3)
                  {
                     echo '<a type="button" class="btn btn-desactive btn-sub2 btn-red">FINALIZADA</a>';
                  }
                  else{
                    echo Html::a('ENTRAR', ['usertcontract','id_contract'=>$c->pk_contract], ['class' => 'btn btn-desactive btn-sub2 btn-green']); 
                  }?>
                </div>
              </div>
               </form>
              </div>
              </div>
            </div>
            <div class="row administration-side administration-side-activity qoute-inner-block">
              <div class="col-sm-10 col-xs-12 administration-activity-info small-padding-around">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-5">
                   <div class="description-block">
                    <p><span class="width-box"><strong>Desde:</strong></span><span><?php echo $load->load_cityorigin; ?></span></p>
                    <p><span class="width-box"><strong>Hasta:</strong></span><span><?php echo $load->load_citydestination; ?></span></p>
                     <p><span class="width-box"><strong>Tipo:</strong></span><span>No definido</span></p>
                      <p><span class="width-box"><strong>Peso:</strong></span><span><?php echo $load->load_weight < 1000 ? $load->load_weight . " Kl" : ($load->load_weight / 1000) . " Ton" ?></span></p>
                   </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-5">
                      <div class="Dimensiones-block">
                    <p><span class="width-box"><strong>Dimensiones:</strong></span><span><?php echo $load->load_dimensions; ?></span></p>
                    <p><span class="width-box"><strong>Observaci&oacute;n:</strong></span><span class="observation-text"><?php echo $load->load_comment; ?></span></p>
                   </div>                
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/buttonsearch.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>

<script>
 var my_data = [];
  $('.contract-name').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    event.preventDefault();
    $('.qoute-main-block').parent().find('.not-found').remove();
    $('.qoute-main-block').each(function(index,value){
      $(this).show();
    });
    var text = $('#inputSearch').val();
    if(text)
    {
      $('.qoute-main-block').each(function(index,value){
        var bool = false;
        ($(value).find('.contract-name').text()===text) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.qoute-main-block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=true;
    $('.qoute-main-block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool && text)
    {
      var txt ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      txt+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      txt+='</h1></div></div>';
      $('.qoute-main-block').parent().append(txt);
    }
  });
  
</script>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\models\McTvehicle;
use app\models\McTloadstatus;

$licensesplates = array();
foreach ($transporter as $t) {
  $vehicle = McTvehicle::find()->where(['fkuser'=>$t->fkuserRecive->mcTusers[0]->pkuser])->one();
  array_push($licensesplates, $vehicle->vehicle_licenceplate);
}
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->

        <div class="row ">
          <div class="col-lg-12">
            <?= Html::a('CLIENTES', ['miscargas'], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
            <?= Html::a('TRANSPORTADOR', ['miscargas','perfil'=>2], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div><br>

        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div><br>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row">
      <!-- left side -->
      <div class="body-main col-lg-12 white">
        <div class="col-lg-6 col-sm-6">
          <?= Html::a('Mi Carga', ['seecotization','id_cot'=>$id_cot], ['class' => 'btn btn-active btn-sub2 col-md-5 col-xs-5 top-button']) ?>
          <?= Html::a('Solicitudes a Conductores', ['listtproposal','id_cot'=>$id_cot], ['class' => 'btn btn-desactive btn-sub2 col-md-5 col-xs-5 top-button']) ?>
        </div>
      </div>
        <div class="col-sm-4 administration-left-side-wrapper">
          <div class="col-xs-12 administration-side administration-left-side new-user-za">

            <h1>Añadir Conductor</h1>
            
            <div class="form-inline">
              <input class="administration-input srchInput" id="inputSearch" placeholder="Placa del Vehículo">
              <a title="Buscar" onClick='loadFunction()'><i class="glyphicon glyphicon-search"></i></a>
              <div id="msg"></div>
            </div>
            <div id="driver"></div>
          </div>

          <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id'=>'submit',
            'action' => ['sendtproposal', 'id_cot' => $id_cot],
            'enableClientValidation' => true,
          ]);
          ?>

          <div class="col-xs-12 administration-submit">
            <?= $form->field($model, "plate")->hiddenInput(['placeholder' => 'Placa', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "payment")->input("number", ['placeholder' => 'Valor a Pagar', 'class' => 'administration-input', 'style'=>'display:none;','value'=>1])->label(false) ?>   
            <?= Html::submitButton('AÑADIR A CARGA', ['class' => 'btn btn-active col-xs-12']) ?>
          </div>
        <?php $form->end(); ?>

        </div>
      <!-- END left side -->

      <!-- right side -->
      <div class="col-sm-8 administration-right-side">
        <div class="row administration-side administration-right-side-title-wrapper">
          <h1>Conductores</h1>
        </div>
          <?php if($loadproposal){
            foreach ($loadproposal as $load) {
              $user = $load->getFktransportador0()->one();
              $profile = $user->getMcTusers()->one();
              $cotizacion = $load->getFkcotizacionemp0()->one();
              $contrato = $load->fkcotizacionemp0->getMcTcontractemps()->where(['fkuser'=>$user->id])->one();
              $loadstatus = McTloadstatus::find()->where(['fkcontractemp'=> $contrato->pk_contract])->one();
            ?>
          <div class="row administration-side administration-side-activity">
            <div class="row administration-activity-info">
              <div class="row center-block">
                <div class="col-xs-3">
                  <img src="<?php if($profile->user_avatar){echo Yii::getAlias('@web'). $profile->user_avatar;}
                  else {echo Yii::getAlias('@web').'/img/pp.png';} ?>">
                </div>
                <div class="col-xs-5">
                  <ul class="list-unstyled">
                    <li><b><?php echo $profile->user_name; ?></b></li>
                    <li class="red"><b><?php echo strtoupper($profile->getMcTvehicles()->one()->vehicle_licenceplate); ?></b></li>
                    <li>Valor a pagar: <span class="green"><?php echo number_format($load->payment).' COP' ?></span></li>
                    <li class="blue"><?php echo $profile->user_phone; ?></li>
                    <li class="blue"><?php echo $user->email; ?></li>
                  </ul>
                </div>
                <div class="col-xs-4">
                  <ul class="list-unstyled">
                    <li><b>Estado: </b><?php echo $loadstatus->fkstatus0->status_name; ?></li>
                    <li class="green"><?php echo $loadstatus->codigo ? $loadstatus->codigo:'# CÓDIGO';?></li>
                    <li class="blue"><?php echo $loadstatus->sello1 ? '<a class="my-img" href='.Yii::getAlias('@web').$loadstatus->sello1.'>Foto Sello 1</a>':'Foto Sello 1';?></li>
                    <li class="blue"><?php echo $loadstatus->sello2 ? '<a class="my-img" href='.Yii::getAlias('@web').$loadstatus->sello2.'>Foto Sello 2</a>':'Foto Sello 2'; ?></li>
                  </ul>
                </div>
                  <input type="text" id="latitude_origin" value='<?php echo $cotizacion->origin_latitude; ?>' style="display:none;">
                  <input type="text" id="logitude_origin" value='<?php echo $cotizacion->origin_longitude; ?>' style="display:none;">
                  <input type="text" id="latitude_destination" value='<?php echo $cotizacion->destination_latitude; ?>' style="display:none;">
                  <input type="text" id="longitude_destination" value='<?php echo $cotizacion->destination_longitude; ?>' style="display:none;">
                  <input type="text" id="latitude_trans" value='<?php echo $loadstatus->latitude; ?>' style="display:none;">
                  <input type="text" id="longitude_trans" value='<?php echo $loadstatus->longitude; ?>' style="display:none;">
              </div>
            </div>
            <div class="row administration-activity-info">
              <div class="col-md-3">
                <?= Html::button('VER PERFIL', ['class' => 'btn btn-active btn-green col-xs-12 seeprofile', 'name' => $user->id ]) ?>
              </div>
              <div class="col-md-3">
                <?= Html::a('CHAT', ['transporterchat', 'id_contract'=> $cotizacion->pkcotizaremp,'id_transcontract' => $contrato->pk_contract, 'action' => 'seecotization']
                , ['class' => 'btn btn-desactive btn-blue btn-sub2 col-xs-11']) ?>
              </div>
              <div class="col-md-3">
                <button type="button" class="btn btn-active col-xs-11" id="mymap">VER MAPA</button>
              </div>
              <div class="col-md-3">
                <?php if ($loadstatus->fkstatus>10 and $loadstatus->fkstatus<13)
                {
                  echo '<button type="button" class="btn btn-desactive col-xs-11">ENTREGADO</button>';
                }
                else if($loadstatus->fkstatus==13)
                {
                  echo Html::a('ENTREGADO', ['endtproposal', 'id_cot' => $id_cot, 'id_load' => $loadstatus->pkloadstatus ], ['class' => 'btn btn-desactive btn-red btn-sub2 col-xs-11',
                    'onclick'=>'SeeModal(event,endproposal,'. $id_cot.',this,"a","href")',]);
                }
                else if($loadstatus->fkstatus==3)
                {
                  echo '<button type="button" class="btn btn-desactive col-xs-11">OK</button>';
                }
                ?>
              </div>
            </div>
          </div>
          <?php
            }
          }
          else {?>
            <div class="row" style="background-color: #fff;"> 
              <div class="page-header">
                <img class="ant" src="<?php echo Yii::getAlias('@web'); ?>/img/ant.png">
                <h1>
                    <small class="small-block mt"><strong>No tienes conductores para esta cotización</strong></small>
                </h1>
                <br><br><br>
              </div> 
            </div>
      <?php }?>
      </div>
      <!-- END right side -->
    </div>
    <div class="row white" id="mapid" style="height:400px; display:none;"></div>
  
  </div>
</div>
<?php
  $this->registerJs("var plate = ".json_encode($licensesplates).";", \yii\web\View::POS_BEGIN, 'plates'); 
  $this->registerJs("var contractemp = ".json_encode($contrato->pk_contract).";", \yii\web\View::POS_END, 'contractemps');
  $pub = Yii::$app->assetManager->publish('@app/web/js/modal-profile.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 

Modal::begin([
  'id' => 'profile',
  'header' => '<h2>Perfil del Transportador</h2>',
  'size'=> 'modal-lg',
  ]); 
  echo '<div id="modalContent"></div>';
Modal::end();


Modal::begin([
  'id' => 'endproposal',
  'header' => '<h3>¿Deseas finalizar esta carga?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['endtproposal', 'id_cot' => $id_cot, 'id_load' => $loadstatus->pkloadstatus ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();

?>
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/magnific-popup.css">
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.magnific-popup.min.js"></script>
<script>
   
  var latitude_origin = parseFloat($('#latitude_origin').val());
  var logitude_origin = parseFloat($('#logitude_origin').val());
  var latitude_destination = parseFloat($('#latitude_destination').val());
  var longitude_destination = parseFloat($('#longitude_destination').val());
  var latitude_trans = parseFloat($('#latitude_trans').val());
  var longitude_trans = parseFloat($('#longitude_trans').val());
  var mymap = L.map('mapid').setView([latitude_origin, logitude_origin], 13);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'rudmanmrrod.0f8gf90d',
    accessToken: 'pk.eyJ1IjoicnVkbWFubXJyb2QiLCJhIjoiY2lwcG5ja29lMDQ0emZqbTNuazRrZWxuciJ9.yYp783TnZbC7HxeIC8v96g'
  }).addTo(mymap)
  $('#mapid').hide();

  var redMarker = L.AwesomeMarkers.icon({ markerColor: 'red' });
  var blueMarker = L.AwesomeMarkers.icon({icon:'pushpin', markerColor: 'blue' });
  var greenMarker = L.AwesomeMarkers.icon({icon: 'truck',  prefix: 'fa',markerColor: 'green' });

  var origin = L.marker([latitude_origin,logitude_origin],{icon: blueMarker,title:'Origen'}).addTo(mymap);
  var destination = L.marker([latitude_destination,longitude_destination], {icon: redMarker, title:'Destino'}).addTo(mymap);
  if(latitude_trans.toLocaleString()!="NaN" && longitude_trans.toLocaleString()!="NaN")
  {
    var transporter_point = L.marker([latitude_trans,longitude_trans], {icon: greenMarker, title:'Transportador'}).addTo(mymap);
  }

  $('.administration-activity-info #mymap').click(function(){
    if($('#mapid').css('display')=='none')
    {
      $('#mapid').show();
    }
    else
    {
      $('#mapid').hide();
    }
  });

  //Desplegar los sellos como img
  $('.my-img').each(function(index,value){
    $(value).magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      closeBtnInside: false,
      fixedContentPos: true,
      image: {
        verticalFit: true
      }
    });
  });
  
  $('#inputSearch').autocomplete({source:plate});
  function loadFunction()
  {
    var inputplate = $('#inputSearch').val();
    if(inputplate=='')
    {
      $('#msg').html('<p class="text-danger">Debe ingresar la placa</p>');
    }
    else
    {
      $.get(url+'/accountc/tranportershortprofile?plate='+inputplate)
      .success(function(data){
        $('#msg').html('');
        $('#driver').html(data);
        $('#inputSearch').val('');
        $('#formtransporterpayment-plate').val(inputplate);
        $('#submit').attr('action',$('#submit').attr('action')+'&plate='+inputplate)
      })
      .error(function(data,estado){
        $('#driver').html('');
        $('#msg').html('<p class="text-danger">Ocurrío un error con esa placa</p>');
      });
    }
  }
</script>
<?php 
  use app\models\McTcategory;
  use app\models\McTcompanyservices;
  use app\models\McTproposal;
  use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <div class="main">
        <div class="row ">
          <div class="col-lg-12">
          <?= Html::a('CLIENTES', ['cotizar'], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
          <?= Html::a('TRANSPORTADOR', ['cotizar','type'=>1], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div>
        <div class="clearfix"></div>
          <div class="part3 col-lg-5 col-xs-12 right-side">
            <div class="col-md-2  form-title">Buscar:</div>
              <div class="col-md-5 col-xs-5">
                <?= yii\jui\DatePicker::widget(['name' => 'srch_date', 'dateFormat' => 'dd-MM-yyyy']) ?>
              </div>
              <div class="col-md-5 col-xs-5">
                <button type="submit" class="btn btn-primary submit">BUSCAR</button>
              </div>
          </div><br>
        <div class="blocks">
          <?php 
            if(isset($msg)) 
              echo "<center><h3 class='confirm'>$msg</h3></center><br>"; 
          ?>
          <?php foreach ($loads as $l) {?>
            <?php $user = $l->getFkuser0()->One(); ?>
            <?php $catg = McTcategory::find($l->getFksubcategory0()->One()->fkcategory)->One(); ?>
            <?php 
              $comi = McTcompanyservices::find()
                    ->where("fkcompany=:id", [":id" => Yii::$app->session->get("userid")])
                    ->andWhere('fksubcategory = :sub', [':sub' => $l->getFksubcategory0()->One()->pktcategory_subcategory])
                    ->one();
              $pexist = McTproposal::find()
                    ->where("fkcompany=:id", [":id" => Yii::$app->session->get("userid")])
                    ->andWhere('fkload = :load', [':load' => $l->pkload])
                    ->one();
              if($comi){
            ?>
            <div class="col-lg-12 qoute-main-block">
              <div class="row administration-side administration-side-activity qoute-inner-block margin-4">
                <div class="col-sm-6 col-xs-12 administration-activity-info ">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                      <img src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>">
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
                         <p class="large-title"><?php echo $user->user_name; ?></p>
                        <div class="type col-log-6">
                         <p><span class="type-text">Categoria:</span>  <span class="green"><?php echo $catg->category_name;?></span></p>
                         <p><span class="type-text">Sub-Catg:</span> <span class="green"><?php echo $l->getFksubcategory0()->One()->fksubcategory0->subcategory_name; ?></span></p>
                         <p><span class="type-text">Asegura:</span>  <span class="red">$ <?php echo number_format($l->load_ensure); ?></span></p>
                        </div>                    
                    </div>
                  </div>
                </div>
              <div class="col-sm-6 col-sm-offset-0 col-xs-offset-2 quote-box">
                  <div class="contract-box ">
              <form>
                <div class="type col-sm-8">
                <p><span class="width-box">Fecha orden:</span>    <span id="fecha" style="font-weight:bolder" class="blue"><?php echo date("d-m-Y",  strtotime($l->load_create));?></span></p>
                <p>
                  <span class="width-box">Valor a Pagar:</span>  
                  <span class="<?php echo $pexist ? "" : "blue"; ?>">
                    <?php 
                      if($pexist) 
                        echo '$ ' . number_format($pexist->proposal_total) . ' COP';
                      else{?>
                      <input type="number" class="textbox-field vp<?php echo $l->pkload; ?>">
                      <span class="field_title">COP</span>
                    <?php } ?>
                  </span>
                </p>
                <p>
                  <span class="width-box">Pago Minimo:</span> 
                  <span class="<?php echo $pexist ? "" : "blue"; ?>">
                    <?php 
                      if($pexist) 
                        echo '$ ' . number_format($pexist->proposal_paymin) . ' COP';
                      else{?>
                      <input type="number" class="textbox-field pm<?php echo $l->pkload; ?>">
                      <span class="field_title">COP</span>
                    <?php } ?>
                  </span>
                </p>
                <p><span class="width-box">% Comisión:</span>  <span class="blue bold-large"> <?php echo $comi->companyservices_percent; ?>%</span> </p>
              </div>
              <div class="type col-sm-4"> 
              <?php if(!$pexist) {?>
               <button type="button" class="btn btn-active submit quote-btn btn-act btn-cotizar" id="<?php echo $l->pkload; ?>">COTIZAR</button>
              <?php }?>
              </div>
               </form>
              </div>
              </div>
            </div>
            <div class="row administration-side administration-side-activity qoute-inner-block">
              <div class="col-sm-10 col-xs-12 administration-activity-info small-padding-around">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-5">
                   <div class="description-block">
                    <p><span class="width-box"><strong>Desde:</strong></span><span><?php echo $l->load_cityorigin; ?></span></p>
                    <p><span class="width-box"><strong>Hasta:</strong></span><span><?php echo $l->load_citydestination; ?></span></p>
                     <p><span class="width-box"><strong>Tipo:</strong></span><span>No definido</span></p>
                      <p><span class="width-box"><strong>Peso:</strong></span><span><?php echo $l->load_weight < 1000 ? $l->load_weight . " Kl" : ($l->load_weight / 1000) . " Ton" ?></span></p>
                   </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-5">
                      <div class="Dimensiones-block">
                    <p><span class="width-box"><strong>Dimensiones:</strong></span><span><?php echo $l->load_dimensions; ?></span></p>
                    <p><span class="width-box"><strong>Observaci&oacute;n:</strong></span><span class="observation-text"><?php echo $l->load_comment; ?></span></p>
                   </div>                
                  </div>
                </div>
              </div>
              <div class="col-sm-2 col-sm-offset-0  Remove-box">
                <?php if(!$pexist) {?>
                 <button type="submit" class="btn btn-primary submit remove-btn" id="<?php echo $l->pkload; ?>">ELIMINAR</button>
                <?php }?>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {  
    $(".remove-btn").click(function(){
      var id = $(this).attr("id");
      //var baseUrl = "http://micargapp.com/web";
      swal({   
        title: "¿Está seguro de eliminar esta cotización?",   
        text: " La acción no podrá deshacerse!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#ff6b6b",   
        cancelButtonText: "Cancelar", 
        confirmButtonText: "Si, Eliminar!",   
        closeOnConfirm: true 
      }, function(){   
          var params = {lid : id};
          var body = document.body;
          form = document.createElement('form');
          form.method = 'POST';
          form.action = url + "/accountc/hideload";
          form.name = 'jsform';
          for (index in params) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = index;
            input.id = index;
            input.value = params[index];
            form.appendChild(input);
          }
          body.appendChild(form);
          form.submit();
      });
    });
    $(".btn-cotizar").click(function(){
      var id = $(this).attr("id");
      //var baseUrl = "http://micargapp.com/web";
      var vp = $(".vp" + id).val();
      var pm = $(".pm" + id).val();
      if(vp == ""){
         $(".vp" + id).attr("style","border: 1px solid #a94442 !important;");
         $(".pm" + id).removeAttr("style");
         return;
      }
      if(pm == ""){
        $(".pm" + id).attr("style","border: 1px solid #a94442 !important;");
        $(".vp" + id).removeAttr("style");
        return;
      }
      $(".pm" + id).removeAttr("style");
      $(".vp" + id).removeAttr("style");
      swal({   
        title: "¿Está seguro de enviar la cotización?",   
        text: "Una vez enviada no puede ser modificada",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#65b3fe",   
        cancelButtonText: "Cancelar", 
        confirmButtonText: "Si, Enviar!",   
        closeOnConfirm: true 
      }, function(){   
          var params = {
            lid : id,
            vp: vp,
            pm: pm
          };
          var body = document.body;
          form = document.createElement('form');
          form.method = 'POST';
          form.action = url + "/accountc/sendproposal";
          form.name = 'jsform';
          for (index in params) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = index;
            input.id = index;
            input.value = params[index];
            form.appendChild(input);
          }
          body.appendChild(form);
          form.submit();
      });
    });
  });
 $('.submit').click(function(event){
    event.preventDefault();
    $('.qoute-main-block').each(function(index,value){
      $(this).show();
    });
    var value_date = $('#w0').val();
    $('.blocks').find('.not-found').remove();
    if(value_date)
    {
      $('.qoute-main-block').each(function(index,value){
        var bool = false;
        var m = $(value).find('#fecha').text()===value_date;
        (m===true) ? bool = true: bool = false;
        console.log(m);
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.qoute-main-block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=true;
    $('.qoute-main-block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool && value_date)
    {
      var txt ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      txt+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      txt+='</h1></div></div>';
      $('.qoute-main-block').parent().append(txt);
    }
  });
</script>
<?php
use yii\helpers\Html;
use app\models\McTfriendship;
use yii\bootstrap\Modal;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row">
      <!-- left side -->
        <div class="col-sm-4 administration-left-side-wrapper"><br>
          <div class="col-xs-12 administration-side administration-left-side new-user-za">
            <h1>Buscar Conductor</h1>

            <div class="row">
              <input class="form-inline search col-sm-10" type="text" id="inputSearch" placeholder="Placa del Vehículo">
              <a class="send col-sm-2"><i class="glyphicon glyphicon-search align-down"></i></a>
            </div><br>
            <div class="row">
              <p class="blue">Escribe la placa con letras y números seguidos, sin guiones</p>
            </div>
            <div class="row transporter_profile">

            </div>
          </div>

          <div class="col-xs-12 administration-submit">
            <a type="button" class="btn btn-sub2 btn-active mybtn col-xs-12">INVITAR CONDUCTOR</a>
          </div>

        </div>
      <!-- END left side -->

      <!-- right side -->
      <div class="col-sm-8 administration-right-side"><br>
        <div class="row administration-side administration-right-side-title-wrapper">
          <h1>Conductores activos</h1>
        </div>
        <?php foreach ($transporter as $t) {?>
          <?php 
          $profile = $t->getMcTusers()->One(); 
          $vehicle = $profile->getMcTvehicles()->One();
          $friendship = McTfriendship::find()->where(['fkuser_send'=>Yii::$app->user->id,'fkuser_recive'=>$t->id])->one();
          ?>
          <div class="row administration-side administration-side-activity">
            <div class="col-sm-9 col-xs-12 administration-activity-info">
              <div class="row">
                <div class="col-xs-3">
                  <img src="<?php if($profile->user_avatar){echo Yii::getAlias('@web'). $profile->user_avatar;}
                  else {echo Yii::getAlias('@web').'/img/pp.png';} ?>">
                </div>
                <div class="col-xs-9">
                  <ul class="list-unstyled">
                    <li><b><?php echo $profile->user_name; ?></b></li>
                    <li class="red"><b><?php echo strtoupper($vehicle->vehicle_licenceplate); ?></b></li>
                    <li class="blue"><?php echo $profile->user_phone; ?></li>
                    <li class="blue"><?php echo $t->email; ?></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12 administration-activity-buttons">
              <div class="row">
                <div class="col-xs-12">
                    <?php 
                      if(!$friendship)
                      {
                        echo '<button class="btn btn-desactive col-xs-12">PENDIENTE</button>';
                      }
                      else
                      {
                        if($friendship->status==1)
                        {
                          echo '<button class="btn btn-desactive btn-blue col-xs-12">ACTIVO</button>';
                        }
                        else if ($friendship->status==2) 
                        {
                          echo Html::a('CONFIRMAR', ['confirm', 'id' => $t->id ], ['class' => 'btn btn-sub2 btn-desactive btn-green col-xs-12']);
                        }
                        else
                        {
                          echo '<button class="btn btn-desactive col-xs-12">ENVIADA</button>';
                        }
                      }
                    ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?= Html::button('VER PERFIL', ['class' => 'btn btn-desactive btn-green col-xs-12','name' => $t->id,]) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?= Html::a('ELIMINAR', ['delete', 'id' => $t->id ], ['class' => 'btn btn-sub2 btn-desactive btn-red col-xs-12',
                      'data' => [
                        'confirm' => '¿Deseas eliminar este usuario de sus amistades?',
                        'method' => 'post',
                    ],
                  ]) ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <!-- END right side -->
    </div>
  </div>
</div>
<?php 
  $pub = Yii::$app->assetManager->publish('@app/web/js/modal-profile.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); ?>

<?php
Modal::begin([
  'id' => 'profile',
  'header' => '<h2>Perfil del Transportador</h2>',
  'size'=> 'modal-lg',
  ]); 
  echo '<div id="modalContent"></div>';
Modal::end();
?>
<script>
  var my_data = [];
  $('.red').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  
  $(document).ready(function() {
    $('.send').click(function(){
      var inputText = $('#inputSearch').val();
      $('.red').each(function(index,value){
        if($(value).text()==inputText)
        {
          var button_id = $(value).parent().parent().parent().parent().parent();
          var myprofile = $(button_id).find('.btn-green').parent();
          $('.transporter_profile').html($(value).parent().parent().parent().html()+$(myprofile).html());
          var myid = get_id($(button_id).find('.btn-red').attr('href'));
          $('.mybtn').attr('href',url+'/accountc/friendship?friendId='+myid);
          seeProfile();
        }
      });
      if(inputText=='')
      {
        $('.transporter_profile').html('');
        $('.mybtn').removeAttr('href');
      }
    });
  });

  function get_id(item)
  {
    var a = item.split('/');
    a = a[a.length-1];
    var b = a.split('?');
    return b[0];
  }
</script>
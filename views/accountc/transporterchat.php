<?php 
use yii\helpers\Html;
?>
<div class="content">
	<div class="container">
	<?php
	if($action=='usertcontract')
	{
		echo Html::a('Regresar', [$action,'id_contract'=> $id_contract], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']);
	}
	else if ($action=='seecotization')
	{
		echo Html::a('Regresar', [$action,'id_cot'=> $id_contract], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']);
	}
	?>
	</div>
</div>

<div class="content">
  <div class="container">
	<div class="container chatcontent">
	  <div class="row white" ng-controller="ChatTransporter">
	    <div class="col col-20"></div>
	    <div class="col col-66">
	      <div class="">
	          <div class="">
	              <div class="btn-group pull-right">
	                  
	              </div>
	          </div>
	          <div class="panel-body">
	              <ul class="chat">
	                  <!-- DATOS -->
	                  <div ng-repeat="mensaje in mensajes.slice().reverse()">
	                    <li class="left clearfix chatli">
	                        <div class="chat-body clearfix">
	                          <div class=""
	                            ng-class="mensaje[1] ? 'panel-footer-blue' : 'panel-footer-gray'">
	                            {{mensaje[0]}} {{ mensaje[3] }}
	                          </div>
	                          <p class=""
	                            ng-class="mensaje[1] ? 'box-blue' : 'box-gray'">
	                              {{ mensaje[2] }}
	                          </p>
	                        </div>
	                    </li>
	                  </div>
	              </ul>
	          </div>
	          <form name="form">
	            <div>
	                <div class="list">
	                  <div class="item item-input-inset back-positive text-center">
	                    <label class="item-input-wrapper input-50">
	                      <input type="text" placeholder="Escribir..."
	                       name="textMsj" ng-model="textMsj" id="input_send" size="70px" focus-me-now>
	                    </label>
	                    <a class="button button-small back-positive" ng-click="send()" >
	                      <i class="glyphicon glyphicon-send"></i>
	                    </a>
	                  </div>
	                </div>
	            </div>
	          </form>
	      </div>
	    </div>
	  </div>
	</div>
	<?php $this->registerJs("var contractemp = ".json_encode($id_transcontract).";", \yii\web\View::POS_END, 'contractemps'); ?>
  </div>
</div>
<?php 
  use yii\helpers\Html;
  use yii\helpers\ArrayHelper;
  use yii\widgets\ActiveForm;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <div class="main">
        <div class="row">
          <div class="col-lg-12">
          <?= Html::a('CLIENTES', ['cotizar'], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('TRANSPORTADOR', ['cotizar','type'=>1], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div>
        <?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
        <div class="blocks administration-right-side-wrapper">
          <div class="col-sm-4"></div>
          <div class="col-sm-4">
            <div class="col-xs-12 administration-side new-user-za">
              <h1 class="text-center">Cotizar Carga</h1><hr>
            </div>
            <div class="accountc-FormCotizarEmp">

              <?php $form = ActiveForm::begin([
                'enableClientValidation' => true,
                'action' => ['cotizar', 'type' => '1'],
              ]); ?>

                  <?= $form->field($model, 'origin')->input("text", ['placeholder' => 'Lugar Origen', 'class' => 'administration-input'])->label(false) ?>
                  <?= $form->field($model, 'destination')->input("text", ['placeholder' => 'Lugar Destino', 'class' => 'administration-input'])->label(false) ?>
                  <?= $form->field($model, 'fk_vehicletype')->dropDownList(ArrayHelper::map($vtype,'pkvehicletype','vehicletype_name'), ['prompt'=>'Tipo de Vehículo', 'class' => 'administration-input'])->label(false); ?>
                  <?= $form->field($model, 'weight')->input("text", ['placeholder' => 'Peso', 'class' => 'administration-input'])->label(false) ?>
                  <?= $form->field($model, 'payment')->input("number", ['placeholder' => 'Valor a Pagar por Vehículo', 'class' => 'administration-input'])->label(false) ?>
                  <?= $form->field($model, 'vehicle_number')->input("number", ['placeholder' => 'Número de Vehículos', 'class' => 'administration-input', 'min'=>'0'])->label(false) ?>
                  <?= $form->field($model, 'comment')->textarea(['placeholder' => 'Comentarios', 'class' => 'administration-input'])->label(false) ?>
                  <div id="mapid" style="height: 180px;"></div>
                  <div class="row">
                    <div class="col-md-6">
                      <?= $form->field($model, 'latitude_orig')->textInput(['placeholder' => 'Latitud', 'class' => 'administration-input form-field', 'readonly'=>true])->label(false); ?>
                    </div>
                    <div class="col-md-6">
                      <?= $form->field($model, 'longitude_orig')->textInput(['placeholder' => 'Longitud', 'class' => 'administration-input form-field', 'readonly'=>true])->label(false); ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <?= $form->field($model, 'latitude_dest')->textInput(['placeholder' => 'Latitud', 'class' => 'administration-input form-field', 'readonly'=>true])->label(false); ?>
                    </div>
                    <div class="col-md-6">
                      <?= $form->field($model, 'longitude_dest')->textInput(['placeholder' => 'Longitud', 'class' => 'administration-input form-field', 'readonly'=>true])->label(false); ?>
                    </div>
                  </div>
                  <?= $form->field($model, 'alltcheck')->checkbox(['value'=>'1', 'label'=>'Envíar a Todos mis conductores'])->label(false) ?>
              
              

          </div><!-- accountc-FormCotizarEmp -->
            <div class="col-xs-12 administration-submit">
              <?= Html::submitButton('COTIZAR CARGA', ['class' => 'btn btn-active col-xs-12']) ?>
            </div>
          </div>

          <?php ActiveForm::end(); ?>

          <div class="col-sm-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var mymap = L.map('mapid').setView([4.59772, -74.07656], 13);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'rudmanmrrod.0f8gf90d',
    accessToken: 'pk.eyJ1IjoicnVkbWFubXJyb2QiLCJhIjoiY2lwcG5ja29lMDQ0emZqbTNuazRrZWxuciJ9.yYp783TnZbC7HxeIC8v96g'
}).addTo(mymap)

  var redMarker = L.AwesomeMarkers.icon({ markerColor: 'red' });
  var blueMarker = L.AwesomeMarkers.icon({icon:'pushpin', markerColor: 'blue' });

  var origin = L.marker([0,0],{icon: blueMarker,title:'Origen'}).addTo(mymap);
  var destination = L.marker([0,0], {icon: redMarker, title:'Destino'}).addTo(mymap);

  var OriginControl = L.Control.extend({
 
    options: {
      position: 'topright' 
      //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
    },
   
    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      $(container).append("<i class='glyphicon glyphicon-pushpin' style='top:8px;left:8px;'></i>");
      $(container).attr('name','origin-point');
      $(container).attr('title','Origen');
      L.DomEvent.on(container, 'mousedown dblclick', L.DomEvent.stop)
      .on(container, 'click', L.DomEvent.stop);
   
      container.style.backgroundColor = 'white';
      container.style.width = '30px';
      container.style.height = '30px';
      return container;
    },
  });

  var DestinationControl = L.Control.extend({
 
    options: {
      position: 'topright',
      title: 'Destino', 
      //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
    },
   
    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      $(container).append("<i class='glyphicon glyphicon-home' style='top:8px;left:8px;'></i>");
      $(container).attr('name','destination-point');
      $(container).attr('title','Destino');
      L.DomEvent.on(container, 'mousedown dblclick', L.DomEvent.stop)
      .on(container, 'click', L.DomEvent.stop);
   
      container.style.backgroundColor = 'white';
      container.style.width = '30px';
      container.style.height = '30px';
      return container;
    },
  });



  function onMapClick(e) {
    var text = $('.selected').attr('name');
    if(text=="origin-point")
    {
      origin.setLatLng(e.latlng);
      $('#formcotizaremp-latitude_orig').val(e.latlng.lat);
      $('#formcotizaremp-longitude_orig').val(e.latlng.lng);
    }
    else if(text=="destination-point")
    {
      destination.setLatLng(e.latlng);
      $('#formcotizaremp-latitude_dest').val(e.latlng.lat);
      $('#formcotizaremp-longitude_dest').val(e.latlng.lng);
    }
    console.log(e.latlng);
  }

  mymap.addControl(new OriginControl());
  mymap.addControl(new DestinationControl());
  mymap.doubleClickZoom.disable(); 
  mymap.on('click', onMapClick);

  $(document).ready(function(){
    $('.leaflet-control-custom').click(function(){
      var attribute = $(this).attr('class');
      if(attribute.search('selected')!=-1)
      {
        $(this).removeClass('selected');
        $(this).css('background-color','white');
      }
      else
      {
        var anterior = $('.leaflet-right').find('.selected');
        $(anterior).removeClass('selected');
        $(anterior).css('background-color','white');
        $(this).addClass('selected');
        $(this).css('background-color','gray');
      }
    });
});
</script>
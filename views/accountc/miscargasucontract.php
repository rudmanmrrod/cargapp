<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->

        <div class="row ">
          <div class="col-lg-12">
            <?= Html::a('CLIENTES', ['miscargas'], ['class' => 'btn btn-active btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-21']) ?>
            <?= Html::a('TRANSPORTADOR', ['miscargas','perfil'=>2], ['class' => 'btn btn-desactive btn-sub2 top-button col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
         </div>
        </div><br>

        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <div class="main">
        <div class="row ">
          <div class="col-lg-6">
            <?= Html::a('Mis Conductores', ['usertcontract','id_contract'=>$id_contract], ['class' => 'btn btn-desactive btn-sub2 col-md-4 col-xs-4 top-button']) ?>
            <?= Html::a('Solicitudes a Conductores', ['listuproposal','id_contract'=>$id_contract], ['class' => 'btn btn-desactive btn-sub2 col-md-4 col-xs-4 top-button']) ?>
            <?= Html::a('Clientes', ['userucontract','id_contract'=>$id_contract], ['class' => 'btn btn-active btn-sub2 col-md-4 col-xs-4 top-button']) ?>
         </div>
        </div><br>
        <div class="blocks">
            <div class="col-lg-12 qoute-main-block">
              <div class="row administration-side administration-side-activity qoute-inner-block margin-4">
                <div class="col-sm-6 col-xs-12 administration-activity-info ">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                      <img src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>">
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
                         <p class="large-title"><?php echo $user->user_name; ?></p>
                        <div class="type col-log-6">
                         <p><span class="type-text">Email:</span>  <span class="blue"><?php echo $user->fklogin0->email;?></span></p>
                         <p><span class="type-text">Teléfono:</span>  <span class="blue"><?php echo $user->user_phone;?></span></p>
                         <p><span class="type-text">Categoria:</span>  <span class="green"><?php echo $carga->fksubcategory0->fkcategory0->category_name;?></span></p>
                         <p><span class="type-text">Sub-Catg:</span> <span class="green"><?php echo $carga->fksubcategory0->fksubcategory0->subcategory_name; ?></span></p>
                         <p><span class="type-text">Asegura:</span>  <span class="red">$ <?php echo number_format($carga->load_ensure); ?></span></p>
                        </div>                    
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-sm-offset-0 col-xs-offset-2 quote-box">
                  <div class="contract-box ">
                    <div class="type col-sm-8">
                    <p><span class="width-box">Fecha orden:</span>    <span id="fecha" style="font-weight:bolder" class="blue"><?php echo date("d-m-Y",  strtotime($carga->load_create));?></span></p>
                    <p>
                      <span class="width-box">Valor a Pagar:</span>  
                      <span>
                        <?php 
                            echo number_format($contrato->fkProposal->proposal_total) . ' COP';
                        ?>
                      </span>
                    </p>
                    <p>
                      <span class="width-box">Pago Minimo:</span> 
                      <span>
                        <?php 
                            echo number_format($contrato->fkProposal->proposal_paymin) . ' COP';
                            ?>
                      </span>
                    </p>
                    <p><span class="width-box">% Comisión:</span>  <span class="blue bold-large"> 
                      <?php echo $company_percent->companyservices_percent; ?>%</span> </p>
                  </div>
                  <div class="type col-sm-3"> 
                    <?php 
                      $zeros = '000000';
                      $contract_type = ''.$contrato->pk_contract;
                      $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                    ?>
                    <div class="row">
                      <b><span class="width-box">Contrato: </span> <span class="blue contract-name"><?php echo 'UE'.$contract_number; ?></span></b>
                    </div>
                   <button type="button" class="btn btn-active submit menu-btn btn-act btn-cotizar" onclick="showChat()">CHAT</button>
                   <button type="button" class="btn btn-active submit menu-btn btn-act btn-cotizar" id="mymap">MAPA</button>
                   <input type="text" id="latitude_origin" value='<?php echo $carga->latitude_origin; ?>' style="display:none;">
                   <input type="text" id="longitude_origin" value='<?php echo $carga->longitude_origin; ?>' style="display:none;">
                   <input type="text" id="latitude_destination" value='<?php echo $carga->latitude_destination; ?>' style="display:none;">
                   <input type="text" id="longitude_destination" value='<?php echo $carga->longitude_destination; ?>' style="display:none;">
                  </div>
                </div>
              </div>
            </div>
            <div class="row administration-side administration-side-activity qoute-inner-block">
              <div class="col-sm-10 col-xs-12 administration-activity-info small-padding-around">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-5">
                   <div class="description-block">
                    <p><span class="width-box"><strong>Desde:</strong></span><span><?php echo $carga->load_cityorigin; ?></span></p>
                    <p><span class="width-box"><strong>Hasta:</strong></span><span><?php echo $carga->load_citydestination; ?></span></p>
                     <p><span class="width-box"><strong>Tipo:</strong></span><span>No definido</span></p>
                      <p><span class="width-box"><strong>Peso:</strong></span><span><?php echo $carga->load_weight < 1000 ? $carga->load_weight . " Kl" : ($carga->load_weight / 1000) . " Ton" ?></span></p>
                   </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-5">
                      <div class="Dimensiones-block">
                    <p><span class="width-box"><strong>Dimensiones:</strong></span><span><?php echo $carga->load_dimensions; ?></span></p>
                    <p><span class="width-box"><strong>Observaci&oacute;n:</strong></span><span class="observation-text"><?php echo $carga->load_comment; ?></span></p>
                   </div>                
                  </div>
                </div>
              </div>
              <div class="col-sm-2 col-sm-offset-0  Remove-box">
                 <button type="submit" class="btn btn-primary submit remove-btn" id="<?php //echo $l->pkload; ?>">ELIMINAR</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row white" id="mapid" style="height:400px; display:none;"></div>
      
    <div class="row white chatcontent" ng-controller="Chat" style="display:none;">
      <div class="col col-20"></div>
      <div class="col col-66">
        <div class="">
            <div class="">
                <div class="btn-group pull-right">
                    
                </div>
            </div>
            <div class="panel-body">
                <ul class="chat">
                    <!-- DATOS -->
                    <div ng-repeat="mensaje in mensajes.slice().reverse()">
                      <li class="left clearfix chatli" >
                          <div class="chat-body clearfix">
                            <div class=""
                              ng-class="mensaje[1] ? 'panel-footer-blue' : 'panel-footer-gray'">
                              {{mensaje[0]}} {{ mensaje[3] }}
                            </div>
                            <p class=""
                              ng-class="mensaje[1] ? 'box-blue' : 'box-gray'">
                                {{ mensaje[2] }}
                            </p>
                          </div>
                      </li>
                    </div>
                </ul>
            </div>
            <form name="form">
              <div>
                  <div class="list">
                    <div class="item item-input-inset back-positive text-center">
                      <label class="item-input-wrapper input-50">
                        <input type="text" placeholder="Escribir..."
                         name="textMsj" ng-model="textMsj" id="input_send" size="70px" focus-me-now>
                      </label>
                      <a class="button button-small back-positive" ng-click="send()" >
                        <i class="glyphicon glyphicon-send"></i>
                      </a>
                    </div>
                  </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>

</div>
<?php $this->registerJs("var contractemp = ".json_encode($contrato->pk_contract).";", \yii\web\View::POS_END, 'contractemps'); ?>
<script>
  
  var latitude_origin = parseFloat($('#latitude_origin').val());
  var logitude_origin = parseFloat($('#longitude_origin').val());
  var latitude_destination = parseFloat($('#latitude_destination').val());
  var longitude_destination = parseFloat($('#longitude_destination').val());
  var mymap = L.map('mapid').setView([latitude_origin, logitude_origin], 13);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'rudmanmrrod.0f8gf90d',
    accessToken: 'pk.eyJ1IjoicnVkbWFubXJyb2QiLCJhIjoiY2lwcG5ja29lMDQ0emZqbTNuazRrZWxuciJ9.yYp783TnZbC7HxeIC8v96g'
  }).addTo(mymap)
  $('#mapid').hide();
  var redMarker = L.AwesomeMarkers.icon({ markerColor: 'red' });
  var blueMarker = L.AwesomeMarkers.icon({icon:'pushpin', markerColor: 'blue' });

  var origin = L.marker([latitude_origin,logitude_origin],{icon: blueMarker,title:'Origen'}).addTo(mymap);
  var destination = L.marker([latitude_destination,longitude_destination], {icon: redMarker, title:'Destino'}).addTo(mymap);
  $('#mymap').click(function(){
    if($('#mapid').css('display')=='none')
    {
      $('#mapid').show();
    }
    else
    {
      $('#mapid').hide();
    }
  });

  function showChat()
  {
    if($('.chatcontent').css("display")=="none")
    {
     $('.chatcontent').show(); 
    }
    else
    {
      $('.chatcontent').hide();
    }
  }
</script>
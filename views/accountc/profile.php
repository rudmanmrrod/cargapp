<div class="row administration-side administration-side-activity block-border">
  <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12 administration-activity-info ">
    <div class="row">
      <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3"> 
        <img src=<?php if($profile->user_avatar){echo Yii::getAlias('@web'). $profile->user_avatar;}
        else {echo Yii::getAlias('@web').'/img/pp.png';} ?>>  
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <ul class="list-unstyled">
          <li class="large-title"><?php echo $profile->user_name; ?></li>
          <li>Ultima conexion: <span class="green"><?php echo date("d-m-Y H:i",  strtotime($model->last_connection));?></span></li>
          <li class="green"><?php echo $profile->user_phone; ?></li>
          <li class="blue"><?php echo $model->email2; ?></li>
          <li><b>Tipo de Vehículo: </b><?php echo $vehicle->fkvehicletype0->vehicletype_name; ?></li>
          <li><b>Tipo de Carrocería: </b><?php echo $vehicle->vehicle_bodywork; ?></li>
          <li><b>Placa: </b><?php echo $vehicle->vehicle_licenceplate; ?></li>
          <li><b>Marca: </b><?php echo $vehicle->vehicle_brand; ?></li>
          <li><b>Modelo: </b><?php echo $vehicle->vehicle_model; ?></li>
          <li><b>Color: </b><?php echo $vehicle->vehicle_color; ?></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <ul class="list-unstyled">
          <?php 
          foreach ($references as $r){
            echo '<li><b>'.strtoupper($r->fkReferencetype->reference_name).'</b></li>';
            echo '<li>'.$r->reference_name.'</li>';
            echo '<li>'.$r->reference_address.'</li>';
            echo '<li class="green">'.$r->reference_phone.'</li>';
            echo '<br>';
          }
          ?>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <ul class="list-unstyled">
          <?php 
          foreach ($attachment as $index => $value){
            $val = ($index+1)%2==0 ? ' - 2':' - 1';
            echo '<li><a class="my-img" href='.Yii::getAlias('@web').$value->attachment_location.'>'.$value->attachmentType->name.$val.'</a></li>';
          }
          ?>
        </ul>
      </div>
    </div>
      <div class="row">
        <div class="center-block" id="myimg"></div>
      </div>
  </div>
</div>
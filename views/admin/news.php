<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper"> 
      
      <!-- main content -->
      
      <div class="main"> 
        
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a class="btn <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-3 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/admin/noticias">USUARIOS</a>
          <a class="btn <?php echo $option == 2 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-3 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/admin/noticias?perfil=2">TRANSPORTADORES</a>
        </div>
        
        <!--end head--> 
        
      </div>
      
      <!--End content--> 
      
    </div>
  </div>
</div>
<?php if(isset($msg)) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row white">
      <!-- main content -->
      <div class="main">
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
              <!-- Block ---------------------------------------------------------->
              <div class="block row col-lg-12">
                <!-- left side -->
                <?php if($type != ""){?>
                  <div class="col-md-12">
                    <div class="alert alert-<?php echo $type == 'true' ? 'success' : 'false'; ?>" role="alert"> 
                    <strong><?php echo $message; ?></strong></div>
                  </div>
                <?php }?>
                
                <div class="profile-left-side col-md-6 col-xs-12">
                  <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'id' => 'frmnews',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                    'options' => ['enctype'=>'multipart/form-data']
                  ]);
                  ?>
                    <?= $form->field($model, "type")->input("hidden", ['value'=>$option])->label(false);?>
                    <div class="col-md-12">
                      <?= $form->field($model, "title")->input("text", ['placeholder' => 'TÍTULO', 'class' => 'itm_not'])->label(false) ?>   
                    </div>
                    <div class="col-md-12 mt20">
                      <?= $form->field($model, 'message')->textarea(['placeholder' => 'TEXTO', 'class' => 'itm_not', 'rows'=> '10'])->label(false); ?>
                    </div>
                    <div class="col-md-2 col-lg-2 hidden-xs">
                        <h1 class="info-title">IMAGEN</h1>
                    </div>
                    <div class="col-md-10 col-lg-10" style="height:200px">
                      <div class="profile-photo wd100">
                        <div class="photo-area p2 wd100">
                          <div class="fileUpload wd100"> <span class="visible-xs">Imagen</span>
                            <?= $form->field($model, "image")->fileInput(['class' => 'tm_not upload upload2'])->label(false); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-offset-2 col-md-4 mt20">
                        <a class="btn btn-block btn-success submit pub padding-t10">PUBLICAR</a>
                    </div>
                  <?php $form->end() ?>

                </div>
                <!-- END left side -->
                <!-- right side -->
                <div class="col-md-6 col-xs-12">
                  <?php foreach ($news as $new){ ?>
                    <div class="article">
                          <div class="profile-right-side col-xs-12">
                            <?php if($new->new_image == ''){ ?>
                              <div class="profile-info article-image-wrapper col-xs-12">
                                  <img class="profile-image" src="<?php echo Yii::getAlias('@web')?>/img/icon-user.png" alt="Profile Picture"/>
                              </div>
                            <?php }else{ ?>
                              <img class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="padding-right: 0px; padding-left: 0px;" src="<?php echo Yii::getAlias('@web') . $new->new_image;?>">
                            <?php } ?>
                            <h1 class="article-title"><?php echo $new->new_title; ?></h1>
                            <h3 class="article-date"><?php echo date("d M Y",  strtotime($new->new_date));?></h3>
                            <p class="article-text"><?php echo $new->new_message; ?></p>
                          </div>
                          <div class="col-xs-12 padding-r0">    
                           <div class="col-sm-4 col-xs-6 col-sm-offset-4">
                                <?= Html::a('EDITAR', ['noticiasedit', 'id' => $new->pknew], ['class' => 'btn btn-desactive btn-green submit article-button button-top-margin-10 padding-t10']) ?>
                            </div>                
                            <div class="col-sm-4 col-xs-6 padding-r0">
                                <?= Html::a('ELIMINAR', ['noticiasdelete', 'id' => $new->pknew, 'perfil' => $option ], ['class' => 'btn btn-desactive btn-red article-button elimn delete padding-t10',
                                    'onclick'=>'SeeModal(event,deletenews,'. $new->pknew.',this,"a","href")',
                                ]) ?>
                            </div>             
                          </div> 
                    </div>
                  <?php } ?>
                </div>
                <!-- END right side -->
            </div>

          </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>

<?php 
Modal::begin([
  'id' => 'deletenews',
  'header' => '<h3>¿Deseas eliminar esta noticia?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row text-center">';
  echo '<p>¿Está seguro que desea eliminar esta noticia?<br>';
  echo '¡Está acción no se puede deshacer!</p>';
  echo '</div>';
  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['noticiasdelete', 'id' => '', 'perfil' => $option ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>

<script type="text/javascript">
$(document).ready(function() {  
    var base64 = "";
    $(".pub").click(function(){
      $("#frmnews").submit();
    });
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "100% auto");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
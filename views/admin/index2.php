<?php
use app\models\McTloadproposal;
use app\models\McUloadproposal;
use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/index" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-4 col-lg-3">USUARIO/EMPRESA</a>
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/index?type=2" class="btn btn-sub2 btn-active col-md-3 col-xs-12 col-sm-4 col-lg-3">EMPRESA/TRANSPORTADOR</a>
        </div>
        <!--end head-->

        <!-- parts -->
        <div class="body-main col-lg-12">
          <div class="col-lg-8 col-sm-8">
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Activo">ACTIVOS</button>
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Finalizado">FINALIZADOS</button>
          </div>
          <div class="part3 col-lg-4 col-xs-4">
            <div class="form-group col-md-8 col-xs-7">
              <input class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
            </div>
            <button type="submit" class="btn btn-primary col-md-4 col-xs-5 submit">BUSCAR</button>
          </div>
        </div>
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
            if($contratos){
            foreach ($contratos as $contrato) {
                $user = $contrato->getFkuser0()->one();
                $perfil = $user->getMcTusers()->one();
                $vehicle = $perfil->getMcTvehicles()->one();
                if($contrato->fkcontract0)
                {
                  $emp_user = $contrato->fkcontract0->getFkuserCompany()->one();
                  $empresa = $emp_user->getMcTcompanies()->one();
                  $proposal = McUloadproposal::find()
                  ->where(['fktransportador'=>$user->id,'fkcontract'=>$contrato->fkcontract0->pk_contract])->one();
                  $load = $contrato->fkcontract0->getFkCarga()->One();
                }
                else
                {
                  $empresa = $contrato->fkcotizaremp0->getFkcompany0()->one();
                  $emp_user = $empresa->getFklogin0()->one();
                  $proposal = McTloadproposal::find()
                  ->where(['fktransportador'=>$user->id,'fkcotizacionemp'=>$contrato->fkcotizaremp0->pkcotizaremp])->one();
                  $load = $contrato->getFkcotizaremp0()->one();
                }

            ?>
          <div class="block row col-lg-12">
            <div class="info col-lg-9">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-1">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $empresa->company_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="types user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $empresa->company_rz ;?></h1>
                    <p><?php echo $empresa->company_attendant ;?></p>
                    <div class="type col-log-7">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Costo:</p>
                    </div>
                    <div class="col-log-5">
                     <p class="blue"><?php echo $emp_user->email2;?></p>
                     <p class="blue"><?php echo $empresa->company_phone;?></p>
                     <p class="green"><?php echo $proposal->payment. ' COP';?></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-2">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $perfil->user_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $perfil->user_name; ?></h1>
                    <div class="type col-log-6">
                      <p>Placa:</p>
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                    </div>
                    <div class="col-log-6">
                     <p class="red"><?php echo $vehicle->vehicle_licenceplate;?></p>
                     <p class="blue"><?php echo $user->email;?></p>
                     <p class="blue"><?php echo $perfil->user_phone; ?></p>
                    </div>
                  </div>
              </div>
            </div>
          </div><!-- /.info -->
          <?php 
            $zeros = '000000';
            $contract_type = ''.$contrato->pk_contract;
            $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
          ?>
          <div class="right-part col-lg-3">
            <p>Contrato : <span class="blue contract-name"><?php echo 'ET'.$contract_number;?></span></p>
            <div class="type col-log-6">
              <p>Fecha orden:</p>
              <p>Estado</p>
            </div>
            <div class="col-log-6">
             <p style="font-weight:bolder"><?php echo date('d/m/Y',strtotime($contrato->signature_time));?></p>
             <p class="red status"><?php echo $contrato->fkStatus->status_name; ?></p>
            </div>
          </div><!-- /.right-part -->

          <div class="clearfix"></div>
          <!--info2 -->
          <div class="info2 col-lg-9">
            <div class="col-lg-6">
              <div class="type col-log-6">
                <p>Desde:</p>
                <p>Hasta:</p>
                <p>Peso: </p>
              </div>
              <div class="col-log-6">
               <p><?php echo $contrato->fkcontract0 ? $load->load_cityorigin:$load->origen; ?></p>
               <p><?php echo $contrato->fkcontract0 ? $load->load_citydestination:$load->destino; ?></p>
               <p><?php 
               if($contrato->fkcontract0)
               {
                echo $load->load_weight ? $load->load_weight . " Kg" : ($load->load_weight / 1000) . " Ton." ;
              }
              else
              {
                echo $load->weight ? $load->weight . " Kg" : ($load->weight / 1000) . " Ton." ;
              }
             ?></p> 
              </div>
            </div>
            <div class="col-lg-6">
              <div class="type col-log-6">
                <p style="height:50px;">Observación:</p>
              </div>
              <div class="col-log-6">
               <p><?php echo $contrato->fkcontract0 ? $load->load_comment:$load->comment; ?></p> 
              </div>
            </div>
          </div>
        </div>
        <!-- End Block ---------------------------------------------------------->
        <?php } 
        }
        else
        {
          echo '<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
          echo '<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
          echo '</h1></div></div>';
        }
        ?>
      </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>
<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/buttonsearch.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>

<script>
 var my_data = [];
  $('.contract-name').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    $('.block').each(function(index,value){
      $(this).show();
    });
    event.preventDefault();
    $('.block').parent().find('.not-found').remove();
    var text = $('#inputSearch').val();
    if(text)
    {
      $('.block').each(function(index,value){
        var bool = false;
        ($(value).find('.contract-name').text()===text) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=true;
    $('.block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool)
    {
      var text ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      text+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      text+='</h1></div></div>';
      $('.block').parent().append(text);
    }

  });
</script>
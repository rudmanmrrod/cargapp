<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 <?php echo $option == 3 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 <?php echo $option == 4 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 <?php echo $option == 2 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row">
      <!-- left side -->
      <?php $form = ActiveForm::begin([
        'method' => 'post',
        'id' => 'new-admin',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'options' => ['enctype'=>'multipart/form-data']
      ]);
      ?>
        <div class="col-sm-4 administration-left-side-wrapper">
          <div class="col-xs-12 administration-side administration-left-side new-user-za">

            <h1>Crear administrador</h1>
            <?= $form->field($model, "name")->input("text", ['placeholder' => 'Nombre', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "phone")->input("number", ['placeholder' => 'Teléfono Móvil', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "email")->input("email", ['placeholder' => 'Correo', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "user")->input("text", ['placeholder' => 'Usuario', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "password_repeat")->input("password", ['placeholder' => 'Confirmar Contraseña', 'class' => 'administration-input'])->label(false); ?>
            <div class="photo-wrapper col-xs-12">

            <div class="profile-photo">
              <div class="photo-area">
                <div class="col-md-6">
                    <p>Foto Perfil</p>
                </div>
                <div class="fileUpload col-md-6"> 
                  <?= $form->field($model, "avatar")->fileInput(['class' => 'upload'])->label(false); ?>
                </div>
              </div>
            </div>

            </div>
          </div>
          <div class="col-xs-12 administration-submit">
            <button type="submit" class="btn btn-active col-xs-12">CREAR NUEVO</button>
          </div>
        </div>
        <?php $model->type = '1'; ?>
      <?php $form->end() ?>
      <!-- END left side -->

      <!-- right side -->
      <div class="col-sm-8 administration-right-side">
        <div class="row administration-side administration-right-side-title-wrapper">
          <h1>Administradores activos</h1>
        </div>
        
        <?php foreach ($users as $l) {?>
          <?php $u = $l->getMcTusers()->One(); ?>
          <div class="row administration-side administration-side-activity">
            <div class="col-sm-9 col-xs-12 administration-activity-info">
              <div class="row">
                <div class="col-xs-3">
                  <img src="<?php if($u['user_avatar']){echo Yii::getAlias('@web'). $u['user_avatar'];}
                  else {echo Yii::getAlias('@web').'/img/pp.png';} ?>">
                </div>
                <div class="col-xs-9">
                  <ul class="list-unstyled">
                    <li><?php echo $u['user_name']; ?></li>
                    <li class="red"><?php echo $u['user_phone']; ?></li>
                    <li><?php echo $l->email2; ?></li>
                    <li class="green"><?php echo $l->email; ?></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12 administration-activity-buttons">
              <div class="row">
                <div class="col-xs-12">
                  <?= Html::a('HISTORIAL', ['historylog', 'id' => $l->id], ['class' => 'btn btn-desactive btn-blue col-xs-12']) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?= Html::a('EDITAR', ['update', 'id' => $l->id], ['class' => 'btn btn-desactive btn-green col-xs-12']) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?php 
                  if($l->activate==1)
                  {
                    echo Html::a('INHABILITAR', ['inhabilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,deactivateuser,'. $l->id.',this,"a","href")',]);
                  }
                  else
                  {
                    echo Html::a('HABILITAR', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,activateuser,'. $l->id.',this,"a","href")',]);
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <!-- END right side -->
    </div>
  </div>
</div>
<?php 
Modal::begin([
  'id' => 'deactivateuser',
  'header' => '<h3>¿Deseas inhabilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['inhabilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();

Modal::begin([
  'id' => 'activateuser',
  'header' => '<h3>¿Deseas habilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>

<script>
$(document).ready(function() {  
    var base64 = "";
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "105px 105px");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
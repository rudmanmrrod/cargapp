<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\McTlogin;
use app\models\McTsubcategory;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 <?php echo $option == 3 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 <?php echo $option == 4 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 <?php echo $option == 2 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row">
      <!-- left side -->
      <?php $form = ActiveForm::begin([
        'method' => 'post',
        'id' => 'new-company',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'options' => ['enctype'=>'multipart/form-data']
      ]);
      ?>
        <?php $listBanks=ArrayHelper::map($listBanks,'pkbank','bank_name'); ?>
        <?php $listaccounts = ['1'=>'Corriente', '2'=>'Ahorro']; ?>
        <div class="col-sm-4 administration-left-side-wrapper">
          <div class="col-xs-12 administration-side administration-left-side new-user-za">
            <h1>Crear Empresa</h1>
            <?= $form->field($model, "rz")->input("text", ['placeholder' => 'Razón Social', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "nit")->input("text", ['placeholder' => 'NIT', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "name")->input("text", ['placeholder' => 'Nombre Encargado', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "address")->input("text", ['placeholder' => 'Dirección', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "phone")->input("text", ['placeholder' => 'Teléfono', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "email")->input("email", ['placeholder' => 'Correo', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "user")->input("text", ['placeholder' => 'Usuario', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "password_repeat")->input("password", ['placeholder' => 'Confirmar Contraseña', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, 'review')->textarea(['placeholder' => 'Reseña', 'class' => 'administration-input', 'rows'=> '4'])->label(false); ?>
            <button type="button" class="btn btn-active col-xs-12 mt7 mb10">Añadir Categorias</button>
            <?= $form->field($model, 'mcTsubcategories[]')->checkboxList(ArrayHelper::map($mc_tsubcategory, 'pktcategory_subcategory', 'fksubcategory'),['class' => 'administration-input chk', 'rows'=> '4','hidden' => true])->label(false); ?>
            <?= $form->field($model, 'mcTpercent')->input("text", ['class' => 'administration-input', 'hidden'=>'true'])->label(false); ?>
            <h1>Añadir opciones de pago</h1>
            <?= $form->field($model, 'bank')->dropDownList($listBanks, ['prompt'=>'Escoja su Banco', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, 'typeaccount')->dropDownList($listaccounts, ['prompt'=>'Seleccione (Ahorros/Corriente)', 'class' => 'administration-input'])->label(false); ?>
            <?= $form->field($model, "numberaccount")->input("number", ['placeholder' => 'No. Cuenta', 'class' => 'administration-input'])->label(false) ?>   
            <?= $form->field($model, "nameaccount")->input("text", ['placeholder' => 'Titular de la cuenta', 'class' => 'administration-input'])->label(false) ?>   
            
            <div class="profile-photo">
              <div class="photo-area">
                <div class="col-md-6">
                    <p>Foto Perfil</p>
                </div>
                <div class="fileUpload col-md-6"> 
                  <?= $form->field($model, "avatar")->fileInput(['class' => 'upload'])->label(false); ?>
                </div>
              </div>
            </div>


          </div>

          <div class="col-xs-12 administration-submit">
            <button type="submit" class="btn btn-active col-xs-12">CREAR NUEVO</button>
          </div>
        </div>
        <?php $model->type = '1'; ?>
      <?php $form->end() ?>
      <!-- END left side -->

      <!-- right side -->
      <div class="col-sm-8 administration-right-side">
        <div class="row administration-side administration-right-side-title-wrapper">
          <h1>Empresas activas</h1>
        </div>
        <?php foreach ($comps as $c) {?>
          <?php $l = $c->getFklogin0()->One(); ?>
          <div class="row administration-side administration-side-activity">
            <div class="col-sm-9 col-xs-12 administration-activity-info">
              <div class="row">
                <div class="col-xs-3">
                  <img src="<?php if($c->company_avatar){echo Yii::getAlias('@web'). $c->company_avatar;}
                  else {echo Yii::getAlias('@web').'/img/pp.png';} ?>">
                </div>
                <div class="col-xs-9">
                  <ul class="list-unstyled">
                    <li><?php echo $c->company_attendant; ?></li>
                    <li class="blue"><?php echo $c->company_rz; ?></li>
                    <li class="red"><?php echo $c->company_phone; ?></li>
                    <li><?php echo $l->email2; ?></li>
                    <li class="green"><?php echo $l->email; ?></li>
                    <li><?php echo $c->company_address; ?></li>
                    <li>NIT: <?php echo $c->company_nit; ?></li>
                    <li class="blue">Opciones de pago:</li>
                    <li class="blue"><?php echo $c->getFkbank0()->One()->bank_name; ?></li>
                    <li>Cuenta <?php echo $c->company_accounttype == 1 ? "Corriente" : "Ahorro"; ?></li>
                    <li><?php echo $c->company_accountnumber; ?></li>
                    <li><?php echo $c->company_accountname; ?></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12 administration-activity-buttons">
              <div class="row">
                <div class="col-xs-12">
                    <?= Html::a('EDITAR', ['updatecompany', 'id' => $c->pkcompany,  'id_login' => $l->id], ['class' => 'btn btn-desactive btn-green col-xs-12']) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?php 
                  if($l->activate==1)
                  {
                    echo Html::a('INHABILITAR', ['inhabilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,deactivateuser,'. $l->id.',this,"a","href")',]);
                  }
                  else
                  {
                    echo Html::a('HABILITAR', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,activateuser,'. $l->id.',this,"a","href")',]);
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <!-- END right side -->
    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content mdl">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h1 class="modal-title tt1">Tarifas de Empresa Transportadora</h1></center>
      </div>
      <div class="modal-body mdl-body">
        <?php foreach ($cats as $c) {?>
          <?php $subs = $c->getMcTcategorySubcategories()->all(); 
          if($subs){?>
          <h5 class="cat5"><?php echo '<b>'.$c->category_name.'</b>'; ?></h5>
          <?php foreach ($subs as $s) {?>
            <div class="row">
              <div class="col-md-1 col-xs-1 col-lg-1 col-sm-1">
                <input type="checkbox" class="cpercent" name="<?php echo $s->pktcategory_subcategory;?>">
              </div>
              <div class="col-md-3 col-xs-5 col-lg-3 col-sm-3 fcc">
                <?php echo $s->fksubcategory0->subcategory_name; ?>
              </div>
              <div class="col-md-8 col-xs-2 col-lg-8 col-sm-8">
                <input disabled type="number" class="tpercent" name="<?php echo $s->pktcategory_subcategory;?>"> %
              </div>
            </div>
          <?php }
         }?>    
        <?php }?>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-desactive btn-green modal-submit">AGREGAR</button>
        </center>
      </div>
    </div>

  </div>
</div>

<?php 
Modal::begin([
  'id' => 'deactivateuser',
  'header' => '<h3>¿Deseas inhabilitar/habilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['inhabilitar', 'id' => $l->id, 'perfil' => '3' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();

Modal::begin([
  'id' => 'activateuser',
  'header' => '<h3>¿Deseas habilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>

<script type="text/javascript">
$(document).ready(function() { 
    jQuery.noConflict(); 
    $(".cpercent").change(function(){
      if($(this).prop('checked')) {
        var tpercent = $(this).parent().parent().find('.tpercent');
        tpercent.attr('disabled',false);
      }else
      {
        var tpercent = $(this).parent().parent().find('.tpercent');
        tpercent.val("");
        tpercent.attr('disabled',true);
      }
    });

    $('.modal-submit').click(function(){
      var bool = comprobar();
      if(bool){
        for(var i=0;i<$('.cpercent').length;i++)
        {
          var inner_chk = $('.chk').find('input:checkbox');
          for(var j=0;j<inner_chk.length;j++)
          {
            cpercent = $('.cpercent')[i];
            var chk = inner_chk[j]
            if($(cpercent).attr('name')==$(chk).val())
            {
              if(cpercent.checked)
              {
                $(chk).attr('checked',true);
                chk.checked = true;
                addTpercent();
              }
              else
              {
                $(chk).removeAttr('checked');
                chk.checked = false;
                addTpercent();
              }
            }
          }
        }
      }
      else
      {
        alert('Falta rellernar el porcentaje de algun campo seleccionado');
      }
      $('#myModal').modal('hide');
    });

    $('.mb10').click(function(){
      $('#myModal').modal();
    });

    function addTpercent()
    {
      var tpercent = $('#myModal').find('.tpercent');
      var text = ''
      $(tpercent).each(function(index,value){
        $(value).val() != '' ? text+=$(value).val()+";":'';
      });
      $('#formregistercompany-mctpercent').val(text);
    }

    function comprobar()
    {
      var row = $('#myModal').find('.row');
      var bool = false;
      $(row).each(function(index, value){
        var percent = $(value).find('.cpercent');
        var input = $(value).find('.tpercent');  
        if(percent[0].checked){
          var condicion = percent[0].checked && $(input).val()!='';
          condicion ? bool = true:bool = false;
        }
      });
      return bool;
    }

    var base64 = "";
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "105px 105px");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
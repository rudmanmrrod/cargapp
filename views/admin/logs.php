<?php
use yii\helpers\Html;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 btn-<?php echo $rol==1 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 btn-<?php echo $rol==4 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 btn-<?php echo $rol==2 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>


<div class="col-sm-4"></div>
<div class="col-sm-4 administration-left-side-wrapper">
  <div class="col-xs-12 administration-side administration-left-side new-user-za">
    <h1>Historial de <?php echo $user->user_name; ?></h1><br>
    <?php if(count($logs)>0){
      foreach ($logs as $log) {
        echo '<div class="info col-lg-12">';
        echo '<p>'.$log->message.'</p>'; 
        echo '<p><b>Fecha: </b>'.date('d/m/Y H:m:s',strtotime($log->date)).'</p>'; 
        echo '</div>';
      }
      ?>

    <?php }
    else {?>
      <div class="row not-found" style="background-color: #fff;">
        <div class="page-header">
          <h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small></h1>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<div class="col-sm-4"></div>
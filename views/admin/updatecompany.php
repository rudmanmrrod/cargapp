<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 btn-active col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>

<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<!-- left side -->
<?php $form = ActiveForm::begin([
  'method' => 'post',
  'id' => 'new-company',
  'enableClientValidation' => true,
  'action' => ['admin/updatecompany', 'id' => $id, 'id_login' => $id_login],
  'enableAjaxValidation' => true,
  'options' => ['enctype'=>'multipart/form-data']
]);
?>
  <?php $listaccounts = ['1'=>'Corriente', '2'=>'Ahorro']; ?>
  <div class="col-sm-4"></div>
  <div class="col-sm-4 administration-left-side-wrapper">
    <div class="col-xs-12 administration-side administration-left-side new-user-za">
      <h1>Editar Empresa</h1>
      <?= $form->field($model, "rz")->input("text", ['placeholder' => 'Razón Social', 'class' => 'administration-input'])->label('Razón Social') ?>   
      <?= $form->field($model, "nit")->input("text", ['placeholder' => 'NIT', 'class' => 'administration-input'])->label('NIT') ?>   
      <?= $form->field($model, "name")->input("text", ['placeholder' => 'Nombre del Encargado', 'class' => 'administration-input'])->label('Nombre del Encargado') ?>   
      <?= $form->field($model, "address")->input("text", ['placeholder' => 'Dirección', 'class' => 'administration-input'])->label('Dirección') ?>   
      <?= $form->field($model, "phone")->input("text", ['placeholder' => 'Teléfono', 'class' => 'administration-input'])->label('Teléfono') ?>   
      <?= $form->field($model, "email")->input("email", ['placeholder' => 'Correo', 'class' => 'administration-input'])->label('Correo'); ?>
      <?= $form->field($model, "user")->input("text", ['placeholder' => 'Usuario', 'class' => 'administration-input'])->label('Usuario'); ?>
      <?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña Original', 'class' => 'administration-input'])->label('Contraseña Original'); ?>
      <?= $form->field($model, "new_password")->input("password", ['placeholder' => 'Nueva Contraseña', 'class' => 'administration-input'])->label('Nueva Contraseña'); ?>
      <?= $form->field($model, "new_password_repeat")->input("password", ['placeholder' => 'Confirmar Nueva Contraseña', 'class' => 'administration-input'])->label('Confirmar Nueva Contraseña'); ?>
      <?= $form->field($model, 'review')->textarea(['placeholder' => 'Reseña', 'class' => 'administration-input', 'rows'=> '4'])->label('Reseña'); ?>
      <button type="button" class="btn btn-active col-xs-12 mt7 mb10" data-toggle="modal" data-target="#myModal">Mis Tarifas</button>
      <?= $form->field($model, 'mcTsubcategories[]')->checkboxList(ArrayHelper::map($mc_tsubcategory, 'pktcategory_subcategory', 'fksubcategory'),['class' => 'administration-input chk', 'rows'=> '4', 'hidden' => true])->label(false); ?>
      <?= $form->field($model, 'mcTpercent')->input("text", ['class' => 'administration-input', 'hidden' => true])->label(false); ?>
      <h1>Añadir opciones de pago</h1>
      <?= $form->field($model, 'bank')->dropDownList(ArrayHelper::map($listBanks,'pkbank','bank_name'), ['prompt'=>'Escoja su Banco', 'class' => 'administration-input'])->label('Banco'); ?>
      <?= $form->field($model, 'typeaccount')->dropDownList($listaccounts, ['prompt'=>'Seleccione (Ahorros/Corriente)', 'class' => 'administration-input'])->label('Tipo de Cuenta'); ?>
      <?= $form->field($model, "numberaccount")->input("number", ['placeholder' => 'Nro. Cuenta', 'class' => 'administration-input'])->label('Nro. de Cuenta') ?>   
      <?= $form->field($model, "nameaccount")->input("text", ['placeholder' => 'Titular de la cuenta', 'class' => 'administration-input'])->label('Titular de la cuenta') ?>   
      
      <div class="profile-photo">
        <div class="photo-area">
          <div class="col-md-6">
              <p>Foto Perfil</p>
          </div>
          <div class="fileUpload col-md-6" <?php if($model->avatar){?>style = "background-image: url(<?php echo Yii::getAlias('@web').$model->avatar;?>);
          background-size: 105px 105px" <?php } ?> >
            <?= $form->field($model, "avatar")->fileInput(['class' => 'upload'])->label(false); ?>
          </div>
        </div>
      </div>


    </div>
    <div class="col-xs-12 administration-submit">
      <?= Html::submitButton('EDITAR', ['class' => 'btn btn-active col-xs-12 submit']) ?>
  </div>
  <?php $model->type = '1'; ?>
<?php $form->end() ?>
</div>
<div class="col-sm-4"></div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content mdl">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h1 class="modal-title tt1">Tarifas de Empresa Transportadora</h1></center>
      </div>
      <div class="modal-body mdl-body">
        <?php foreach ($categories as $c) {?>
          <?php $subs = $c->getMcTcategorySubcategories()->all(); 
          if($subs){?>
          <h5 class="cat5"><?php echo '<b>'.$c->category_name.'</b>'; ?></h5>
          <?php foreach ($subs as $s) {?>
            <div class="row">
              <div class="col-md-1 col-xs-1 col-lg-1 col-sm-1">
                <input type="checkbox" class="cpercent" name="<?php echo $s->pktcategory_subcategory;?>">
              </div>
              <div class="col-md-3 col-xs-5 col-lg-3 col-sm-3 fcc">
                <?php echo $s->fksubcategory0->subcategory_name; ?>
              </div>
              <div class="col-md-8 col-xs-2 col-lg-8 col-sm-8">
                <input disabled type="number" class="tpercent" name="<?php echo $s->pktcategory_subcategory;?>"> %
              </div>
            </div>
          <?php }
          }?>    
        <?php }?>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-desactive btn-green modal-submit">AGREGAR</button>
        </center>
      </div>
    </div>

  </div>
</div>

<?php 
$this->registerJs("var services = new Array(); services = ".json_encode($services).";", \yii\web\View::POS_END, 'services'); ?>
<script>
  $(document).ready(function(){
      jQuery.noConflict();
      var base64 = "";
      $(".upload").change(function(event) {
        $.each(event.target.files, function(index, file) {
          var reader = new FileReader();
          reader.onload = function(event) {  
            object = {};
            object.filename = file.name;
            object.data = event.target.result;
            base64 = object.data;
            $(".fileUpload").css("background-size", "105px 100%");
            $(".fileUpload").css("background-image", "url(" + base64 + ")");
          };  
          reader.readAsDataURL(file);
        });
      });
      
      $(".cpercent").change(function(){
        if($(this).prop('checked')) {
          var tpercent = $(this).parent().parent().find('.tpercent');
          tpercent.attr('disabled',false);
        }else
        {
          var tpercent = $(this).parent().parent().find('.tpercent');
          tpercent.val("");
          tpercent.attr('disabled',true);
        }
      });
      updatefield();

      $('.modal-submit').click(function(){
        var bool = comprobar();
        if(bool){
          for(var i=0;i<$('.cpercent').length;i++)
          {
            var inner_chk = $('.chk').find('input:checkbox');
            for(var j=0;j<inner_chk.length;j++)
            {
              cpercent = $('.cpercent')[i];
              var chk = inner_chk[j]
              if($(cpercent).attr('name')==$(chk).val())
              {
                if(cpercent.checked)
                {
                  $(chk).attr('checked',true);
                  chk.checked = true;
                  addTpercent();
                }
                else
                {
                  $(chk).removeAttr('checked');
                  chk.checked = false;
                  addTpercent();
                }
              }
            }
          }
        }
        else
        {
          alert('Falta rellernar el porcentaje de algun campo seleccionado');
        }
        $('#myModal').modal('hide');
    });

    function addTpercent()
    {
      var tpercent = $('#myModal').find('.tpercent');
      var text = ''
      $(tpercent).each(function(index,value){
        $(value).val() != '' ? text+=$(value).val()+";":'';
      });
      $('#formchangepasswordcompany-mctpercent').val(text);
    }

    function comprobar()
    {
      var row = $('#myModal').find('.row');
      var bool = false;
      $(row).each(function(index, value){
        var percent = $(value).find('.cpercent');
        var input = $(value).find('.tpercent');  
        if(percent[0].checked){
          var condicion = percent[0].checked && $(input).val()!='';
          condicion ? bool = true:bool = false;
        }
      });
      return bool;
    }

    function updatefield()
    {
      var text = ''
      $(services).each(function(index,value){
        var input = $('#formchangepasswordcompany-mctsubcategories').find('input:checkbox');
        $(input).each(function(index2,value2){
          if($(value2).val()==value.fksubcategory)
          {
            $(value2).attr('checked',true);
            value2.checked = true;
            text += value.companyservices_percent+";";
          }
        });
        var modal = $('#myModal').find('input:checkbox');
        $(modal).each(function(index2,value2){
          if($(value2).attr('name')==value.fksubcategory)
          {
            $(value2).attr('checked',true);
            value2.checked = true;
            var tpercent = $(value2).parent().parent().find('.tpercent');
            $(tpercent).val(value.companyservices_percent);
            $(tpercent).attr('disabled',false);
          }
        });
      });
      $('#formchangepasswordcompany-mctpercent').val(text);
    }
  });

</script>
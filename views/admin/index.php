<?php
use app\models\McTcategory;
use app\models\McTproposal;
use app\models\McTlogin;
use app\models\McTcompanyservices;
use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/index" class="btn btn-sub2 btn-active col-md-3 col-xs-12 col-sm-4 col-lg-3">USUARIO/EMPRESA</a>
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/index?type=2" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-4 col-lg-3">EMPRESA/TRANSPORTADOR</a>
        </div>
        <!--end head-->

        <!-- parts -->
        <div class="body-main col-lg-12">
          <div class="col-lg-4 col-sm-12">
            <?= Html::a('ACTIVOS', ['index', 'contract' => '1'], ['class' => $contract == 1 ? 'btn btn-active btn-sub2 col-md-5 col-xs-5 top-button' : 'btn btn-desactive btn-sub2 col-md-5 col-xs-5 top-button']) ?>
            <?= Html::a('FINALIZADOS', ['index', 'contract' => '3'], ['class' => $contract == 3 ? 'btn btn-active btn-sub2 col-md-5 col-xs-5 top-button' : 'btn btn-desactive btn-sub2 col-md-5 col-xs-5 top-button']) ?>
          </div>
          <div class="part2 col-lg-4 col-xs-12">
            <span class="col-md-2 col-xs-2">Filter por:</span>
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Abonado">ABONADOS</button>
            <button type="submit" class="btn btn-desactive col-md-5 col-xs-5 top-button srch" id="Pagado">PAGADOS</button>
          </div>
          <div class="part3 col-lg-4 col-xs-12 ">
            <div class="form-group col-md-8 col-xs-7">
              <input class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
            </div>
            <button type="submit" class="btn btn-primary col-md-4 col-xs-5 submit">BUSCAR</button>
          </div>
        </div>
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
            $admin = McTlogin::find()->where(['role'=>'1'])->one()->getMcTusers()->one();
            if($contratos){
            foreach ($contratos as $contrato) {
                $load = $contrato->getFkCarga()->One();
                $loadpayment = $load->getMcTpaymentloads()->one();
                $user = $load->getFkuser0()->One(); 
                $company = $contrato->getFkuserCompany()->One();
                $comp_profile = $company->getMcTcompanies()->One();
                $proposal = McTproposal::find()->where(['fkload'=>$load->pkload,'fkcompany'=>$comp_profile->pkcompany])->one();
                $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
                $service = McTcompanyservices::find()
                    ->where("fkcompany=:id", [":id" => $comp_profile->pkcompany])
                    ->andWhere('fksubcategory = :sub', [':sub' => $load->getFksubcategory0()->One()->pktcategory_subcategory])
                    ->one();
            ?>
          <div class="block row col-lg-12">
            <div class="info col-lg-9">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-1">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $user->user_name; ?></h1>
                    <div class="type col-log-6">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Categoria:</p>
                      <p>Sub-categ:</p>
                      <p>Asegura:</p>
                    </div>
                    <div class="col-log-6">
                     <p class="blue"><?php echo $user->fklogin0->email;?></p>
                     <p class="blue"><?php echo $user->user_phone; ?></p>
                     <p class="green"><?php echo $catg->category_name;?></p>
                     <p class="green"><?php echo $load->getFksubcategory0()->One()->getFksubcategory0()->One()->subcategory_name;?></p>
                     <p class="red"><?php echo $load->load_ensure. ' COP';?></p>  
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-2">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $comp_profile->company_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="types user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $comp_profile->company_rz ;?></h1>
                    <p><?php echo $comp_profile->company_attendant ;?></p>
                    <div class="type col-log-7">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Costo:</p>
                      <p>Pago Min:</p>
                    </div>
                    <div class="col-log-5">
                     <p class="blue"><?php echo $company->email2;?></p>
                     <p class="blue"><?php echo $comp_profile->company_phone;?></p>
                     <p class="green"><?php echo $proposal->proposal_total. ' COP';?></p>
                     <p class="red"><?php echo $proposal->proposal_paymin. ' COP';?></p> 
                    </div>
                  </div>
              </div>
            </div>
          </div><!-- /.info -->
          <?php 
            $zeros = '000000';
            $contract_type = ''.$contrato->pk_contract;
            $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
          ?>
          <div class="right-part col-lg-3">
            <p>Contrato : <span class="blue contract-name"><?php echo 'UE'.$contract_number;?></span></p>
            <div class="type col-log-6">
              <p>Fecha orden:</p>
              <p>Encargado:</p>
              <p>Estado Pago:</p>
              <p>Comison: </p>
            </div>
            <div class="col-log-6">
             <p style="font-weight:bolder"><?php echo date('d/m/Y',strtotime($load->load_create));?></p>
             <p class="blue"><?php echo $admin->user_name; ?></p>
             <p class="red status"><?php echo $loadpayment->fkstatus0->status_name;?></p>
             <p class="green"><?php echo ($proposal->proposal_total*($service->companyservices_percent/100)). ' COP';?></p> 
            </div>
          </div><!-- /.right-part -->

          <div class="clearfix"></div>
          <!--info2 -->
          <div class="info2 col-lg-9">
            <div class="col-lg-6">
              <div class="type col-log-6">
                <p>Desde:</p>
                <p>Hasta:</p>
                <p>Tipo:</p>
                <p>Peso: </p>
              </div>
              <div class="col-log-6">
               <p><?php echo $load->load_cityorigin; ?></p>
               <p><?php echo $load->load_citydestination; ?></p>
               <p>Pendiente</p>
               <p><?php echo $load->load_weight ? $load->load_weight . " Kg" : ($load->load_weight / 1000) . " Ton." ;  ?></p> 
              </div>
            </div>
            <div class="col-lg-6">
              <div class="type col-log-6">
                <p>Dimensiones:</p>
                <p style="height:50px;">Observación:</p>
              </div>
              <div class="col-log-6">
               <p><?php echo $load->load_dimensions; ?> metros</p>
               <p><?php echo $load->load_comment; ?></p> 
              </div>
            </div>
          </div>
          <?php
          if ($load->fkstatus==14)
          {
            echo '<div class="col-lg-3 col-xs-12">';
            echo '<button type="button" class="btn btn-danger elimn col-lg-7 col-lg-offset-3 col-xs-12">CANCELADO</button>';
            echo '</div>';
          }
        echo '</div>';
         } 
        }
        else
        {
          echo '<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
          echo '<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
          echo '</h1></div></div>';
        }
        ?>
      </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>
<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/buttonsearch.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>

<script>
 var my_data = [];
  $('.contract-name').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    $('.block').each(function(index,value){
      $(this).show();
    });
    event.preventDefault();
    $('.block').parent().find('.not-found').remove();
    var text = $('#inputSearch').val();
    if(text)
    {
      $('.block').each(function(index,value){
        var bool = false;
        ($(value).find('.contract-name').text()===text) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
        $('.block').each(function(index,value){
          $(this).show();
      });
    }
    var bool=true;
    $('.block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool)
    {
      var text ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      text+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      text+='</h1></div></div>';
      $('.block').parent().append(text);
    }
  });
 
</script>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="row administration-left-side-wrapper"> 
  
  <div class="col-sm-4"></div>
  <div class="col-sm-4 administration-side administration-left-side new-user-za">
    <h3>Editar Noticia</h3>
  </div>
  <div class="col-sm-4"></div>
  
</div>
  

<div class="col-sm-4"></div>
<div class="col-sm-4 administration-left-side-wrapper">
  <div class="col-xs-12 administration-side administration-left-side new-user-za">
    <?php $form = ActiveForm::begin([
      'method' => 'post',
      'enableClientValidation' => true,
      'enableAjaxValidation' => true,
      'action' => ['admin/noticiasedit', 'id' => $id],
      'options' => ['enctype'=>'multipart/form-data']
    ]);
    ?>
      <div class="col-md-12">
        <?= $form->field($model, "title")->input("text", ['placeholder' => 'TÍTULO', 'class' => 'itm_not'])->label("Título") ?>   
      </div>
      <div class="col-md-12 mt20">
        <?= $form->field($model, 'message')->textarea(['placeholder' => 'TEXTO', 'class' => 'itm_not', 'rows'=> '10'])->label("Mensaje"); ?>
      </div>
      <?= $form->field($model, "type")->input("hidden")->label(false);?>
      <div class="col-md-12 col-lg-12">
        <div class="profile-photo wd100">
          <label class="control-label">Imagen</label>
          <div class="photo-area p2 wd100">
            <div class="fileUpload wd100" <?php if($model->image){?>style = "background-image: url(<?php echo Yii::getAlias('@web').$model->image;?>);
              background-size: 270px 120px" <?php } ?>>
              <?= $form->field($model, "image")->fileInput(['class' => 'tm_not upload upload2'])->label(false); ?>
            </div>
          </div>
        </div>
      </div><br><br>
      <div class="col-xs-12 administration-submit">
          <?= Html::submitButton('EDITAR', ['class' => 'btn btn-active btn-green col-xs-12 submit pub']) ?>
      </div>
    <?php $form->end() ?>
  
  </div>
</div>
<div class="col-sm-4"></div>



<script type="text/javascript">
$(document).ready(function() { 
    var base64 = "";
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "270px 120px");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
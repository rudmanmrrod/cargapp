<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\McTlogin;
use app\models\McTloadproposal;
use app\models\McUloadproposal;
use app\models\FormAdminTransporterPayment;
?>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main col-lg-12">
          <?= Html::a('USUARIO/EMPRESA', ['cargas'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('TRANSPORTADORES', ['cargast'], ['class' => 'btn btn-sub2 btn-active col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('GARANTIAS', ['cargasg'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
        </div>
        <!--end head-->

        <!-- parts -->
        <div class="row body-main col-lg-12 part3">
          <div class="col-lg-2">
              <div class="col-md-8 col-md-offset-4">  
                Filtrar por:
              </div>
          </div>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch" id="ENVIADO">ENVIADOS</button>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch" id="OK">PAGADOS</button>
            
          <div class="col-lg-4 col-xs-12 pull-right ">
              <div class="form-group col-md-8 col-xs-7">
                <input type="text" class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
              </div>
              <button type="submit" class="btn btn-primary col-md-4 col-xs-5 submit">BUSCAR</button>
          </div>
        </div>
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
            $admin = McTlogin::find()->where(['role'=>'1'])->one()->getMcTusers()->one();
            if($contratos){
            foreach ($contratos as $contrato) {
                $user = $contrato->getFkuser0()->one();
                $perfil = $user->getMcTusers()->one();
                $vehicle = $perfil->getMcTvehicles()->one();
                $payment = $contrato->getMcTtransporterpayments()->one();
                if($contrato->fkcontract0)
                {
                  $proposal = McUloadproposal::find()
                  ->where(['fktransportador'=>$user->id,'fkcontract'=>$contrato->fkcontract0->pk_contract])->one();
                }
                else
                {
                  $proposal = McTloadproposal::find()
                  ->where(['fktransportador'=>$user->id,'fkcotizacionemp'=>$contrato->fkcotizaremp0->pkcotizaremp])->one();
                }
          ?>
          <div class="block row col-lg-12">
            <div class="info col-lg-12">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info-1" style="">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $perfil->user_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $perfil->user_name; ?></h1>
                    <div class="type col-log-6">
                      <p>Placa:</p>
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                    </div>
                    <div class="col-log-6">
                     <p class="red"><?php echo $vehicle->vehicle_licenceplate;?></p>
                     <p class="blue"><?php echo $user->email;?></p>
                     <p class="blue"><?php echo $perfil->user_phone; ?></p>
                    </div>
                  </div>
                </div>
                <?php 
                  $zeros = '000000';
                  $contract_type = ''.$contrato->pk_contract;
                  $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info-2">
                  <div style="padding-left:5px; background-color: #FFF;" class="types user-info-wrapper">
                    <h1>
                      CONTRATO: <span class="blue contract"><?php echo 'ET'.$contract_number;?></span>
                    </h1>
                    <div class="type col-log-6">
                      <p>Fecha Orden:</p>
                      <p>Encargado:</p>
                      <p>Pago Carga:</p>
                      <p>Estado Carga: </p>
                    </div>
                    <div class="col-log-6">
                     <p class=""><?php echo date('d/m/Y',strtotime($contrato->signature_time));?></p>
                     <p class="blue"><?php echo $admin->user_name; ?>.</p>
                     <p class="green"><?php echo $proposal->payment. 'COP';?></p>
                     <p class="blue"> Sin Recojer</p> 
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info-3">
                  <div class="form-group text-center">
                  
                  <?php
                    if($payment)
                    { 
                      if($payment->fkstatus==9)
                      {
                        echo Html::textInput('reference',$payment->reference,['class' => 'administration-input','style'=>'background-color:white;','disabled'=>true]);
                        echo Html::textInput('value',$payment->value,['class' => 'administration-input','style'=>'background-color:white;','disabled'=>true]);
                        echo '<button type="submit" class="btn btn-primary btn-desactive col-md-6 status">ENVIADO</button>';
                        echo Html::a('PAGADO', ['transportermakepaid','id'=>$payment->pktpayment], ['class' => 'btn btn-sub2 btn-active col-md-6']);
                      }
                      else if ($payment->fkstatus==10) 
                      {
                         echo Html::textInput('reference',$payment->reference,['class' => 'administration-input','style'=>'background-color:white;','disabled'=>true]);
                        echo Html::textInput('value',$payment->value,['class' => 'administration-input','style'=>'background-color:white;','disabled'=>true]);
                        echo '<button type="submit" class="btn btn-primary btn-desactive col-md-6">ENVIADO</button>';
                        echo '<button type="submit" class="btn btn-primary btn-desactive col-md-6 status">OK</button>';
                      }
                    }
                    else
                    {
                      $form = ActiveForm::begin([
                        'method' => 'post',
                        'enableClientValidation' => true,
                        'action' => ['transporterpayment','id'=>$contrato->pk_contract],
                      ]);

                        $payment = new FormAdminTransporterPayment;
                        echo $form->field($payment, "reference")->input("text", ['placeholder' => 'Escriba Referencia', 'class' => 'administration-input','style'=>'background-color:white;'])->label(false);   
                        echo $form->field($payment, "value")->input("number", ['placeholder' => 'Escriba Valor', 'class' => 'administration-input','style'=>'background-color:white;'])->label(false);
                        echo '<button type="submit" class="btn btn-primary submit col-md-6 col-md-push-3">ENVIAR</button>';

                      $form->end(); 
                     }
                   ?>
                  </div>
                </div>
            </div><!-- END ROW-->
          </div><!-- /.info -->
          
        </div>
        <!-- End Block ---------------------------------------------------------->
        <?php } 
        }
        else
        {
          echo '<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
          echo '<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
          echo '</h1></div></div>';
        }
        ?>
        
      </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>
<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/buttonsearch.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>

<script>
 var my_data = [];
  $('.contract').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    $('.block').each(function(index,value){
      $(this).show();
    });
    var text = $('#inputSearch').val();
    $('.block').parent().find('.not-found').remove();
    if(text)
    {
      $('.block').each(function(index,value){
        var bool = false;
        var m = $(value).find('.contract').text()===text;
        (m===true) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=true;
    $('.block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool && text)
    {
      var txt ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      txt+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      txt+='</h1></div></div>';
      $('.block').parent().append(txt);
    }
  });
</script>
<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 <?php echo $option == 3 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 <?php echo $option == 4 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 <?php echo $option == 2 ? 'btn-active' : 'btn-desactive'; ?> col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>
<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
<div class="content">
  <div class="container">
    <div class="row"> 
      <div class="col-sm-12">
        <div class="row administration-side administration-right-side-title-wrapper block-border">
          <div class="part3 col-lg-5 col-xs-12 right-side">
            <form>
              <div class="col-md-2  form-title">Buscar:</div>
              <div class="form-group col-md-6 col-xs-5">
                <input class="form-control search" type="text" id="inputSearch" placeholder="EJ: JUAN LOPERA">
              </div>
              <button type="submit" class="btn btn-primary col-md-3 col-xs-4 submit">BUSCAR</button>
            </form>
          </div>
        </div>
        <?php foreach ($users as $l) {?>
          <?php $u = $l->getMcTusers()->One(); ?>
          <div class="row administration-side administration-side-activity block-border">
            <div class="col-sm-9 col-xs-9 col-lg-9 col-md-9 administration-activity-info ">
              <div class="row">
                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3"> 
                  <img src="<?php if($u->user_avatar){echo Yii::getAlias('@web'). $u->user_avatar;}
                  else {echo Yii::getAlias('@web').'/img/pp.png';} ?>"> 
                </div>
                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
                  <ul class="list-unstyled">
                    <li class="large-title"><?php echo $u->user_name; ?></li>
                    <li>Ultima conexion: <span class="green"><?php echo date("d-m-Y H:i",  strtotime($l->last_connection));?></span></li>
                    <li class="green"><?php echo $u->user_phone; ?></li>
                    <li class="blue"><?php echo $l->email2; ?></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-sm-3 col-xs-12 administration-activity-buttons">
                <div class="row">
                <div class="col-xs-12">
                  <?= Html::a('EDITAR', ['update', 'id' => $l->id], ['class' => 'btn btn-desactive btn-green col-xs-12']) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <?php
                  if($l->activate==1)
                  {
                    echo Html::a('INHABILITAR', ['inhabilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,deactivateuser,'. $l->id.',this,"a","href")',]);
                  }
                  else
                  {
                    echo Html::a('HABILITAR', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-red col-xs-12',
                      'onclick'=>'SeeModal(event,activateuser,'. $l->id.',this,"a","href")',]);
                  }
                  ?>
                </div>
              </div>
            </div>

          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
Modal::begin([
  'id' => 'deactivateuser',
  'header' => '<h3>¿Deseas inhabilitar/habilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['inhabilitar', 'id' => $l->id, 'perfil' => '2' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();

Modal::begin([
  'id' => 'activateuser',
  'header' => '<h3>¿Deseas habilitar este usuario?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['habilitar', 'id' => $l->id, 'perfil' => '1' ], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>

<script>
 var my_data = [];
  $('.blue').each(function(index,value){
    my_data.push($(value).text());
  });
  $('.large-title').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    event.preventDefault();
    $('.administration-activity-info').each(function(index,value){
      $(this).parent().show();
    });
    var text = $('#inputSearch').val();
    $('.administration-activity-info').parent().parent().find('.not-found').remove();
    if(text)
    {
      $('.administration-activity-info').each(function(index,value){
        var bool = false;
        var m = $(value).find('.blue').text()===text || $(value).find('.large-title').text()===text;
        (m===true) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).parent().hide();
        }
      });
    }
    else
    {
      $('.administration-activity-info').each(function(index,value){
        $(this).parent().show();
      });
    }
    var bool=true;
    $('.administration-activity-info').each(function(index,value){
      $(this).parent().attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool && text)
    {
      var txt ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      txt+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      txt+='</h1></div></div>';
      $('.administration-activity-info').parent().parent().append(txt);
    }
  });
</script>
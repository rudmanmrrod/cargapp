<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\models\McTcategory;
use app\models\McTproposal;
use app\models\McTlogin;
use app\models\McTcompanyservices;
use app\models\FormDevolucion;
?>

<?= Html::csrfMetaTags() ?>

<div class="content">
  <div class="container">
    <?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <?= Html::a('USUARIO/EMPRESA', ['cargas'], ['class' => 'btn btn-sub2 btn-active col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('TRANSPORTADORES', ['cargast'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('GARANTIAS', ['cargasg'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <div class="col-md-6 col-xs-12 col-sm-3 col-lg-6">
              <div class="row">

                <div class=" col-md-6 col-md-offset-6"> Abono Recibido: <span class="blue abono_total"></span></div>
               
              </div>
              <div class="row ">     

                <div class="col-md-6 col-md-offset-6">Comision Recibida: <span class="blue comision_total"></span></div>            
              </div>
              <div class="row ">              
                <div class="col-md-6 col-md-offset-6">Pagos Realizados: <span class="blue pagos_total"></span></div>              
              </div>
          </div>
        </div>
        <!--end head-->

        <!-- parts -->
        <div class=" row body-main col-lg-12 part3">
         
            <div class="col-lg-2">
              <div class="col-md-8 col-md-offset-4">  
                Filtrar por:
              </div>
            </div>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">PENDIENTES</button>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">PAGADOS</button>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">DEVOLUCIONES</button>
         
          <div class=" col-lg-4 col-xs-12 ">
            <div class="form-group col-md-8 col-xs-7">
              <input class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
            </div>
            <button type="submit" class="btn btn-primary col-md-4 col-xs-5 submit">BUSCAR</button>
          </div>
        </div>
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
          $admin = McTlogin::find()->where(['role'=>'1'])->one()->getMcTusers()->one();
          foreach ($contratos as $contrato) {
              $load = $contrato->getFkCarga()->One(); 
              $loadpayment = $load->getMcTpaymentloads()->one();
              $user = $load->getFkuser0()->One(); 
              $company = $contrato->getFkuserCompany()->One();
              $comp_profile = $company->getMcTcompanies()->One();
              $proposal = McTproposal::find()->where(['fkload'=>$load->pkload,'fkcompany'=>$comp_profile->pkcompany])->one();
              $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
              $service = McTcompanyservices::find()
                  ->where("fkcompany=:id", [":id" => $comp_profile->pkcompany])
                  ->andWhere('fksubcategory = :sub', [':sub' => $load->getFksubcategory0()->One()->pktcategory_subcategory])
                  ->one();
              $payment = $contrato->getMcTcontractpayments()->one();
              ?>
          <div class="block row col-lg-12">
            <div class="info col-lg-9">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-1">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $user->user_name; ?></h1>
                    <div class="type col-log-6">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Categoria:</p>
                      <p>Sub-categ:</p>
                      <p>Asegura:</p>
                    </div>
                    <div class="col-log-6">
                     <p class="blue"><?php echo $user->fklogin0->email;?></p>
                     <p class="blue"><?php echo $user->user_phone; ?></p>
                     <p class="green"><?php echo $catg->category_name;?></p>
                     <p class="green"><?php echo $catg->getMcTcategorySubcategories()->One()->getFksubcategory0()->One()->subcategory_name;?></p>
                     <p class="red"><?php echo $load->load_ensure. ' COP';?></p>  
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-2">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $comp_profile->company_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="types user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $comp_profile->company_rz ;?></h1>
                    <p><?php echo $comp_profile->company_attendant ;?></p>
                    <div class="type col-log-7">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Costo:</p>
                      <p>Pago Min:</p>
                    </div>
                    <div class="col-log-5">
                     <p class="blue"><?php echo $company->email2;?></p>
                     <p class="blue"><?php echo $comp_profile->company_phone;?></p>
                     <p class="green"><?php echo $proposal->proposal_total. ' COP';?></p>
                     <p class="red pagominimo"><?php echo $proposal->proposal_paymin. ' COP';?></p> 
                    </div>
                  </div>
              </div>
            </div>
          </div><!-- /.info -->
          <?php 
            $zeros = '000000';
            $contract_type = ''.$contrato->pk_contract;
            $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
          ?>
          <div class="right-part col-lg-3">
            <p>Contrato : <span class="blue contract-name"><?php echo 'UE'.$contract_number;?></span></p>
            <div class="type col-log-6">
              <p>Fecha orden:</p>
              <p>Encargado:</p>
              <p>Estado Pago:</p>
              <p>Comison: </p>
            </div>
            <?php $abono = $proposal->proposal_total*($service->companyservices_percent/100); ?>
            <div class="col-log-6">
             <p style="font-weight:bolder"><?php echo date('d/m/Y',strtotime($load->load_create));?></p>
             <p class="blue"><?php echo $admin->user_name; ?></p>
             <p class="red estado"><?php echo $loadpayment->fkstatus0->status_name;?></p>
             <p class="green comision"><?php echo $abono. ' COP';?></p> 
            </div>
          </div><!-- /.right-part -->

          <div class="clearfix"></div>
          <!--info2 -->
          <?php if ($payment->fkstatus == 8){ 
            echo '<div class="info2 col-lg-9">';
            echo '<h4 class="blue">Causa del Reembolso:</h4>'. $payment->paymentdesc.'</div>';
            echo '<div class="col-lg-3 col-xs-12">';
            echo  '<button type="button" class="btn btn-active elimn col-lg-6 col-lg-offset-3 ">REEMBOLSADO</button>';
            echo  '</div>';
          } 
          else if ($payment->fkstatus==7){
            echo '<div class="info2 col-lg-9">';
            echo '<h4 class="blue">Causa de la devolución:</h4>'. $payment->paymentdesc.'</div>';
            echo '<div class="col-lg-3 col-xs-12">';
            echo  '<button type="button" class="btn btn-desactive col-lg-6 col-lg-offset-3 status" name="DEVOLUCIONES">DEVUELTO</button>';
            echo Html::a('REEMBOLSO', ['reepaycontract','id'=>$payment->pkcontractpayment], ['class' => 'btn btn-desactive btn-red btn-sub2 col-lg-6 col-lg-offset-3',
              'onclick'=>'SeeModal(event,makerepay,'.$payment->pkcontractpayment.',this,"form","action")']);
          }
          else{
            echo '<div class="info2 col-lg-9"><div class="col-lg-6"><div class="type col-log-6">';            
            echo '<p class="blue">'.  $comp_profile->fkbank0->bank_name.'</p>';
            echo '<p>'. $comp_profile->company_accounttype==1 ? "Cuenta Corriente":"Cuenta Ahorros";
            echo '</p><p>'.$comp_profile->company_accountnumber.'</p>';
            echo '<p>'. $comp_profile->company_accountname .'</p></div>';
                
            echo ' </div><div class="col-lg-6"><div class="type col-log-6">';
            echo '<h3><p>Valor a Pagar <span class="blue">'. number_format($payment->paymentvalue).'COP</span></p></h3>';
            echo $payment->fkstatus==5 ? '<h5><p>Fecha de pago <span class="blue">'.date('d/m/Y',strtotime($payment->paymentdate)).'</span></p></h5>':'';
            echo '</div></div></div>';
            echo '<div class="col-lg-3 col-xs-12">';
            if($payment->fkstatus==6){
            echo Html::a('PAGAR', ['paycontract','id'=>$payment->pkcontractpayment], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3 status',
              'onclick'=>'SeeModal(event,makepay,'.$payment->pkcontractpayment.',this,"a","href");', 'name' => 'PENDIENTES']);
              echo '<button type="button" class="btn btn-desactive col-lg-6 col-lg-offset-3 ">DEVOLUCION</button>';
            
            }
            else if($payment->fkstatus==5){
              echo '<span class="blue pagar" style="display:none;">'.number_format($abono).'COP</span>';
              echo '<button type="button" class="btn btn-desactive col-lg-6 col-lg-offset-3 status" name="PAGADOS">PAGADO</button>';
              echo Html::a('DEVOLUCIÓN', ['refundcontract','id'=>$payment->pkcontractpayment], ['class' => 'btn btn-desactive btn-red btn-sub2 col-lg-6 col-lg-offset-3',
              'onclick'=>'SeeModal(event,makedevolution,'.$payment->pkcontractpayment.',this,"form","action")']);
            }
            echo '</div>';
           }?>
        </div>

        <?php } ?>
        <!-- End Block ---------------------------------------------------------->
      </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>
<?php 
Modal::begin([
  'id' => 'makepay',
  'header' => '<h3>¿Deseas pagar este contrato?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['paycontract','id'=>$payment->pkcontractpayment], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>

<?php 
$model = new FormDevolucion;
Modal::begin([
  'id' => 'makedevolution',
  'header' => '<h4 class="blue text-center">CONFIRMACIÓN DE DEVOLUCIÓN</h4>',
  'size'=> 'modal-md',
  ]);?>

  <div style="padding:15px 15px 15px 15px;">
    <div class="row text-center">
      <p>¿Está seguro que desea hacer la devolución de este contrato?<br>
      En este caso el dinero será retirado de la empresa de la cuenta y será acreditada
        con saldo a favor en la cuenta del cliente.<br>
      ¡Está acción no se puede deshacer!</p>
    </div>

    <?php $form = ActiveForm::begin([
          'id' => 'devolution-form',
          'options' => ['class' => 'form-signin'],
          'action' => ['refundcontract','id'=>$payment->pkcontractpayment],
      ]); ?>

      <?= $form->field($model, 'motivo')->textArea(['placeholder' => "Escriba aquí la causa de la devolución",
      'class' => 'administration-input'])->label(false); ?>

    <div class="row">
      <div class="col-md-6">
        <button type="button" class="btn btn-desactive btn-red col-lg-8 col-lg-offset-2" data-dismiss="modal">CANCELAR</button>
      </div>
      <div class="col-md-6">
        <button type="submit" class="btn btn-desactive btn-green col-lg-8 col-lg-offset-3">ACEPTAR</button>
      </div>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

<?php 
Modal::end();
?>

<?php 
Modal::begin([
  'id' => 'makerepay',
  'header' => '<h4 class="blue text-center">CONFIRMACIÓN DE REEMBOLSO</h4>',
  'size'=> 'modal-md',
  ]);?>

  <div style="padding:15px 15px 15px 15px;">
    <div class="row text-center">
      <p>¿Está seguro que desea hacer el reembolso de este contrato?<br>
      En este caso el dinero será retirado de la cuenta virtual del cliente y su
      contrato será finalizado.<br>
      ¡Está acción no se puede deshacer!</p>
    </div>

    <?php $form = ActiveForm::begin([
          'id' => 'devolution-form',
          'options' => ['class' => 'form-signin'],
          'action' => ['repaycontract','id'=>$payment->pkcontractpayment],
      ]); ?>

      <?= $form->field($model, 'motivo')->textArea(['placeholder' => "Escriba aquí la causa del reembolso",
      'class' => 'administration-input'])->label(false); ?>

    <div class="row">
      <div class="col-md-6">
        <button type="button" class="btn btn-desactive btn-red col-lg-8 col-lg-offset-2" data-dismiss="modal">CANCELAR</button>
      </div>
      <div class="col-md-6">
        <button type="submit" class="btn btn-desactive btn-green col-lg-8 col-lg-offset-3">ACEPTAR</button>
      </div>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

<?php 
Modal::end();
?>

<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/adminpayment.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
  $srch = Yii::$app->assetManager->publish('@app/web/js/searchfinanzas.js');
  $this->registerJsFile($srch[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>
<script>
 var my_data = [];
  $('.contract-name').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    event.preventDefault();
    $('.block').each(function(index,value){
      $(this).show();
    });
    $('.block').parent().find('.not-found').remove();
    var text = $('#inputSearch').val();
    if(text)
    {
      $('.block').each(function(index,value){
        var bool = false;
        ($(value).find('.contract-name').text()===text) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=true;
    $('.block').each(function(index,value){
      $(this).attr('style')!="display: none;" ? bool=false:'';
    });
    if(bool && text)
    {
      var txt ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      txt+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      txt+='</h1></div></div>';
      $('.block').parent().append(txt);
    }
  });
</script>
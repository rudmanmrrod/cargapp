<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a type="button"  href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles" class="btn btn-sub2 btn-<?php echo $rol==1 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">ADMINISTRADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=3" class="btn btn-sub2 btn-desactive col-md-3 col-xs-12 col-sm-3 col-lg-2">EMPRESAS</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=4" class="btn btn-sub2 btn-<?php echo $rol==4 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">TRANSPORTADORES</a>
          <a type="button" href="<?php echo Yii::getAlias('@web') ?>/admin/perfiles?perfil=2" class="btn btn-sub2 btn-<?php echo $rol==2 ? 'active': 'desactive';?> col-md-3 col-xs-12 col-sm-3 col-lg-2">USUARIOS</a>
        </div>
        <!--end head-->
      </div>
        <!--End content-->
    </div>
  </div>
</div>

<?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>

<div class="col-sm-4"></div>
<div class="col-sm-4 administration-left-side-wrapper">
  <div class="col-xs-12 administration-side administration-left-side new-user-za">
      <?php $form = ActiveForm::begin([
        'method' => 'post',
        //'id' => 'new-admin',
        'action' => ['admin/update', 'id' => $id],
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'options' => ['enctype'=>'multipart/form-data'],
      ]);?>

    <h1>Editar <?php echo $rol==1 ? 'administrador': 'usuario';?></h1><hr>

    <?= $form->field($model, "name")->input("text", ['placeholder' => 'Usuario', 'class' => 'administration-input'])->label('Nombre de Usuario'); ?>

    <?= $form->field($model, "acc_name")->input("text", ['placeholder' => 'Nombre de la Cuenta', 'class' => 'administration-input'])->label('Nombre de la Cuenta'); ?>

    <?= $form->field($model, "email")->input("text", ['placeholder' => 'Nombre', 'class' => 'administration-input']); ?>   

    <?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'administration-input'])->label('Contraseña Original'); ?>
   
    <?= $form->field($model, "new_password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'administration-input'])->label('Nueva Contraseña'); ?>

    <?= $form->field($model, "new_password_repeat")->input("password", ['placeholder' => 'Confirmar Contraseña', 'class' => 'administration-input'])->label('Repita la Contraseña'); ?>
   
    <?= $form->field($model, "phone")->input("text", ['placeholder' => 'Teléfono', 'class' => 'administration-input'])->label('Teléfono'); ?>

    <div class="profile-photo">
      <div class="photo-area">
        <div class="col-md-6">
            <p>Foto Perfil</p>
        </div>
        <div class="fileUpload col-md-6" <?php if($model->avatar){?>style = "background-image: url(<?php echo Yii::getAlias('@web').$model->avatar;?>);
        background-size: 105px 105px" <?php } ?> >
          <?= $form->field($model, "avatar")->fileInput(['class' => 'upload'])->label(false); ?>
        </div>
      </div>
    </div>
  
    <div class="col-xs-12 administration-submit">
      <?= Html::submitButton('EDITAR', ['class' => 'btn btn-active btn-green col-xs-12']) ?>
    </div>

  <?php $form->end() ?>
  
  </div>
</div>
<div class="col-sm-4"></div>



<script type="text/javascript">
$(document).ready(function() { 
    var base64 = "";
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "105px 105px");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
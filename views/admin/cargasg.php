<?php
use yii\helpers\Html;
use app\models\McTcategory;
use app\models\McTproposal;
use app\models\McTlogin;
use app\models\McTcompanyservices;
?>

<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper">
      <!-- main content -->
      <div class="main">
        <!--head-->
        <div class="row head-main col-lg-12">
          <?= Html::a('USUARIO/EMPRESA', ['cargas'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('TRANSPORTADORES', ['cargast'], ['class' => 'btn btn-sub2 btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <?= Html::a('GARANTIAS', ['cargasg'], ['class' => 'btn btn-sub2 btn-active col-md-2 col-xs-12 col-sm-3 col-lg-2']) ?>
          <div class="col-md-6 col-xs-12 col-sm-3 col-lg-6">
              <div class="row">

                <div class=" col-md-6 col-md-offset-6 "> Abono Recibido: <span class="blue abono_total"></span></div>
               
              </div>
              <div class="row ">     

                <div class="col-md-6 col-md-offset-6">Comision Recibida: <span class="blue comision_total"></span></div>            
              </div>
              <div class="row ">              
                <div class="col-md-6 col-md-offset-6">Pagos por realizar: <span class="blue pagos_total"></span></div>              
              </div>
          </div>
        </div>
        <!--end head-->

        <!-- parts -->
        <div class=" row body-main col-lg-12 part3">
         
        
          
            <div class="col-lg-2">
              <div class="col-md-8 col-md-offset-4">  
                Filtrar por:
              </div>
            </div>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">PENDIENTES</button>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">PAGADOS</button>
            <button type="button" class="btn btn-desactive col-md-2 col-xs-12 col-sm-3 col-lg-2 srch">DEVOLUCIONES</button>
         
          <div class=" col-lg-4 col-xs-12 ">
              <div class="form-group col-md-8 col-xs-7">
                <input class="form-control search col-l-8" id="inputSearch" placeholder="EJ: AM0000123">
              </div>
              <button type="submit" class="btn btn-primary col-md-4 col-xs-5 submit">BUSCAR</button>
          </div>
        </div>
        <!-- End Parts-->
        <div class="clearfix"></div>
        <!--content-->
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
          $admin = McTlogin::find()->where(['role'=>'1'])->one()->getMcTusers()->one();
          foreach ($contratos as $contrato) {
              $payment = $contrato->getMcTcontractpayments()->one();
              $count = 0;
              $load = $contrato->getFkCarga()->One(); 
              $loadpayment = $load->getMcTpaymentloads()->one();
              $user = $load->getFkuser0()->One(); 
              $company = $contrato->getFkuserCompany()->One();
              $comp_profile = $company->getMcTcompanies()->One();
              $proposal = McTproposal::find()->where(['fkload'=>$load->pkload,'fkcompany'=>$comp_profile->pkcompany])->one();
              $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
              $service = McTcompanyservices::find()
                  ->where("fkcompany=:id", [":id" => $comp_profile->pkcompany])
                  ->andWhere('fksubcategory = :sub', [':sub' => $load->getFksubcategory0()->One()->pktcategory_subcategory])
                  ->one();
              if($payment->fkstatus==7 or $payment->fkstatus==8){
                $count++;
              ?>
          <div class="block row col-lg-12">
            <div class="info col-lg-9">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-1">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $user->user_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $user->user_name; ?></h1>
                    <div class="type col-log-6">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Categoria:</p>
                      <p>Sub-categ:</p>
                      <p>Asegura:</p>
                    </div>
                    <div class="col-log-6">
                     <p class="blue"><?php echo $user->fklogin0->email;?></p>
                     <p class="blue"><?php echo $user->user_phone; ?></p>
                     <p class="green"><?php echo $catg->category_name;?></p>
                     <p class="green"><?php echo $catg->getMcTcategorySubcategories()->One()->getFksubcategory0()->One()->subcategory_name;?></p>
                     <p class="red"><?php echo $load->load_ensure. ' COP';?></p>  
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-2">
                  <div class="profile-picture-wrapper col-lg-4 col-md-4 col-sm-3 col-xs-3">
                    <img class="profile-picture" src="<?php echo Yii::getAlias('@web') . $comp_profile->company_avatar; ?>" alt="Profile Picture">
                  </div>
                  <div style="padding-left:0;" class="types user-info-wrapper col-lg-8 col-md-8 col-sm-9 col-xs-9">
                    <h1><?php echo $comp_profile->company_rz ;?></h1>
                    <p><?php echo $comp_profile->company_attendant ;?></p>
                    <div class="type col-log-7">
                      <p>E-mail:</p>
                      <p>Telefono:</p>
                      <p>Costo:</p>
                      <p>Pago Min:</p>
                    </div>
                    <div class="col-log-5">
                     <p class="blue"><?php echo $company->email2;?></p>
                     <p class="blue"><?php echo $comp_profile->company_phone;?></p>
                     <p class="green"><?php echo $proposal->proposal_total. ' COP';?></p>
                     <p class="red pagominimo"><?php echo $proposal->proposal_paymin. ' COP';?></p> 
                    </div>
                  </div>
              </div>
            </div>
          </div><!-- /.info -->
          <?php 
            $zeros = '000000';
            $contract_type = ''.$contrato->pk_contract;
            $contract_number = substr_replace($zeros,$contract_type,strlen($zeros)-strlen($contract_type));
          ?>
          <div class="right-part col-lg-3">
            <p>Contrato : <span class="blue contract-name"><?php echo 'UE'.$contract_number;?></span></p>
            <div class="type col-log-6">
              <p>Fecha orden:</p>
              <p>Encargado:</p>
              <p>Estado Pago:</p>
              <p>Comison: </p>
            </div>
            <div class="col-log-6">
             <p style="font-weight:bolder"><?php echo date('d/m/Y',strtotime($load->load_create));?></p>
             <p class="blue"><?php echo $admin->user_name; ?></p>
             <p class="red estado"><?php echo $loadpayment->fkstatus0->status_name;?></p>
             <p class="green comision"><?php echo ($proposal->proposal_total*($service->companyservices_percent/100)). ' COP';?></p> 
             <p class="status" name=<?php if($payment->fkstatus==7)echo "DEVOLUCIONES";else if($payment->fkstatus==6) echo "PENDIENTES";
             else if($payment->fkstatus==5) echo "PAGADOS";?>></p>
            </div>
          </div><!-- /.right-part -->

          
          <?php if ($payment->fkstatus == 8){ 
            echo '<div class="info2 col-lg-9">';
            echo '<h4 class="blue">Causa del Reembolso:</h4>'. $payment->paymentdesc.'</div>';
            echo '<div class="col-lg-3 col-xs-12">';
            echo  '<button type="button" class="btn btn-active elimn col-lg-6 col-lg-offset-3 ">REEMBOLSADO</button>';
            echo  '</div>';
          } 
          else if ($payment->fkstatus==7){
            echo '<div class="info2 col-lg-9">';
            echo '<h4 class="blue">Causa de la devolución:</h4>'. $payment->paymentdesc.'</div>';
            echo '<div class="col-lg-3 col-xs-12">';
            echo  '<button type="button" class="btn btn-desactive col-lg-6 col-lg-offset-3 status" name="DEVOLUCIONES">DEVUELTO</button>';
          }?>
        </div>
        <?php 
          }
          else
          {
            echo '<div class="block row col-lg-12" style="display:none;">';
            $abono = $proposal->proposal_total*($service->companyservices_percent/100);
            echo '<p class="red estado">'.$loadpayment->fkstatus0->status_name.'</p>';
            echo '<p class="red pagominimo">'.$proposal->proposal_paymin.' COP </p>';
            echo '<p class="green comision">'.$abono.' COP</p>';
            if($payment->fkstatus==5){
              echo '<span class="blue pagar">'.number_format($abono).'COP</span>'; 
            }
            echo '</div>';
          }
        } 
        if($count==0)
          {
            echo '<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
            echo '<h1><small class="small-block mt"><strong>No se encontraron contratos!</strong></small>';
            echo '</h1></div></div>';
          }?>
        <!-- End Block ---------------------------------------------------------->
      </div>
        <!--End content-->
      </div>
    </div>
  </div>
  <!-- End main content -->
</div>
<?php
  $pub = Yii::$app->assetManager->publish('@app/web/js/adminpayment.js');
  $this->registerJsFile($pub[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
  $srch = Yii::$app->assetManager->publish('@app/web/js/searchfinanzas.js');
  $this->registerJsFile($srch[1], ['depends' => ['yii\web\JqueryAsset'], 'position' => \yii\web\View::POS_END ]); 
?>

<script>
 var my_data = [];
  $('.contract-name').each(function(index,value){
    my_data.push($(value).text());
  });
  $('#inputSearch').autocomplete({source:my_data});
  $('.submit').click(function(event){
    event.preventDefault();
    $('.block').each(function(index,value){
      $(this).show();
    });
    $('.block').parent().find('.not-found').remove();
    var text = $('#inputSearch').val();
    if(text)
    {
      $('.block').each(function(index,value){
        var bool = false;
        ($(value).find('.contract-name').text()===text) ? bool = true: bool = false;
        if(!bool)
        {
          $(this).hide();
        }
      });
    }
    else
    {
      $('.block').each(function(index,value){
        $(this).show();
      });
    }
    var bool=false;
    $('.block').each(function(index,value){
      $(this).attr('style')=="display: none;" ? bool=true:'';
    });
    if(bool)
    {
      var text ='<div class="row not-found" style="background-color: #fff;"><div class="page-header">';
      text+='<h1><small class="small-block mt"><strong>No se encontraron resultados!</strong></small>';
      text+='</h1></div></div>';
      $('.block').parent().append(text);
    }
  });
</script>
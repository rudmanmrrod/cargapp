<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<style type="text/css">
	.form-group {
	    margin-bottom: -10px !important;
	}
</style>

<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
<div class="content">
  <div class="container">
  	<center><h3 class="confirm"><?= $msg ?></h3></center>
    <div class="edit-form register-form">
      <div class="row white edit-form-inner">
      	<?php $form = ActiveForm::begin([
		    'method' => 'post',
			'id' => 'frmreg',
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'options' => ['enctype'=>'multipart/form-data']
		]);
		?>
		<center><h4>REGISTRO DE USUARIO</h4></center>
		<?= $form->field($model, "name")->input("text", ['placeholder' => 'Nombre', 'class' => 'form-field'])->label(false) ?>   
		<?= $form->field($model, "email")->input("email", ['placeholder' => 'Correo', 'class' => 'form-field'])->label(false); ?>
		<?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'form-field'])->label(false); ?>
		<?= $form->field($model, "password_repeat")->input("password", ['placeholder' => 'Confirmar Contraseña', 'class' => 'form-field'])->label(false); ?>
		<?= $form->field($model, "phone")->input("number", ['placeholder' => 'Teléfono Móvil', 'class' => 'form-field'])->label(false); ?>
		<div class="profile-photo">
	        <div class="photo-area">
	          <div class="fileUpload"> <span>Cambiar Foto</span>
	            <?= $form->field($model, "avatar")->fileInput(['class' => 'upload'])->label(false); ?>
	          </div>
	        </div>
	    </div>
	    <div>
            <center>
              <?= $form->field($model, 'type')->radioList(array(2=>'Cliente',4=>'Transportador'))->label(false); ?> 
            </center>
        </div>
		<?php $form->end() ?>
		</div>
		<div class="submit-btn-area">
	        <button type="submit" class="updateprofile_btn reg has-spinner">
	          REGISTRARME 
	          <span class="spinner"><i class="fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom"></i></span>
	        </button>
	    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {  
    var base64 = "";
    $(".reg").click(function(){
      $("#frmreg").submit();
    });
    $(".upload").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
        reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          base64 = object.data;
          $(".fileUpload").css("background-size", "100% auto");
          $(".fileUpload").css("background-image", "url(" + base64 + ")");
        };  
        reader.readAsDataURL(file);
      });
    });
});
</script>
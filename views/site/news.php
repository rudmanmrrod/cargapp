<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
<div class="content">
  <div class="container">
		<div class="page-header">
 			<h1><strong>Secci&oacute;n de Noticias</strong><br>
                <small class="small-block"><strong>No te pierdas las &uacute;ltimas noticias en </strong><span>Mi Cargapp</span></small>
            </h1>
		</div> 	
       <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <?php foreach ($news as $new){ ?>
            <div class="article">
                  <div class="profile-right-side col-xs-12 bwhite">
                    <?php if($new->new_image == ''){ ?>
                      <div class="profile-info article-image-wrapper col-xs-12">
                          <img class="profile-image" src="<?php echo Yii::getAlias('@web')?>/img/icon-user.png" alt="Profile Picture"/>
                      </div>
                    <?php }else{ ?>
                      <img class="col-lg-5 col-sm-12 col-md-12 col-xs-12" style="padding-right: 0px; padding-left: 0px;" src="<?php echo Yii::getAlias('@web') . $new->new_image;?>">
                    <?php } ?>
                    <h1 class="article-title col-lg-7 mt5"><?php echo $new->new_title; ?></h1>
                    <h3 class="article-date col-lg-7 mt0"><?php echo date("d M Y",  strtotime($new->new_date));?></h3>
                    <p class="article-text col-lg-7"><?php echo $new->new_message; ?></p>
                  </div>
            </div>
          <?php } ?>
        </div>
  </div>
  <div class="clearfix"></div>
</div>

<!-- End main content -->

</div>
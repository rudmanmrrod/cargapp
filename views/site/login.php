<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="signin-content">
  <div class="signin-form">
     <div class="logo">
     <img src="<?php echo Yii::getAlias('@web'); ?>/img/logo-vertical.png" alt="" class="img-responsive"/>
     </div>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-signin']
        ]); ?>
        <?= $form->field($model, "username")->input("text", ['placeholder' => 'Email o usuario', 'class' => 'form-control'])->label(false); ?>
        <?= $form->field($model, "password")->input("password", ['placeholder' => 'Contraseña', 'class' => 'form-control'])->label(false); ?>
        <div class="forgot-password text-right">
            <a href="#" id="forgot">Olvid&oacute; su contrase&ntilde;a?</a>
          </div>
          <button class="btn btn-lg btn-primary btn-block" type="submit" id="login">INGRESAR</button>
          <div class="noaccount">
              <center>¿No tienes una cuenta? <a class="green_corp" href="<?php echo Yii::getAlias('@web'); ?>/site/registro">Reg&iacute;strate!</a></center>
          </div>
        <div id="forgot-section">
          <span id="forgot-close"></span>
          <p class="forgot-text">Escriba el correo con el que se registro all&iacute; le enviaremos informaci&oacute;n</p>
            <p class="forgot-email"><input type="text" id="inputcorreo" class="form-control" placeholder="Correo" required autofocus></p>
            <p><button class="btn btn-lg btn-primary btn-block" type="submit">RECUPERAR CONTRASE&Ntilde;A</button></p>
        </div>
        <?php ActiveForm::end(); ?>
  </div>
</div>
<!-- jQuery --> 
<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="<?php echo Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $("#forgot, #forgot-close").click(function(){
        $("#forgot-section").fadeToggle(600);
    });
    $("#login").click(function(){
      $(".form-signin").submit();
    });
});
</script>
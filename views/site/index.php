<script src="<?php echo Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
<div class="content">
  <div class="container">
		<div class="page-header">
 			<h1><strong>Bienvenido a Mi Cargapp </strong><br>
                <small class="small-block"><strong>cuentanos, ¿que deseas cotizar hoy?</strong></small>
            </h1>
		</div> 	
       <div class="home-boxes">
        	<?php foreach ($categorys as $c) {?>
                <div class="home-box">
                    <div class="box pointer">
                    <div class="icon">
                        <span class="front">
                            <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/<?php echo $c->caregory_location;?>b.png" class="img-responsive" alt=""/>
                        </span>
                        <span class="hover">
                            <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/<?php echo $c->caregory_location;?>w.png" class="img-responsive" alt=""/>
                        </span>
                    </div>
                    <div class="desc">
                        <p><?php echo $c->category_name;?></p>
                    </div>
                    </div>
                </div>
            <?php } ?>
        </div>
  	<div class="page-header-2">
 		<h1>
            <strong>
                Beneficios de <span>Mi Cargapp</span>
                <small class="small-block"><strong>¿Porqué somos mejor que cualquier cosa que hayas visto?</strong></small>
            </strong>
        </h1>
		</div>
    <div class="bottom-boxs clearfix">
    	<div class="col-sm-6 col-md-3">
        	<div class="box">
            	<div class="icon">
                	<img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon3.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                	<p>Pueden elegir las mejores tarifas para todo tipo de transporte</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
        	<div class="box color2">
            	<div class="icon">
                	<img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon2.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                	<p>Pago de un anticipo mínimo y el saldo del transporte contraentrega</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
        	<div class="box color3">
            	<div class="icon">
                	<img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon1.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                	<p>Si la empresa que eligió incumple puede cancelar el servicio y solocitar otra</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
        	<div class="box color4">
            	<div class="icon">
                	<img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/icon1.png" class="img-responsive" alt=""/>
                </div>
                <div class="info">
                	<p>Si no se siente satisfecho y ninguna empresa le cumplio, devolvemos el dinero</p>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<!-- End main content -->

</div>

<script type="text/javascript">
    $(".box").click(function(){
        location.href="site/login";
    });
</script>
<?php
  use yii\helpers\Html;
  use yii\bootstrap\Modal;
  use app\models\McTcategory;
?>
<div class="content">
  <div class="container">
    <?php if($msg) echo '<h3 class="confirm">' . $msg . '</h3>';?>
    <div class="row white administration-navigation-wrapper">
      <div class="main">
        <div class="clearfix"></div>
        <div class="blocks">
          <!-- Block ---------------------------------------------------------->
          <?php 
              $company = $proposal->getFkcompany0()->One(); 
              $load = $proposal->getFkload0()->One(); 
              $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
              $loadpayment = $load->getMcTpaymentloads()->one();
              $contrato = $load->getMcTcontracts()->one();
            ?>
            <div class="col-lg-12 qoute-main-block">
              <div class="row administration-side administration-side-activity qoute-inner-block margin-4">
                <div class="col-sm-6 col-xs-12 administration-activity-info ">
                  <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                      <img src="<?php echo Yii::getAlias('@web').$company->company_avatar ?>">
                      <div class="rating-star"><input class="input-rating2 rating" value="<?php echo $company->company_avg;?>" data-step="0.5" type="number"></div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 low-space">
                        <p class="large-title"><?php echo strtoupper($company->company_rz); ?></p>
                        <p><?php echo $company->company_attendant; ?></p>
                        <div class="type col-log-6">
                          <p><span class="type-text">Email:</span>  <span class="blue"><?php echo $company->fklogin0->email2;?></span></p>
                          <p><span class="type-text">Teléfono:</span>  <span class="blue"><?php echo $company->company_phone;?></span></p>
                          <p><span class="type-text">Categoría:</span>  <span class="green"><?php echo $catg->category_name;?></span></p>
                          <p><span class="type-text">Sub-Catg:</span>  <span class="green"><?php echo $load->getFksubcategory0()->One()->fksubcategory0->subcategory_name;; ?></span></p>
                        </div>                    
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-sm-offset-0 quote-box">
                  <div class="contract-box ">
                    <div class="type col-sm-8 col-xs-7">
                      <p><span class="width-box">Fecha orden:</span>    <span style="font-weight:bolder" class="blue"><?php echo date("d-m-Y",  strtotime($load->load_create));?></span></p>
                      <p><span class="width-box">Valor a Pagar:</span>  <span class="blue"><input type="text" disabled class="textbox-field" placeholder="$ <?php echo number_format($proposal->proposal_total); ?>"><span class="field_title">COP</span></span></p>
                      <p><span class="width-box">Pago M&iacute;nimo:</span>  <span class="blue"><input type="text" disabled class="textbox-field" placeholder="$ <?php echo number_format($proposal->proposal_paymin); ?>"><span class="field_title">COP</span></span></p>
                    </div>
                    <div class="type col-sm-12 col-xs-5"> 
                        <button class="btn btn-active btn-block" onclick="showChat()">CHAT</button>
                        <small class="small-text hidden-xs">
                          Una vez hayas recibido la mercancía a gusto, puedes marcar la casilla de recibido
                        </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row administration-side administration-side-activity qoute-inner-block bgwhite">
              <div class="col-sm-10 col-xs-12 administration-activity-info small-padding-around bggray">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-5">
                   <div class="description-block">
                    <p><span class="width-box">Desde:</span><span><?php echo $load->load_cityorigin; ?></span></p>
                    <p><span class="width-box">Hasta:</span><span><?php echo $load->load_citydestination; ?></span></p>
                      <p><span class="width-box">Peso:</span><span><?php echo $load->load_weight < 1000 ? $load->load_weight . " Kl" : ($load->load_weight / 1000) . " Ton." ?></span></p>
                   </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-5">
                    <div class="Dimensiones-block">
                      <p><span class="width-box">Dimensiones:</span><span><?php echo $load->load_dimensions; ?></span></p>
                      <p><span class="width-box">Observación:</span><span class="observation-text"><?php echo $load->load_comment; ?></span></p>
                   </div>                
                  </div>
                </div>
              </div>
              <div class="col-sm-2 col-sm-offset-0  Remove-box">
                <?php if($load->fkstatus==1){
                  echo '<button type="button" class="btn btn-desactive btn-block remove-btn mt25" data-toggle="modal" data-target="#recibido">RECIBIDO</button>';
                }
                else if($load->fkstatus==3){
                  echo '<button type="button" class="btn btn-desactive btn-block" data-toggle="modal" data-target="#recibido">RECIBIDO</button>';
                  if(!$contrato->getMcTcontractevals()->one())
                  {
                    echo '<button type="button" class="btn btn-desactive btn-block btn-blue" id="ended" data-toggle="modal" data-target="#calification">CALIFICAR</button>';
                  }
                }
                  ?>
              </div>
            </div>
          </div>
        <!-- End Block ---------------------------------------------------------->
        </div>
      </div>
    </div>
</div>
      <!-- End main content -->
<div class="container chatcontent" style="display:none;">
  <div class="row white" ng-controller="Chat">
    <div class="col col-20"></div>
    <div class="col col-66">
      <div class="">
          <div class="">
              <div class="btn-group pull-right">
                  
              </div>
          </div>
          <div class="panel-body">
              <ul class="chat">
                  <!-- DATOS -->
                  <div ng-repeat="mensaje in mensajes.slice().reverse()">
                    <li class="left clearfix chatli">
                        <div class="chat-body clearfix">
                          <div class=""
                            ng-class="mensaje[1] ? 'panel-footer-blue' : 'panel-footer-gray'">
                            {{mensaje[0]}} {{ mensaje[3] }}
                          </div>
                          <p class=""
                            ng-class="mensaje[1] ? 'box-blue' : 'box-gray'">
                              {{ mensaje[2] }}
                          </p>
                        </div>
                    </li>
                  </div>
              </ul>
          </div>
          <form name="form">
            <div>
                <div class="list">
                  <div class="item item-input-inset back-positive text-center">
                    <label class="item-input-wrapper input-50">
                      <input type="text" placeholder="Escribir..."
                       name="textMsj" ng-model="textMsj" id="input_send" size="70px" focus-me-now>
                    </label>
                    <a class="button button-small back-positive" ng-click="send()" >
                      <i class="glyphicon glyphicon-send"></i>
                    </a>
                  </div>
                </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php $this->registerJs("var contractemp = ".json_encode($contrato->pk_contract).";", \yii\web\View::POS_END, 'contractemps');

Modal::begin([
  'id' => 'calification',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="content">';
    echo '<div class="row text-center">';
      echo '<small style="color: #114459;"><strong>Gracias por usar Mi Cargapp!</strong></small>';
    echo '</div>';
    echo '<div class="row text-center">';
      echo '<h4 class="mred"><strong>No olvides calificar tu transportador<strong></h4>';
    echo '</div>';
    echo '<div class="row text-center">';
     echo '<small style="color: #114459;"><strong>Tu puntaje se mantendrá anónimo,<br> ayudanos a llevar un mejor servicio a otros usuario</strong></small>';
    echo '</div><br>';
    echo '<div class="row text-center">';
      echo '<div class="rating-star">Mi Calificación: <input class="input-rating rating companyrating" value=0 type="number"></div>';
    echo '</div>';
      echo '<div class="row text-center">';
      echo '<div class="calification-text"></div>';
    echo '</div>';
    echo '<div class="row text-center">';
      echo '<button type="button" class="btn btn-desactive btn-blue btn-sub2 col-lg-6 col-lg-offset-3" onclick="calificar()">CALIFICAR</button>';
    echo '</div>';
  echo '</div>';

Modal::end();

Modal::begin([
  'id' => 'recibido',
  'header' => '<h3>¿Deseas marcar esta carga como recibida?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['endproposal','id'=>$proposal->pkproposal], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>
<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var ok = $('#ended');
    console.log(ok);
    if(ok.length>0)
    {
      $('#calification').modal();
    }
  });
  $(".input-rating2").rating({
    size:'xs',
    readonly:true,
    showCaption: false,
    disabled: false,
    showClear: false,
  });

  $(".input-rating").rating({
    size:'xs',
    showCaption: true,
    showClear: false,
  });

  function showChat()
  {
    if($('.chatcontent').css("display")=="none")
    {
     $('.chatcontent').show(); 
    }
    else
    {
      $('.chatcontent').hide();
    }
  }

  function calificar()
  {
    var ratingvalue = $('.companyrating').val();
    if(ratingvalue>0.0)
      evaluatecompany(url+'/account/evaluatecompany',ratingvalue,contractemp)
  }
</script>
<?php
  use yii\helpers\Html;
  use yii\bootstrap\Modal;
  use app\models\McTcategory;
?>
<div class="content">
      <div class="container">
        <div class="row white administration-navigation-wrapper">
          <div class="main">
            <div class="clearfix"></div>
            <?php if(Yii::$app->session->get('status') != null){?>
              <?php 
                $type = ""; 
                $message = "";
                if(Yii::$app->session->get('status') == -1){
                  $type = "danger";
                  $message = "No se pudo realizar el pago, por favor intente nuevamente";
                }
                if(Yii::$app->session->get('status') == 0){
                  $type = "warning";
                  $message = "El pago esta en proceso de verificación";
                }
                if(Yii::$app->session->get('status') == 1){
                  $type = "success";
                  $message = "El pago se ha realizado correctamente";
                }
              ?>
            <?php }?>
            <div class="blocks">
            <?php
              if (Yii::$app->session->get('status') != null) {?>
                <div class="row">
                <div class="alert alert-<?php echo $type; ?>" role="alert"> 
                <strong><?php echo $message; ?></strong></div>
              </div>
            <?php 
              }
              Yii::$app->session->set('status',null);
              if(isset($msj)){
                echo '<div class="row">';
                echo '<div class="alert alert-<?php echo $type; ?>" role="alert"> ';
                echo '<strong>'.$msj.'</strong></div></div>';
              }
            ?>
              <!-- Block ---------------------------------------------------------->
              <?php 
                foreach ($proposals as $p) {
                  $company = $p->getFkcompany0()->One(); 
                  $load = $p->getFkload0()->One(); 
                  $catg = McTcategory::find($load->getFksubcategory0()->One()->fkcategory)->One(); 
                  $loadpayment = $load->getMcTpaymentloads()->one();
                ?>
                <div class="col-lg-12 qoute-main-block">
                    <div class="row administration-side administration-side-activity qoute-inner-block margin-4">
                  <div class="col-sm-6 col-xs-12 administration-activity-info ">
                    <div class="row">
                      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                        <img src="<?php echo Yii::getAlias('@web').$company->company_avatar ?>">
                        <div class="rating-star"><input class="input-rating2 rating" value="<?php echo $company->company_avg;?>" type="number"></div>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 low-space">
                           <p class="large-title"><?php echo strtoupper($company->company_rz); ?></p>
                          <div class="type col-log-6">
                            <p class="red"></p><br>
                           <p><span class="type-text">Categoria:</span>  <span class="green"><?php echo $catg->category_name;?></span></p>
                           <p><span class="type-text">Sub-Catg:</span>  <span class="green"><?php echo $load->getFksubcategory0()->One()->fksubcategory0->subcategory_name;; ?></span></p>
                           <?php if(!$load->load_ensure == null){ ?>
                            <p><span class="type-text">Asegura:</span>  <span class="blue">$ <?php echo number_format($load->load_ensure); ?></span></p>
                           <?php }?>
                          </div>                    
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-sm-offset-0 quote-box">
                      <div class="contract-box ">
                  <form>
                    <div class="type col-sm-8 col-xs-7">
                    <p><span class="width-box">Fecha orden:</span>    <span style="font-weight:bolder" class="blue"><?php echo date("d-m-Y",  strtotime($load->load_create));?></span></p>
                    <p><span class="width-box">Valor a Pagar:</span>  <span class="blue"><input type="text" disabled class="textbox-field" placeholder="$ <?php echo number_format($p->proposal_total); ?>"><span class="field_title">COP</span></span></p>
                    <?php if(!$load->load_ensure == null){ ?>
                      <p><span class="width-box">Pago M&iacute;nimo:</span>  <span class="blue"><input type="text" disabled class="textbox-field" placeholder="$ <?php echo number_format($p->proposal_paymin); ?>"><span class="field_title">COP</span></span></p>
                    <?php }?>
                  </div>
                  <?php if(!$loadpayment){?>
                  <div class="type col-sm-12 col-xs-5"> 
                    <?php if(!$load->load_ensure == null){ ?>
                      <a href="<?php echo Yii::getAlias('@web') ?>/account/payment?pid=<?php echo $p->pkproposal;?>" class="btn btn-active btn-blue btn-block pay padding-t10">PAGAR</a>
                    <?php }?>
                  </div>
                  <div class="col-sm-12">
                    <?php if(!$load->load_ensure == null){ ?>
                      <small class="small-text hidden-xs">
                        Si quieres contratar esta empresa debes pagar el m&iacute;nimo para empezar tu viaje. Tenemos un sitio seguro
                        donde puedes pagar con cualquier medio de pago
                      </small>
                    <?php }?>
                    <?php }else{
                        if ($load->fkstatus==14) {
                          echo '<button type="submit" class="btn btn-active mt50 submit btn-blue btn-block accept">CANCELADO</button>';
                        }
                        else
                        {
                          echo '<div class="type col-sm-12 col-xs-5">';
                          echo Html::a('ENTRAR', ['enterproposal','id'=>$p->pkproposal], ['class' => 'btn btn-sub2 btn-active btn-block pay padding-t10']);
                          echo '</div>';
                        }
                     }?>
                  </div>
                   </form>
                  </div>
                  </div>
                </div>
                <div class="row administration-side administration-side-activity qoute-inner-block bgwhite">
                  <div class="col-sm-10 col-xs-12 administration-activity-info small-padding-around bggray">
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-5">
                       <div class="description-block">
                        <p><span class="width-box">Desde:</span><span><?php echo $load->load_cityorigin; ?></span></p>
                        <p><span class="width-box">Hasta:</span><span><?php echo $load->load_citydestination; ?></span></p>
                          <p><span class="width-box">Peso:</span><span><?php echo $load->load_weight < 1000 ? $load->load_weight . " Kl" : ($load->load_weight / 1000) . " Ton." ?></span></p>
                       </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-5">
                          <div class="Dimensiones-block">
                        <p><span class="width-box">Dimensiones:</span><span><?php echo $load->load_dimensions; ?></span> metros</p>
                        <p><span class="width-box">Observación:</span><span class="observation-text"><?php echo $load->load_comment; ?></span></p>
                       </div>                
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2 col-xs-12">
                    <?php if(!$loadpayment){
                      echo Html::a('ELIMINAR', ['deleteproposal','id'=>$p->pkproposal], ['class' => 'btn btn-sub2 btn-desactive btn-red btn-block pay padding-t10',
                        'onclick'=>'SeeModal(event,deleteprop,'. $p->pkproposal.',this,"a","href")',]);
                    }?>
                  </div>
                </div>
              </div>
            <?php }?>
            <!-- End Block ---------------------------------------------------------->
            </div>
          </div>
        </div>
      </div>
      <!-- End main content -->
    </div>

<?php 
Modal::begin([
  'id' => 'deleteprop',
  'header' => '<h3>¿Deseas eliminar esta propuesta?</h3>',
  'size'=> 'modal-md',
  ]); 

  echo '<div class="row">';
    echo '<div class="col-md-6">';
      echo '<p>Esta acción no se puede deshacer</p><br>';
    echo '</div>';
  echo '</div>';
  echo '<div class="row">';
  echo '<div class="col-md-6">';
  echo Html::a('SI', ['inhabilitar', 'deleteproposal' => $p->pkproposal], ['class' => 'btn btn-desactive btn-green btn-sub2 col-lg-6 col-lg-offset-3']);
  echo '</div><div class="col-md-6">';
  echo '<button type="button" class="btn btn-desactive btn-red col-lg-6 col-lg-offset-3" data-dismiss="modal">NO</button>';
  echo '</div></div>';

Modal::end();
?>
<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.js"></script>
<script type="text/javascript">
  $(".input-rating2").rating({
    size:'xs',
    readonly:true,
    showCaption: false,
    disabled: false,
    showClear: false,
  });
</script>
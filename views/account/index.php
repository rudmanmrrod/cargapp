
<div class="page-header">
		<h1><strong>Hola <span><?php echo Yii::$app->session->get("name") ?>.</span> Bienvenido a Mi Cargapp </strong><br>
        <small class="small-block"><strong>cuentanos, ¿que deseas cotizar hoy?</strong></small>
    </h1>
</div> 	
<div class="home-boxes">
    <?php foreach ($categorys as $c) {?>
        <div class="home-box">
            <div class="box pointer" id="<?php echo $c->pkcategory; ?>">
            <div class="icon">
                <span class="front">
                    <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/<?php echo $c->caregory_location;?>b.png" class="img-responsive" alt=""/>
                </span>
                <span class="hover">
                    <img src="<?php echo Yii::getAlias('@web'); ?>/img/icons/<?php echo $c->caregory_location;?>w.png" class="img-responsive" alt=""/>
                </span>
            </div>
            <div class="desc">
                <p><?php echo $c->category_name;?></p>
            </div>
            </div>
        </div>
    <?php } ?>
</div>

<script type="text/javascript">
    $(".box").click(function(){
        location.href="account/cotizar?category=" + $(this).attr("id");
    });
</script>
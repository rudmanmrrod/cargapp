<?php
  use app\models\McTcategory;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper"> 
      <div class="main"> 
        <div class="row head-main administration-navigation col-lg-12">
          <a class="btn <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas">ACTIVOS</a>
          <a class="btn <?php echo $option == 0 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas?status=2">FINALIZADOS</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- bucle -->
<div class="content">
    <div class="container">
      <div class="row" style="background-color: #fff;"> 
        <div class="page-header">
			<img class="ant" src="<?php echo Yii::getAlias('@web'); ?>/img/ant.png">
		    <h1>
		        <small class="small-block mt"><strong>No hay propuestas para esta carga!</strong></small>
		        <strong class="mred">No te desesperes</strong>
		    </h1>
		    <h4>
		    	<strong>Te notificaremos tan pronto un transportador <br> haga una propuesta</strong>
		    </h4>
		    <br><br><br>
		</div> 
      </div>
    </div>
  </div>
<!-- End bucle -->
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<style type="text/css">
  .form-group {
      margin-bottom: -10px !important;
  }
</style>
<?php if(isset($error)){?>
<div class="page-header">
    <h1>
        <small class="small-block"><strong>Error<br><?php print_r($error); ?></strong></small>
    </h1>
</div> 
<?php } ?>
<div class="edit-form cotizar-form">

     <?php $listData=[]; ?>
     <?php $listData2=ArrayHelper::map($category,'pkcategory','category_name'); 
      unset($listData2[4]);
     ?>
     <?php $listDataCity=ArrayHelper::map($citys,'pkcity','city_name'); ?>
     <?php $form = ActiveForm::begin([
          'id' => 'login-form',
          'options' => ['class' => 'form-signin']
      ]); ?>
      <div class="edit-form-inner">
          <h4>Cotizar carga</h4>
          <?= $form->field($model, 'origin')->input("text", ['placeholder'=>'Origen', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, 'destination')->input("text", ['placeholder'=>'Destino', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, 'category')->dropDownList($listData2, 
            [
              'options' => [ $catid => ['selected ' => true]],
              'prompt'=>'Categoria',
              'class' => 'form-field',
              'onchange'=>'
                $.get( "'.Url::toRoute('account/getsubs').'", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#'.Html::getInputId($model, 'subcategory').'" ).html( data );
                    }
                );
            '
            ])->label(false); ?>
          <?= $form->field($model, 'subcategory')->dropDownList($listData, ['prompt'=>'Sub Categoria', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, "weight")->input("number", ['placeholder' => 'Peso (Kg)', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, "value")->input("number", ['placeholder' => 'Valor Declarado', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, "dimensions")->input("text", ['placeholder' => 'Dimensión (metro) (largo x ancho x prof)', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, "valueaseg")->input("number", ['placeholder' => 'Valor a asegurar', 'class' => 'form-field'])->label(false); ?>
          <?= $form->field($model, 'comment')->textarea(['placeholder' => 'Comentarios a tu envío', 'class' => 'form-field', 'rows'=> '4'])->label(false); ?>


      <div id="mapid" style="height: 180px;"></div>
        <div class="row">
          <div class="col-md-6">
          <?= $form->field($model, 'latitude_orig')->textInput(['placeholder' => 'Latitud', 'class' => 'form-field', 'readonly'=>true])->label(false); ?>
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'longitude_orig')->textInput(['placeholder' => 'Longitud', 'class' => 'form-field', 'readonly'=>true])->label(false); ?>
        </div>
        </div>
        <div class="row">
          <div class="col-md-6">
          <?= $form->field($model, 'latitude_dest')->textInput(['placeholder' => 'Latitud', 'class' => 'form-field', 'readonly'=>true])->label(false); ?>
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'longitude_dest')->textInput(['placeholder' => 'Longitud', 'class' => 'form-field', 'readonly'=>true])->label(false); ?>
        </div>
        </div>
      </div>
      <div class="submit-btn-area">
            <input  type="submit" name="updateprofile" value="Cotizar carga" class="updateprofile_btn text-uppercase">
        </div>
      <?php ActiveForm::end(); ?>
</div>
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/leaflet.css">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/leaflet.awesome-markers.css">
<script src="<?php echo Yii::getAlias('@web') ?>/js/leaflet.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/leaflet.awesome-markers.min.js"></script>
<script>
  var mymap = L.map('mapid').setView([4.59772, -74.07656], 13);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'rudmanmrrod.0f8gf90d',
    accessToken: 'pk.eyJ1IjoicnVkbWFubXJyb2QiLCJhIjoiY2lwcG5ja29lMDQ0emZqbTNuazRrZWxuciJ9.yYp783TnZbC7HxeIC8v96g'
}).addTo(mymap)

  var redMarker = L.AwesomeMarkers.icon({ markerColor: 'red' });
  var blueMarker = L.AwesomeMarkers.icon({icon:'pushpin', markerColor: 'blue' });

  var origin = L.marker([0,0],{icon: blueMarker,title:'Origen'}).addTo(mymap);
  var destination = L.marker([0,0], {icon: redMarker, title:'Destino'}).addTo(mymap);

  var OriginControl = L.Control.extend({
 
    options: {
      position: 'topright' 
      //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
    },
   
    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      $(container).append("<i class='glyphicon glyphicon-pushpin' style='top:8px;left:8px;'></i>");
      $(container).attr('name','origin-point');
      $(container).attr('title','Origen');
      L.DomEvent.on(container, 'mousedown dblclick', L.DomEvent.stop)
      .on(container, 'click', L.DomEvent.stop);
   
      container.style.backgroundColor = 'white';
      container.style.width = '30px';
      container.style.height = '30px';
      return container;
    },
  });

  var DestinationControl = L.Control.extend({
 
    options: {
      position: 'topright',
      title: 'Destino', 
      //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
    },
   
    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      $(container).append("<i class='glyphicon glyphicon-home' style='top:8px;left:8px;'></i>");
      $(container).attr('name','destination-point');
      $(container).attr('title','Destino');
      L.DomEvent.on(container, 'mousedown dblclick', L.DomEvent.stop)
      .on(container, 'click', L.DomEvent.stop);
   
      container.style.backgroundColor = 'white';
      container.style.width = '30px';
      container.style.height = '30px';
      return container;
    },
  });



  function onMapClick(e) {
    var text = $('.selected').attr('name');
    if(text=="origin-point")
    {
      origin.setLatLng(e.latlng);
      $('#formcotizar-latitude_orig').val(e.latlng.lat);
      $('#formcotizar-longitude_orig').val(e.latlng.lng);
    }
    else if(text=="destination-point")
    {
      destination.setLatLng(e.latlng);
      $('#formcotizar-latitude_dest').val(e.latlng.lat);
      $('#formcotizar-longitude_dest').val(e.latlng.lng);
    }
    console.log(e.latlng);
  }

  mymap.addControl(new OriginControl());
  mymap.addControl(new DestinationControl());
  mymap.doubleClickZoom.disable(); 
  mymap.on('click', onMapClick);

  $(document).ready(function(){
    $('.leaflet-control-custom').click(function(){
      var attribute = $(this).attr('class');
      if(attribute.search('selected')!=-1)
      {
        $(this).removeClass('selected');
        $(this).css('background-color','white');
      }
      else
      {
        var anterior = $('.leaflet-right').find('.selected');
        $(anterior).removeClass('selected');
        $(anterior).css('background-color','white');
        $(this).addClass('selected');
        $(this).css('background-color','gray');
      }
    });
});
</script>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper"> 
      <div class="main"> 
        <div class="row head-main administration-navigation col-lg-12">
          <a class="btn <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas">ACTIVOS</a>
          <a class="btn <?php echo $option == 0 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas?status=3">FINALIZADOS</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
    <div class="container">
      <div class="row" style="background-color: #fff;"> 
        <div class="page-header">
			<img class="ant" src="<?php echo Yii::getAlias('@web'); ?>/img/ant.png">
		    <h1>
		        <small class="small-block mt"><strong>No has realizado ninguna cotizaci&oacute;n</strong></small>
		    </h1>
		    <h4>
		    	<strong>Haz clic <a class="mred" href="<?php echo Yii::getAlias('@web'); ?>/account/cotizar">aqui</a> para hacer una cotizaci&oacute;n</strong>
		    </h4>
		    <br><br><br>
		</div> 
      </div>
    </div>
  </div>
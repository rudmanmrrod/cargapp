<?php
  use app\models\McTcategory;
  use yii\helpers\Html;
?>
<div class="content">
  <div class="container">
    <div class="row white administration-navigation-wrapper"> 
      
      <!-- main content -->
      
      <div class="main"> 
        
        <!--head-->
        <div class="row head-main administration-navigation col-lg-12">
          <a class="btn <?php echo $option == 1 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas">ACTIVOS</a>
          <a class="btn <?php echo $option == 0 ? 'btn-active' : 'btn-desactive'; ?> btn-sub2 col-md-2 col-xs-12 col-sm-2 col-lg-2" href="<?php echo Yii::getAlias('@web') ?>/account/cargas?status=3">FINALIZADOS</a>
        </div>
        
        <!--end head--> 
      </div>
      
      <!--End content--> 
      
    </div>
  </div>
</div>
<!-- bucle -->
<div class="content">
    <div class="container">
      <div class="row"> 
        <div class="col-sm-12">
          <?php if(isset($msg)) echo '<h3 class="confirm">' . $msg . '</h3>';?>
          <?php foreach ($cargas as $c) {?>
            <?php $catg = McTcategory::find($c->getFksubcategory0()->One()->fkcategory)->One(); ?>
            <div class="row administration-side administration-side-activity block-border">
              <div class="col-sm-12 col-xs-12 administration-activity-info ">
                <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <center>
                      <img src="<?php echo Yii::getAlias('@web') ?>/img/<?php echo $c->load_ensure == null ? 'c_noens.png' : 'c_ens.png'; ?>" class="mt40 hidden-xs"/>
                      <img src="<?php echo Yii::getAlias('@web') ?>/img/<?php echo $c->load_ensure == null ? 'c_noens.png' : 'c_ens.png'; ?>" class="mt7 visible-xs"/>
                    </center>
                    <center class="title_carg hidden-xs"><?php echo $c->load_ensure == null ? 'Carga No Asegurada' : 'Carga Asegurada'; ?></center>
                    <center class="title_carg visible-xs mt7 mb20"><?php echo $c->load_ensure == null ? 'Carga No Asegurada' : 'Carga Asegurada'; ?></center>
                  </div>
                  <div class="col-lg-2 col-md-3 col-sm-4 col-xs-5">
                    <ul class="list-unstyled">
                      <li class="blue">Fecha Pedido:</li>
                      <li>Lugar Origen:</li>
                      <li>Lugar Destino</li>
                      <li>Categoria</li>
                      <li>Sub Categoria</li>
                      <li>Peso</li>
                      <li>Dimensiones</li>
                      <?php echo $c->load_ensure == null ? "" : "<li>Valor asegurado</li>"; ?>
                      <li>Comentarios</li>
                    </ul>
                  </div>
                  <div class="col-lg-4 col-md-3 col-sm-4 col-xs-7">
                    <ul class="list-unstyled">
                      <li class="blue"><?php echo date("d-m-Y",  strtotime($c->load_create));?></li>
                      <li><?php echo $c->load_cityorigin; ?></li>
                      <li><?php echo $c->load_citydestination; ?></li>
                      <li><?php echo $catg->category_name;?></li>
                      <li><?php echo $c->getFksubcategory0()->One()->fksubcategory0->subcategory_name; ?></li>
                      <li><?php echo $c->load_weight < 1000 ? $c->load_weight . " Kg" : ($c->load_weight / 1000) . " Ton." ?></li>
                      <li><?php echo $c->load_dimensions; ?></li>
                      <?php echo $c->load_ensure == null ? "" : "<li class='blue'>" . number_format($c->load_value) . " COP</li>"; ?>
                      <li><?php echo $c->load_comment; ?></li>
                    </ul>
                  </div>
                  <div class="col-lg-2 col-md-2">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php if($c->fkstatus!=3) {?>
                      <a type="button" class="btn btn-desactive btn-sub2 btn-green col-xs-12" href="<?php echo Yii::getAlias('@web') ?>/account/detalle/<?php echo $c->pkload;?>">ENTRAR</a>
                      <?php }
                      else
                      {
                        echo '<a type="button" class="btn btn-desactive btn-sub2 btn-red col-xs-12">FINALIZADO</a>';
                      }
                      ?>
                    </div>
                    <?php if($c->fkstatus!=3) {?>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?= Html::a('CANCELAR', ['cancelload', 'id' => $c->pkload ], ['class' => 'btn btn-red btn-sub2 btn-desactive col-xs-12']) ?>
                      </div>
                    <?php }?>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
<!-- End bucle -->